package de.superioz.network.bungee;

import de.superioz.network.api.*;
import de.superioz.network.api.mongo.MongoConfigData;
import de.superioz.network.api.netty.*;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.command.*;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.common.party.Party;
import de.superioz.network.bungee.common.party.PartyHandler;
import de.superioz.network.bungee.common.report.ReportReasonManager;
import de.superioz.network.bungee.listener.*;
import de.superioz.network.bungee.util.Debugger;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.CommandErrorEvent;
import de.superioz.sx.bungee.command.CommandExecutor;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.bungee.exception.CommandRegisterException;
import de.superioz.sx.java.util.Consumer;
import de.superioz.sx.java.util.JsonUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.UUID;

/**
 * Created on 26.04.2016.
 */
public class NetworkBungee extends Plugin {

	public static final String CONFIG_FILE_NAME = "config.yml";
	public static final String FILE_RESOURCE_DIR = "bungee";

	public static NetworkBungee h;
	public static ConfigManager c;
	public static NioSocketServer s;

	@Override
	public void onEnable(){
		BungeeLibrary.initFor(this);
		NetworkAPI.setLogger(getLogger());
		h = this;

		// Load config & database
		this.loadConfig();
		this.loadDatabase();

		// Load the masterServer
		this.loadNettyServer();

		// Register listener
		this.registerListener();

		// Register command
		this.registerCommand();
	}

	@Override
	public void onDisable(){
		// Code here
		Debugger.build();
	}

	/**
	 * Adds default values for given player into the database
	 *
	 * @param player The player
	 */
	public static void writeIntoDatabase(ProxiedPlayer player){
		PlayerProfile profile = ProfileManager.writeSoft(player.getUniqueId(),
				player.getName(), "", player.getAddress().getAddress().getHostAddress());
		FriendManager.writeSoft(profile.getId());
	}

	/**
	 * Registers all listener
	 */
	private void registerListener(){
		// Debug
		Debugger.write("Register listener ..");
		long t = System.currentTimeMillis();

		// Listener
		getProxy().getPluginManager().registerListener(this, new ChatListener());
		getProxy().getPluginManager().registerListener(this, new JoinListener());
		getProxy().getPluginManager().registerListener(this, new TabCompleteListener());
		getProxy().getPluginManager().registerListener(this, new FriendListener());
		getProxy().getPluginManager().registerListener(this, new PermissionListener());
		getProxy().getPluginManager().registerListener(this, new SetTabHeaderListener());
		getProxy().getPluginManager().registerListener(this, new PartyListener());
		getProxy().getPluginManager().registerListener(this, new PingListener());

		// Debug
		Debugger.write("Listener registered. (" + (System.currentTimeMillis()-t) + "ms)");
	}

	/**
	 * Registers the commands
	 */
	private void registerCommand(){
		final Consumer<CommandErrorEvent> DEFAULT_CONSUMER
				= new Consumer<CommandErrorEvent>() {
			@Override
			public void accept(CommandErrorEvent event){
				CommandSender sender = event.getSender();
				CommandContext context = event.getContext();
				CommandExecutor.ErrorReason reason = event.getReason();

				switch(reason){
					case NO_PERMISSIONS:
						BungeeChat.send(Strings.NO_PERMISSIONS, sender);
					case WRONG_SENDER:
						BungeeChat.send(Strings.WRONG_COMMAND_EXECUTOR, sender);
					case WRONG_USAGE:
						context.sendUsage();
				}
			}
		};

		// Debug
		Debugger.write("Register commands ..");
		long t = System.currentTimeMillis();

		// Register the classes
		try{
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ChatLogCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, TeamChatCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, BroadcastCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ConfigCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, JumpCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ReportCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ReportInfoCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, FriendCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, MessageCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ReplyCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, PlayerInfoCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, ChatBanCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, UnbanCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, BanCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, KickCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, TempBanCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, BanInfoCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, PermissionCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, RankCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, GroupCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, RealNameCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, NickCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, PartyCommand.class);
			BungeeLibrary.registerCommand(DEFAULT_CONSUMER, PartyCommand.MessageCommand.class);

			// Debug
			Debugger.write("Commands registered. (" + (System.currentTimeMillis()-t) + "ms)");
		}
		catch(CommandRegisterException e){
			e.printStackTrace();
		}
	}

	/**
	 * Loads the config file
	 */
	private void loadConfig(){
		c = new ConfigManager(CONFIG_FILE_NAME, FILE_RESOURCE_DIR);
		Debugger.initialise();
		getLogger().info("Debug Mode is " + (Debugger.isEnabled() ? "on" : "off"));

		// Report reasons
		ReportReasonManager.init();
	}

	/**
	 * Load the config data for the database and connect it to the
	 * mongo database
	 */
	private void loadDatabase(){
		MongoConfigData configData = c.getDatabaseConfig(true);
		NetworkAPI.mongoConnect(configData.getDatabasePrefix(),
				configData.getHostName(),
				configData.getPort(),
				configData.getUserName(),
				configData.getPassword(),
				configData.getDatabase());

		if(NetworkAPI.checkMongo()){
			GroupManager.synchronize();
		}
	}

	/**
	 * Loads the netty server and adds a packet handler
	 * for synchronizing database credentials across the servers
	 */
	private void loadNettyServer(){
		if(s == null || !s.check()){
			s = NettyHandler.setupServer();
		}
		if(!s.check()){
			return;
		}

		// Register packet handler
		NettyHandler.registerPacketHandler(new NioPacketHandler() {
			@Override
			public NioPacketReceiveEvent.ChannelType getType(){
				return NioPacketReceiveEvent.ChannelType.MASTER;
			}

			@Override
			public void onReceive(NioPacketReceiveEvent event){
				NioPacket packet = event.getPacket();

				// Debug
				Debugger.write("Received packet from " + event.getSender().remoteAddress());

				// Code here
				switch(packet.getCommand()){
					case NettyHandler.GET_DATABASE_COMMAND:
						if(packet.getType() != NioPacketType.REQUEST){
							return;
						}
						MongoConfigData data = c.getDatabaseConfig(false);

						// Debug
						Debugger.write("Respond to " + event.getSender().remoteAddress() + " (" + packet.getCommand() + ")");
						event.respond(new NioPacket(NettyHandler.GET_DATABASE_COMMAND, JsonUtil.getGson().toJson(data)));
						break;
					case NettyHandler.GET_PARTY_COMMAND:
						if(packet.getType() != NioPacketType.REQUEST){
							return;
						}
						UUID playerUniqueId = UUID.fromString(packet.getContent());
						String respond;

						if(!ProfileManager.hasProfile(playerUniqueId)
								|| !PartyHandler.hasParty(playerUniqueId)){
							respond = "0";
						}
						else{
							Party party = PartyHandler.getParty(playerUniqueId);
							if(party == null) return;
							SerializableParty sParty = party.serialize();
							respond = sParty.toJson();
						}

						// Debug
						Debugger.write("Respond to " + event.getSender().remoteAddress() + " (" + packet.getCommand() + ")");
						event.respond(new NioPacket(NettyHandler.GET_PARTY_COMMAND, respond));
						break;
					default:
						break;
				}
			}
		});
	}

}
