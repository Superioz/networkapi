package de.superioz.network.bungee.util;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.sx.java.file.SuperProperties;
import de.superioz.sx.java.file.TextFile;
import lombok.Getter;

/**
 * Created on 30.04.2016.
 */
public class LanguageManager {

	private static SuperProperties<String> prefixes;
	private static SuperProperties<String> strings;

	@Getter
	private static TextFile banMessageFile;
	@Getter
	private static TextFile permaBanMessageFile;
	@Getter
	private static TextFile kickMessageFile;

	/**
	 * Initialises all property files
	 */
	public static void initialise(){

		// Prfixes
		prefixes = new SuperProperties<>("prefixes", "language", NetworkBungee.h.getDataFolder());
		prefixes.load(NetworkBungee.FILE_RESOURCE_DIR, true, true);

		// Strings
		strings = new SuperProperties<>("strings", "language", NetworkBungee.h.getDataFolder());
		strings.load(NetworkBungee.FILE_RESOURCE_DIR, true, true);

		// Text files
		banMessageFile = new TextFile("ban_message",
				"language", NetworkBungee.h.getDataFolder());
		banMessageFile.load("bungee", true, true);
		permaBanMessageFile = new TextFile("ban_permanent_message",
				"language", NetworkBungee.h.getDataFolder());
		permaBanMessageFile.load("bungee", true, true);
		kickMessageFile = new TextFile("kick_message",
				"language", NetworkBungee.h.getDataFolder());
		kickMessageFile.load("bungee", true, true);
	}

	/*
	Getter for different property files
	 */

	public static String getPrefix(String v){
		return prefixes.get(v);
	}

	public static String getString(String v){
		return strings.get(v);
	}

}
