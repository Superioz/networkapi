package de.superioz.network.bungee.util;

/**
 * Created on 09.04.2016.
 */
public class Prefixes {

	public static String GLOBAL;
	public static String ADVERTISEMENT;
	public static String TEAM;
	public static String CHAT;
	public static String REPORT_USER;
	public static String REPORT_TEAM;
	public static String FRIEND;
	public static String BAN;
	public static String HELP;
	public static String PERM;
	public static String NICK;
	public static String PARTY;

	// Load from properties
	public static void load(){
		GLOBAL = LanguageManager.getPrefix("global");
		ADVERTISEMENT = LanguageManager.getPrefix("advert");
		TEAM = LanguageManager.getPrefix("teamChat");
		CHAT = LanguageManager.getPrefix("chat");
		REPORT_USER = LanguageManager.getPrefix("reportUser");
		REPORT_TEAM = LanguageManager.getPrefix("reportTeam");
		FRIEND = LanguageManager.getPrefix("friend");
		BAN = LanguageManager.getPrefix("ban");
		HELP = LanguageManager.getPrefix("help");
		PERM = LanguageManager.getPrefix("perm");
		NICK = LanguageManager.getPrefix("nick");
		PARTY = LanguageManager.getPrefix("party");
	}

}
