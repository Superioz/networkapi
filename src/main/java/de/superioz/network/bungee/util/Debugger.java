package de.superioz.network.bungee.util;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.sx.java.file.LogCache;
import lombok.Getter;

/**
 * Created on 06.05.2016.
 */
@Getter
public class Debugger {

	public static final String FILE_NAME = "Debugger Log (%time)";

	private static LogCache log;
	@Getter
	private static boolean enabled = false;

	/**
	 * Initialises the debugger
	 */
	public static void initialise(){
		enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.DEBUG_MODE);
		if(!enabled) return;

		if(log != null) return;
		log = new LogCache(NetworkBungee.h.getDataFolder(), "debug", FILE_NAME);
	}

	/**
	 * Writes something into the debugger log
	 *
	 * @param message The message
	 */
	public static void write(String message){
		if(!enabled) return;
		log.log(message);
	}

	/**
	 * Builds the debugger file
	 */
	public static void build(){
		if(!enabled) return;
		if(log != null) log.build();
	}

}
