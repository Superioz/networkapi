package de.superioz.network.bungee.util;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.file.TextFile;
import de.superioz.sx.java.util.PageableList;
import lombok.Getter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 10.04.2016.
 */
public class HelpManager {

	private static final String LINE_PREFIX = "&8%a";
	private static final String LINE_SPACER = "&8%b";
	private static final String COMMAND_COLOR = "&3";

	@Getter
	private static HashMap<String, TextFile> helpFileMap = new HashMap<>();

	/**
	 * Inits the helpManager
	 */
	public static void init(){
		File dataFolder = NetworkBungee.h.getDataFolder();
		for(Type t : Type.values()){
			TextFile file = new TextFile("help_" + t.getFileName(), "language", dataFolder);
			file.load("bungee", true, true);
			helpFileMap.put(t.getFileName(), file);
		}
	}

	/**
	 * Send help from a specific type
	 *
	 * @param player The player
	 * @param type   The type
	 */
	public static void sendHelp(CommandSender player, Type type, int page){
		List<String> lines = type.getLines();
		PageableList<String> list = new PageableList<>(12, lines);

		// Check page
		if(!list.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST,
					Prefixes.HELP, player);
			return;
		}

		List<String> pageLines = list.calculatePage(page);
		String header = type.getHeader()
				.replace("%page", page + "")
				.replace("%max", list.getTotalPages() + "");

		BungeeChat.send(header, Prefixes.HELP, player);
		for(String s : pageLines){
			if(s == null)
				continue;
			s = s.replace("%s", LINE_SPACER);
			BungeeChat.send(LINE_PREFIX + " " + COMMAND_COLOR + s, player);
		}
		BungeeChat.send(header, Prefixes.HELP, player);
	}

	/**
	 * Get one line help
	 *
	 * @param command The command
	 * @param senders The receiver
	 */
	public static void sendOneLine(String command, CommandSender... senders){
		String s = Prefixes.HELP + " " + "&c/" + command;
		BungeeChat.send(s, senders);
	}

	public static void sendLine(String command, String desc, CommandSender... senders){
		String s = LINE_PREFIX + " " + COMMAND_COLOR + "/" + command + " "
				+ LINE_SPACER + " &7" + desc;
		BungeeChat.send(s, senders);
	}

	public static void sendHeader(CommandContext context){
		String s = "&7Befehlhilfe &b/" + context.getCommand().getLabel();
		BungeeChat.send(s, Prefixes.HELP, context.getSender());
	}

	/**
	 * Type of help
	 */
	@Getter
	public enum Type {

		FRIEND_SYSTEM("friendsystem", "&7Freundesystem"),
		PARTY_SYSTEM("partysystem", "&7Partysystem"),
		GROUP_SYSTEM("groupsystem", "&7Gruppensystem");

		private String fileName;
		private String header;

		Type(String fileName, String header){
			this.fileName = fileName;
			this.header = header + " " + "(Seite &a%page&7/%max)";
		}

		/**
		 * Get the lines
		 *
		 * @return The list of string
		 */
		public List<String> getLines(){
			return HelpManager.getHelpFileMap().get(getFileName()).readln();
		}

	}

}
