package de.superioz.network.bungee.util;

/**
 * Created on 30.04.2016.
 */
public class Permissions {

	public static final String CHAT_SPAM_BYPASS = "chat.antispam.bypass";
	public static final String CHAT_CAPS_BYPASS = "chat.anticaps.bypass";
	public static final String CHAT_DELAY_BYPASS = "chat.delay.bypass";
	public static final String CHAT_BLOCKED_WORDS_BYPASS = "chat.antiswear.bypass";
	public static final String CHAT_ADVERTISEMENT_BYPASS = "chat.antiadvert.bypass";
	public static final String REPORT_BYPASS = "report.bypass";

	public static final String CHAT_TEAMCHAT = "chat.teamchat";
	public static final String CHAT_COLOR_CODES = "chat.colors";
	public static final String CHAT_SPECIAL_CHARS = "chat.chars";

	public static final String COMMAND_CHATLOG = "command.chatlog";
	public static final String COMMAND_BROADCAST = "command.broadcast";
	public static final String COMMAND_CONFIG = "command.config";
	public static final String COMMAND_REPORT = "command.report";
	public static final String COMMAND_JUMP = "command.jump";
	public static final String COMMAND_FRIEND = "command.registry";
	public static final String COMMAND_PLAYERINFO = "command.playerinfo";
	public static final String COMMAND_UNBAN = "command.unban";
	public static final String COMMAND_CHATBAN = "command.chatban";
	public static final String COMMAND_BAN = "command.ban";
	public static final String COMMAND_KICK = "command.kick";
	public static final String COMMAND_BAN_INFO = "command.baninfo";
	public static final String COMMAND_TEMPBAN = "command.tempban";
	public static final String COMMAND_IPBAN = "command.ipban";
	public static final String COMMAND_PARTY = "command.party";
	public static final String COMMAND_PERMISSION = "command.perm";
	public static final String COMMAND_PERMISSION_GROUP = "command.perm.group";
	public static final String COMMAND_REALNAME = "command.realname";
	public static final String COMMAND_NICK = "command.nick";

}
