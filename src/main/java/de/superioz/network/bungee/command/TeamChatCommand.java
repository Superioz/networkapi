package de.superioz.network.bungee.command;

import de.superioz.network.bungee.common.chat.ChatLog;
import de.superioz.network.bungee.common.chat.TeamChat;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 08.04.2016.
 */
@Command(label = "teamchat",
		aliases = {"tc"},
		desc = "Sends a message to all team members",
		permission = Permissions.CHAT_TEAMCHAT,
		usage = "<message>",
		commandTarget = AllowedCommandSender.PLAYER)
public class TeamChatCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("teamchat <Nachricht>", sender);
			return;
		}

		ProxiedPlayer player = context.getSenderAsPlayer();

		String message = ListUtil.insert(context.getArguments(), " ");

		// Check if teamChat is enabled
		if(!TeamChat.isEnabled()){
			BungeeChat.send(Strings.TEAMCHAT_NOT_ENABLED, Prefixes.GLOBAL, player);
			return;
		}
		TeamChat.sendMessageFrom(message, player);

		//
		// Logging
		//
		ChatLog.log(player, "/" + context.getCommand().getLabel() + " " + message, ChatLog.Level.DEBUG);
	}

}
