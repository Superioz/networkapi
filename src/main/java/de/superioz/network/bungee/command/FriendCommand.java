package de.superioz.network.bungee.command;

import de.superioz.network.api.FriendManager;
import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.common.FriendHandler;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.util.*;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.PageableList;
import de.superioz.sx.java.util.SimpleStringUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 10.04.2016.
 */
@Command(label = "friend",
		desc = "Main command for friends",
		permission = Permissions.COMMAND_FRIEND,
		flags = {"?"},
		usage = "<command> | -? <page>",
		commandTarget = AllowedCommandSender.PLAYER)
public class FriendCommand implements CommandCase {

	private static final String ACCEPT_COMMAND = "/friend accept ";
	private static final String DENY_COMMAND = "/friend deny ";

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend -? [Seite]", sender);
			return;
		}

		int page = 1;
		CommandFlag flag = context.getCommandFlag("?");
		if(flag.getArguments().size() > 0){
			page = flag.getInt(1, 1);
		}
		HelpManager.sendHelp(sender, HelpManager.Type.FRIEND_SYSTEM, page);
	}

	@Command(label = "add",
			desc = "Adds a friend to your list",
			usage = "[name]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void add(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendManager.getEntry(player.getUniqueId());

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend add <Name>", player);
			return;
		}

		// Check target
		String name = context.getArgument(1);
		if(!FriendHandler.checkTargetFriend(player, name, false, false)){
			return;
		}
		Friend target = FriendManager.getEntry(name);

		// Check request
		if(target.getPlayerId() == friendPlayer.getPlayerId()){
			BungeeChat.send(Strings.CANNOT_ADD_YOURSELF_FRIEND,
					Prefixes.FRIEND, player);
			return;
		}
		else if(friendPlayer.hasRequest(target.getName())){
			player.chat("/" + ACCEPT_COMMAND + name);
			return;
		}
		else if(target.hasRequest(friendPlayer.getName())){
			BungeeChat.send(Strings.ALREADY_SENT_REQUEST,
					Prefixes.FRIEND, player);
			return;
		}
		else if(friendPlayer.hasFriend(target.getUniqueId())){
			BungeeChat.send(Strings.IS_ALREADY_FRIEND,
					Prefixes.FRIEND, player);
			return;
		}

		// Confirmation
		target.addRequest(friendPlayer);
		BungeeChat.send(Strings.SENT_FRIEND_REQUEST_TO
				.replace("%color", ChatManager.getColor(target.getUniqueId()))
				.replace("%target", target.getName()), Prefixes.FRIEND, player);

		// Send information to target
		ProxiedPlayer targetPlayer = BungeeLibrary.proxy().getPlayer(target.getUniqueId());
		if(targetPlayer != null){
			String color = ChatManager.getColor(target.getUniqueId());
			String acceptCommand = ACCEPT_COMMAND + player.getName();
			String declineCommand = DENY_COMMAND + player.getName();

			BungeeChat.send(Strings.YOU_GOT_A_REQUEST.replace("%color", color)
					.replace("%sender", player.getName()), Prefixes.FRIEND, targetPlayer);
			BungeeChat.send(new WrappedMessage(Strings.USE_TO_ACCEPT_FRIEND)
					.replace("command", "&a" + acceptCommand,
							new ClickEvent(ClickEvent.Action.RUN_COMMAND, acceptCommand),
							new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									TextComponent.fromLegacyText(Strings.CLICK_TO_ACCEPT)))
					.applyReplacements(), Prefixes.FRIEND, targetPlayer);
			BungeeChat.send(new WrappedMessage(Strings.USE_TO_DENY_FRIEND)
					.replace("command", "&c" + declineCommand,
							new ClickEvent(ClickEvent.Action.RUN_COMMAND, declineCommand),
							new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									TextComponent.fromLegacyText(Strings.CLICK_TO_DECLINE)))
					.applyReplacements(), Prefixes.FRIEND, targetPlayer);
		}
	}

	@Command(label = "remove",
			desc = "Removes a friend from your list",
			usage = "[name]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void remove(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendManager.getEntry(player.getUniqueId());

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend remove <Name>", player);
			return;
		}

		// Check target
		String name = context.getArgument(1);
		if(!FriendHandler.checkTargetFriend(player, name)){
			return;
		}

		friendPlayer.removeFriend(name);
		BungeeChat.send(Strings.FRIEND_REMOVED, Prefixes.FRIEND, player);
	}

	@Command(label = "deny",
			desc = "Denies a friend request",
			usage = "[name]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void deny(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend deny <Name>", player);
			return;
		}

		// Check target
		String name = context.getArgument(1);
		if(!friendPlayer.hasRequest(name)){
			BungeeChat.send(Strings.DONT_HAVE_FRIEND_REQUEST_FROM, Prefixes.FRIEND, player);
			return;
		}

		friendPlayer.denyRequest(name);
		BungeeChat.send(Strings.FRIEND_DENIED, Prefixes.FRIEND, player);
	}

	@Command(label = "denyall",
			desc = "Denies all registry request",
			commandTarget = AllowedCommandSender.PLAYER)
	public void denyAll(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Check requests
		if(friendPlayer.getRequests().size() == 0){
			BungeeChat.send(Strings.DONT_HAVE_FRIEND_REQUESTS, Prefixes.FRIEND, player);
			return;
		}

		friendPlayer.denyAllRequests();
		BungeeChat.send(Strings.DENIED_ALL_FRIENDS, Prefixes.FRIEND, player);
	}

	@Command(label = "accept",
			desc = "Accepts a registry request",
			usage = "[name]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void accept(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend accept <Name>", player);
			return;
		}

		// Check target
		String name = context.getArgument(1);
		if(!friendPlayer.hasRequest(name)){
			BungeeChat.send(Strings.DONT_HAVE_FRIEND_REQUEST_FROM, Prefixes.FRIEND, player);
			return;
		}

		friendPlayer.acceptRequest(name);
		BungeeChat.send(Strings.FRIEND_ACCEPTED, Prefixes.FRIEND, player);
	}

	@Command(label = "acceptall",
			desc = "Accepts all registry request",
			commandTarget = AllowedCommandSender.PLAYER)
	public void acceptAll(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Check requests
		if(friendPlayer.getRequests().size() == 0){
			BungeeChat.send(Strings.DONT_HAVE_FRIEND_REQUESTS, Prefixes.FRIEND, player);
			return;
		}

		friendPlayer.acceptAllRequests();
		BungeeChat.send(Strings.ACCEPTED_ALL_FRIENDS, Prefixes.FRIEND, player);
	}

	@Command(label = "jump",
			desc = "Teleports to a friend",
			usage = "[name]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void jump(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("friend jump <Name>", player);
			return;
		}

		// Check target
		String targetName = context.getArgument(1);
		if(!FriendHandler.checkTargetFriend(player, targetName, true)){
			return;
		}
		Friend target = FriendManager.getEntry(targetName);
		ProxiedPlayer targetPlayer = BungeeLibrary.proxy().getPlayer(target.getUniqueId());

		ServerInfo serverInfo = targetPlayer.getServer().getInfo();
		player.connect(serverInfo);
	}

	@Command(label = "list",
			desc = "Lists all friends",
			usage = "[page]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void list(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();

		// Get page
		int page = 1;
		if(context.getArgumentsLength() >= 1){
			String p = context.getArgument(1);

			if(SimpleStringUtils.isInteger(p)){
				page = Integer.valueOf(p);
			}
		}
		Friend friendPlayer = FriendHandler.getFriend(player);
		PageableList<Friend> friends = new PageableList<>(10, friendPlayer.getFriends());

		// Check page & friends
		if(friends.getObjects().size() == 0){
			BungeeChat.send(Strings.YOU_DONT_HAVE_FRIENDS, Prefixes.FRIEND, player);
			return;
		}
		if(!friends.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.FRIEND, player);
			return;
		}

		// Headerlist
		String header = Strings.FRIEND_LIST_HEADER
				.replace("%page", page + "")
				.replace("%max", friends.getTotalPages() + "");
		BungeeChat.send(header, Prefixes.FRIEND, player);
		for(Friend friend : friends.calculatePage(page)){
			if(friend == null) continue;
			ProxiedPlayer p = BungeeLibrary.proxy().getPlayer(friend.getUniqueId());

			// Send message
			String onlineState = p != null ? "&aOnline" : "&cOffline";
			String color = ChatManager.getColor(friend.getUniqueId());
			BungeeChat.send(Strings.FRIEND_LIST_PART
					.replace("%color", color)
					.replace("%friend", friend.getName())
					.replace("%state", onlineState), player);
		}
		BungeeChat.send(header, Prefixes.FRIEND, player);
	}

	@Command(label = "requests",
			desc = "Lists all registry requests",
			usage = "[page]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void requests(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Get page
		int page = 1;
		if(context.getArgumentsLength() >= 1){
			String p = context.getArgument(1);

			if(SimpleStringUtils.isInteger(p)){
				page = Integer.valueOf(p);
			}
		}
		PageableList<Friend> requests = new PageableList<>(10, friendPlayer.getRequests());

		// Check page & friends
		if(requests.getObjects().size() == 0){
			BungeeChat.send(Strings.DONT_HAVE_FRIEND_REQUESTS, Prefixes.FRIEND, player);
			return;
		}
		if(!requests.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.FRIEND, player);
			return;
		}

		// Header list
		String header = Strings.REQUEST_LIST_HEADER.replace("%page", page + "")
				.replace("%max", requests.getTotalPages() + "");
		BungeeChat.send(header, Prefixes.FRIEND, player);
		for(Friend request : requests.calculatePage(page)){
			if(request == null) continue;

			// Send requests
			String color = ChatManager.getColor(request.getUniqueId());
			String acceptCommand = ACCEPT_COMMAND + request.getName();
			String declineCommand = DENY_COMMAND + request.getName();

			WrappedMessage message = new WrappedMessage(Strings.REQUEST_LIST_PART
					.replace("%color", color).replace("%name", request.getName()))
					.replace("y", "&aAnnehmen",
							new ClickEvent(ClickEvent.Action.RUN_COMMAND, acceptCommand),
							new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Strings.CLICK_TO_ACCEPT)))
					.replace("n", "&cAblehnen",
							new ClickEvent(ClickEvent.Action.RUN_COMMAND, declineCommand),
							new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Strings.CLICK_TO_DECLINE)))
					.applyReplacements();
			BungeeChat.send(message, player);
		}
		BungeeChat.send(header, Prefixes.FRIEND, player);
	}

}
