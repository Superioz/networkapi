package de.superioz.network.bungee.command;

import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.common.FriendHandler;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;

/**
 * Created on 13.04.2016.
 */
@Command(label = "msg",
		aliases = {"message"},
		desc = "Sends a message to a registry",
		permission = Permissions.COMMAND_FRIEND,
		usage = "<Name> <Nachricht>",
		commandTarget = AllowedCommandSender.PLAYER)
public class MessageCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() < 2){
			HelpManager.sendOneLine("msg <Name> <Nachricht>", sender);
			return;
		}

		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);
		String name = context.getArgument(1);

		// Check message
		String message = ListUtil.insert(Arrays.copyOfRange(context.getArguments(),
				1, context.getArgumentsLength()), " ");
		FriendHandler.writeMessage(friendPlayer, name, message);
	}

}
