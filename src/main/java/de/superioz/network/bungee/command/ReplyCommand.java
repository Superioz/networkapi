package de.superioz.network.bungee.command;

import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.common.FriendHandler;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 13.04.2016.
 */
@Command(label = "r",
		aliases = {"reply"},
		desc = "Replies to a private message",
		permission = Permissions.COMMAND_FRIEND,
		usage = "<Nachricht>",
		commandTarget = AllowedCommandSender.PLAYER)
public class ReplyCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("reply <Nachricht>", sender);
			return;
		}

		ProxiedPlayer player = context.getSenderAsPlayer();
		Friend friendPlayer = FriendHandler.getFriend(player);

		// Send message
		String message = ListUtil.insert(context.getArguments(), " ");
		FriendHandler.replyMessage(friendPlayer, message);
	}

}
