package de.superioz.network.bungee.command;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.util.*;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;

/**
 * Created on 09.04.2016.
 */
@Command(label = "jump",
		desc = "Command for global teleporting",
		permission = Permissions.COMMAND_JUMP,
		usage = "<player>",
		commandTarget = AllowedCommandSender.PLAYER)
public class JumpCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("jump <Name>", sender);
			return;
		}

		ProxiedPlayer player = context.getSenderAsPlayer();
		String argument = context.getArgument(1);

		ProxiedPlayer target = NetworkBungee.h.getProxy().getPlayer(argument);
		if(target == null){
			BungeeChat.send(Strings.PLAYER_NOT_ONLINE,
					Prefixes.GLOBAL, player);
			return;
		}

		// Target server
		Server targetServer = target.getServer();
		ServerInfo serverInfo = targetServer.getInfo();

		// Connect
		player.connect(serverInfo);
	}

}
