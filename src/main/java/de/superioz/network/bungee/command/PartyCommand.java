package de.superioz.network.bungee.command;

import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.party.Party;
import de.superioz.network.bungee.common.party.PartyHandler;
import de.superioz.network.bungee.common.party.PartyPlayer;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import de.superioz.sx.java.util.PageableList;
import de.superioz.sx.java.util.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;

/**
 * Created on 03.05.2016.
 */
@Command(label = "party",
		desc = "Main command for parties",
		flags = {"?"},
		permission = Permissions.COMMAND_PARTY,
		usage = "<command> | -? <page>",
		commandTarget = AllowedCommandSender.PLAYER)
public class PartyCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0
				|| !context.hasFlag("?")){
			HelpManager.sendOneLine("party -? [Seite]", sender);
			return;
		}

		int page = 1;
		CommandFlag flag = context.getCommandFlag("?");
		if(flag.getArguments().size() > 0){
			page = flag.getInt(1, 1);
		}
		HelpManager.sendHelp(sender, HelpManager.Type.PARTY_SYSTEM, page);
	}

	@Command(label = "info",
			desc = "Information about the party",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void info(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}
		Party party = partyPlayer.getParty();

		BungeeChat.send(Strings.PARTY_INFO_HEADER, Prefixes.PARTY, player);
		BungeeChat.send(Strings.PARTY_INFO_LEADER
				.replace("%color", ChatManager.getColor(party.getLeader()))
				.replace("%leader", party.getLeader().getName()), player);
		BungeeChat.send(Strings.PARTY_INFO_MEMBERSIZE
				.replace("%s", party.getFullMember().size() + ""), player);
		BungeeChat.send(Strings.PARTY_INFO_CREATEDATE
				.replace("%color", ChatManager.getColor(party.getRootOwner()))
				.replace("%date", TimeUtils.getCurrentTime(party.getTimeStamp(), "dd.MM.yy HH:mm"))
				.replace("%pl", party.getRootOwner()), player);
		BungeeChat.send(Strings.PARTY_INFO_HEADER, Prefixes.PARTY, player);
	}

	@Command(label = "leave",
			desc = "Leaves the current party",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void leave(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}

		Party party = partyPlayer.getParty();

		partyPlayer.leaveParty();
		party.send(Strings.PARTY_PLAYER_LEFT
				.replace("%c", ChatManager.getColor(player))
				.replace("%p", player.getName()));
		BungeeChat.send(Strings.YOU_LEFT_THE_PARTY, Prefixes.PARTY, player);
	}

	@Command(label = "requests",
			desc = "Shows all requests",
			permission = Permissions.COMMAND_PARTY,
			usage = "[Seite]",
			commandTarget = AllowedCommandSender.PLAYER)
	public void requests(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// Check page
		int page = context.getInt(1, 1);
		PageableList<String> list = new PageableList<>(10, new ArrayList<>(partyPlayer.getRequests().keySet()));
		if(list.getObjects().size() == 0){
			BungeeChat.send(Strings.YOU_HAVENT_AN_INVI, Prefixes.PARTY, player);
			return;
		}
		if(!list.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.PARTY, player);
			return;
		}

		String header = Strings.PARTY_REQUEST_LIST_HEADER
				.replace("%page", page + "")
				.replace("%max", list.getTotalPages() + "");
		BungeeChat.send(header, Prefixes.PARTY, player);
		for(String request : list.calculatePage(page)){
			if(request == null) continue;
			BungeeChat.send(Strings.PARTY_REQUEST_LIST_ITEM
					.replace("%p", request), player);
		}
		BungeeChat.send(header, Prefixes.PARTY, player);
	}

	@Command(label = "list",
			desc = "Lists all party members",
			usage = "[Seite]",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void list(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}
		Party party = partyPlayer.getParty();

		// Check page
		int page = context.getInt(1, 1);
		PageableList<ProxiedPlayer> list = new PageableList<>(10, party.getFullMember());
		if(!list.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.PARTY, player);
			return;
		}

		BungeeChat.send(Strings.PARTY_MEMBER_LIST_HEADER, Prefixes.PARTY, player);
		for(ProxiedPlayer p : list.calculatePage(page)){
			if(p == null) continue;
			BungeeChat.send(Strings.PARTY_MEMBER_LIST_ITEM
					.replace("%c", ChatManager.getColor(p))
					.replace("%p", p.getName())
					.replace("%r", party.isLeader(p) ? "&cPartyleiter" : "&7Mitglied"), player);
		}
		BungeeChat.send(Strings.PARTY_MEMBER_LIST_HEADER, Prefixes.PARTY, player);
	}

	@Command(label = "promote",
			desc = "Promotes someone from your party to leader",
			usage = "<Spieler>",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void promote(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// SEND HELP
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("party promote <Spieler>", player);
			return;
		}

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}
		if(!partyPlayer.isLeader()){
			BungeeChat.send(Strings.YOU_ARENT_THE_LEADER, Prefixes.PARTY, player);
			return;
		}

		ProxiedPlayer target;
		if(context.getArgumentsLength() < 1
				|| (target = BungeeLibrary.proxy().getPlayer(context.getArgument(1))) == null){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.PARTY, player);
			return;
		}
		Party party = partyPlayer.getParty();

		if(!party.getMember().contains(target)){
			BungeeChat.send(Strings.NOT_IN_YOUR_PARTY, Prefixes.PARTY, player);
			return;
		}

		// PROMOTE PLAYER
		party.promote(target);
		party.send(Strings.PARTY_PLAYER_PROMOTED
				.replace("%c", ChatManager.getColor(target))
				.replace("%p", target.getName()));
	}

	@Command(label = "kick",
			desc = "Kicks someone from your party",
			usage = "<Spieler>",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void kick(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// SEND HELP
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("party kick <Spieler>", player);
			return;
		}

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}
		if(!partyPlayer.isLeader()){
			BungeeChat.send(Strings.YOU_ARENT_THE_LEADER, Prefixes.PARTY, player);
			return;
		}

		ProxiedPlayer target;
		if(context.getArgumentsLength() < 1
				|| (target = BungeeLibrary.proxy().getPlayer(context.getArgument(1))) == null){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.PARTY, player);
			return;
		}
		Party party = partyPlayer.getParty();

		if(!party.getMember().contains(target)){
			BungeeChat.send(Strings.NOT_IN_YOUR_PARTY, Prefixes.PARTY, player);
			return;
		}

		// KICK PLAYER
		party.quitMember(target);
		party.send(Strings.PLAYER_KICKED_FROM_PARTY
				.replace("%c", ChatManager.getColor(target))
				.replace("%p", target.getName()));
		BungeeChat.send("", Prefixes.PARTY, target);
	}

	@Command(label = "disband",
			desc = "Disbands your party",
			permission = Permissions.COMMAND_PARTY,
			commandTarget = AllowedCommandSender.PLAYER)
	public void disband(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// Check party
		if(!partyPlayer.hasParty()){
			BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
			return;
		}
		if(!partyPlayer.isLeader()){
			BungeeChat.send(Strings.YOU_ARENT_THE_LEADER, Prefixes.PARTY, player);
			return;
		}

		// Delete party
		partyPlayer.getParty().send(Strings.PARTY_DISBANDED);
		partyPlayer.deleteParty();
	}

	@Command(label = "deny",
			desc = "Denies someones request",
			permission = Permissions.COMMAND_PARTY,
			usage = "<Spieler>",
			commandTarget = AllowedCommandSender.PLAYER)
	public void deny(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// SEND HELP
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("party deny <Spieler>", player);
			return;
		}

		String name = context.getArgument(1);
		if(!partyPlayer.hasRequest(name)){
			BungeeChat.send(Strings.NO_INVITATION_FOUND, Prefixes.PARTY, player);
			return;
		}
		else if(!partyPlayer.requestActive(name)){
			partyPlayer.removeRequest(name);
			BungeeChat.send(Strings.INVITATION_EXPIRED, Prefixes.PARTY, player);
			return;
		}

		// DENY INVITE
		partyPlayer.denyRequest(name);
		BungeeChat.send(Strings.INVITATION_DENIED, Prefixes.PARTY, player);
	}


	@Command(label = "accept",
			desc = "Accept someones request",
			permission = Permissions.COMMAND_PARTY,
			usage = "<Spieler>",
			commandTarget = AllowedCommandSender.PLAYER)
	public void accept(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		// SEND HELP
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("party accept <Spieler>", player);
			return;
		}

		String name = context.getArgument(1);
		if(!partyPlayer.hasRequest(name)){
			BungeeChat.send(Strings.NO_INVITATION_FOUND, Prefixes.PARTY, player);
			return;
		}
		else if(!partyPlayer.requestActive(name)){
			partyPlayer.removeRequest(name);
			BungeeChat.send(Strings.INVITATION_EXPIRED, Prefixes.PARTY, player);
			return;
		}

		// ACCEPT INVITE
		partyPlayer.acceptRequest(name);
		BungeeChat.send(Strings.INVITATION_ACCEPTED, Prefixes.PARTY, player);
		partyPlayer.getParty().send(Strings.PARTY_PLAYER_JOINED
				.replace("%c", ChatManager.getColor(player))
				.replace("%p", player.getName()));
	}

	@Command(label = "invite",
			desc = "Invites someone to your party",
			permission = Permissions.COMMAND_PARTY,
			usage = "<Spieler>",
			commandTarget = AllowedCommandSender.PLAYER)
	public void invite(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();

		// SEND HELP
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("party invite <Spieler>", player);
			return;
		}

		// Check argument
		ProxiedPlayer target;
		if(context.getArgumentsLength() < 1
				|| (target = BungeeLibrary.proxy().getPlayer(context.getArgument(1))) == null){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.PARTY, player);
			return;
		}
		if(target.equals(player)){
			BungeeChat.send(Strings.CANNOT_INVITE_YOURSELF, Prefixes.PARTY, player);
			return;
		}
		PartyPlayer partySender = PartyHandler.getPartyPlayer(player);
		Party party = partySender.createParty();

		// Is leader?
		if(!partySender.isLeader()){
			BungeeChat.send(Strings.YOU_ARENT_THE_LEADER, Prefixes.PARTY, player);
			return;
		}
		if(party.getMember().contains(target)){
			BungeeChat.send(Strings.ALREADY_IN_PARTY, Prefixes.PARTY, player);
			return;
		}
		PartyPlayer partyTarget = PartyHandler.getPartyPlayer(target);
		if(partyTarget.hasParty()){
			BungeeChat.send(Strings.ALREADY_IN_YOUR_PARTY, Prefixes.PARTY, player);
			return;
		}
		if(partyTarget.hasRequest(player.getName())){
			BungeeChat.send(Strings.PARTY_PLAYER_ALREADY_INVITED, Prefixes.PARTY, player);
			return;
		}

		// INVITE
		party.sendInvitation(player, partyTarget);
		party.send(Strings.PARTY_PLAYER_INVITED
				.replace("%c", ChatManager.getColor(target))
				.replace("%p", target.getName()));

		// SEND TO TARGET
		String color = ChatManager.getColor(target);
		String acceptCommand = "/party accept " + player.getName();
		String declineCommand = "/party deny " + player.getName();

		// SEND INFO TO TARGET
		BungeeChat.send(Strings.PARTY_INVITATION_RECEIVED
				.replace("%c", color)
				.replace("%p", player.getName()), Prefixes.PARTY, target);
		BungeeChat.send(new WrappedMessage(Strings.PARTY_CLICK_TO_ACCEPT)
				.replace("command", "&a" + acceptCommand,
						new ClickEvent(ClickEvent.Action.RUN_COMMAND, acceptCommand),
						new HoverEvent(HoverEvent.Action.SHOW_TEXT,
								TextComponent.fromLegacyText(Strings.CLICK_TO_ACCEPT)))
				.applyReplacements(), Prefixes.PARTY, target);
		BungeeChat.send(new WrappedMessage(Strings.PARTY_CLICK_TO_DENY)
				.replace("command", "&c" + declineCommand,
						new ClickEvent(ClickEvent.Action.RUN_COMMAND, declineCommand),
						new HoverEvent(HoverEvent.Action.SHOW_TEXT,
								TextComponent.fromLegacyText(Strings.CLICK_TO_DECLINE)))
				.applyReplacements(), Prefixes.PARTY, target);
	}

	@Command(label = "p",
			desc = "Sends a message",
			permission = Permissions.COMMAND_PARTY,
			usage = "<message>",
			commandTarget = AllowedCommandSender.PLAYER)
	public static class MessageCommand implements CommandCase {

		@Override
		public void execute(CommandContext context){
			ProxiedPlayer player = context.getSenderAsPlayer();
			PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

			if(context.getArgumentsLength() < 1){
				HelpManager.sendOneLine("p [Nachricht]", player);
				return;
			}
			if(!partyPlayer.hasParty()){
				BungeeChat.send(Strings.YOU_ARENT_IN_A_PARTY, Prefixes.PARTY, player);
				return;
			}

			Party party = partyPlayer.getParty();
			party.chat(player, ListUtil.insert(context.getArguments(), " "));
		}

	}

}
