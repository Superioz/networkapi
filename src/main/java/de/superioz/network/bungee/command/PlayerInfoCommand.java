package de.superioz.network.bungee.command;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import net.md_5.bungee.api.CommandSender;

/**
 * Created on 14.04.2016.
 */
@Command(label = "playerinfo",
		aliases = {"pinfo"},
		desc = "Command for informing about a player",
		permission = Permissions.COMMAND_PLAYERINFO,
		usage = "<name>",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class PlayerInfoCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("playerinfo <Name>", sender);
			return;
		}

		// Get target
		String name = context.getArgument(1);
		if(!ProfileManager.contains(name)){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.GLOBAL, sender);
			return;
		}
		PlayerProfile profile = ProfileManager.getProfile(name);

		// Send info
		String header = Strings.PLAYER_INFO_HEADER
				.replace("%color", ChatManager.getColor(profile.getUniqueId()))
				.replace("%name", name);

		// List
		BungeeChat.send(header, Prefixes.GLOBAL, sender);
		BungeeChat.send(Strings.PLAYER_INFO_UUID
				.replace("%uuid", profile.getUniqueId() + ""), sender);
		BungeeChat.send(Strings.PLAYER_INFO_ID
				.replace("%id", profile.getId() + ""), sender);
		BungeeChat.send(Strings.PLAYER_INFO_IP
				.replace("%ip", profile.getIp()), sender);
		BungeeChat.send(Strings.PLAYER_INFO_GROUP
				.replace("%group", profile.getGroup()), sender);
		BungeeChat.send(Strings.PLAYER_INFO_NICK
				.replace("%nick", profile.getNickName()), sender);
		BungeeChat.send(header, Prefixes.GLOBAL, sender);
	}

}
