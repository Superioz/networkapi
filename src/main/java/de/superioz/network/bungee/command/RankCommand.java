package de.superioz.network.bungee.command;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import net.md_5.bungee.api.CommandSender;

/**
 * Created on 01.05.2016.
 */
@Command(label = "rank",
		desc = "Ranks a specific user",
		flags = {"n", "r"},
		permission = Permissions.COMMAND_PERMISSION,
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class RankCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("rank <Name> -n <Gruppe>", "Gibt Spieler einen Rang", sender);
			HelpManager.sendLine("rank <Name> -r <Rang", "Gibt Spieler einen Rang", sender);
			return;
		}

		String name;
		if(context.getArgumentsLength() < 0
				|| !ProfileManager.hasProfile(name = context.getArgument(1))){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.PERM, sender);
			return;
		}
		PlayerProfile profile = ProfileManager.getProfile(name);

		PermissionGroup group = profile.getRealGroup();
		if(context.hasFlag("n")){
			CommandFlag f = context.getCommandFlag("n");
			String g;
			if(f.getArgumentsLength() < 1
					|| !GroupManager.hasGroup((g = f.getArgument(1)))){
				BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, sender);
				return;
			}

			group = GroupManager.getGroup(g);
		}
		else if(context.hasFlag("r")){
			CommandFlag f = context.getCommandFlag("r");
			int g;
			if(f.getArgumentsLength() < 1
					|| !GroupManager.hasGroup((g = f.getInt(1, group.getRank())))){
				BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, sender);
				return;
			}

			group = GroupManager.getGroup(g);
		}
		else{
			BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, sender);
			return;
		}

		if(group == null){
			BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, sender);
			return;
		}
		if(group.getName().equals(profile.getGroup())){
			BungeeChat.send(Strings.ALREADY_IN_GROUP, Prefixes.PERM, sender);
			return;
		}

		// set group
		profile.setGroup(group);
		BungeeChat.send(Strings.RANK_SETTED
				.replace("%player", profile.getName())
				.replace("%group", group.getName()), Prefixes.PERM, sender);
	}

}
