package de.superioz.network.bungee.command;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.util.*;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import net.md_5.bungee.api.CommandSender;
import org.apache.commons.codec.language.bm.Lang;

/**
 * Created on 09.04.2016.
 */
@Command(label = "config",
		desc = "Command for the manager config",
		permission = Permissions.COMMAND_CONFIG,
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class ConfigCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Send help
		HelpManager.sendOneLine("config reload", sender);
	}

	@Command(label = "reload",
			desc = "Reloads the config",
			permission = Permissions.COMMAND_CONFIG,
			commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
	public void reload(CommandContext commandContext){
		NetworkBungee.c.reload();
		BungeeChat.send(Strings.CONFIG_RELOADED,
				Prefixes.GLOBAL, commandContext.getSender());
	}

}
