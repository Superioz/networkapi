package de.superioz.network.bungee.command;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.chat.ChatLog;
import de.superioz.network.bungee.util.*;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;

/**
 * Created on 08.04.2016.
 */
@Command(label = "chatlog",
		flags = {"s", "u"},
		desc = "Creates a chatlog for a specific server",
		permission = Permissions.COMMAND_CHATLOG,
		usage = "(-s <serverName> | -u <player>)",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class ChatLogCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("chatlog -s <Server>", "Erstellt einen Server-Chatlog", sender);
			HelpManager.sendLine("chatlog -u <Name>", "Erstellt einen Spieler-Chatlog", sender);
			return;
		}

		// Get server
		if(context.hasFlag("s")){
			String serverName = ListUtil.insert(context.getCommandFlag("s").getArguments(), " ");
			ServerInfo server = NetworkBungee.h.getProxy().getServerInfo(serverName);

			// If the server doesn't exist
			if(server == null){
				BungeeChat.send(Strings.SERVER_DOESNT_EXIST,
						Prefixes.CHAT, sender);
				return;
			}

			// If cache exists
			if(!ChatLog.hasCache(server)){
				BungeeChat.send(Strings.SERVER_NO_LOG,
						Prefixes.CHAT, sender);
				return;
			}

			// Create log
			ChatLog.create(server.getName());
			BungeeChat.send(Strings.SERVER_CHATLOG_CREATED.replace("%server", serverName)
							.replace("%folder", ChatLog.getCache(server).getFolder()),
					Prefixes.CHAT, sender);
		}

		// Get user
		else if(context.hasFlag("u")){
			String user = context.getCommandFlag("u").getArgument(0);

			// If cache exists
			if(!ChatLog.hasCache(user)){
				BungeeChat.send(Strings.USER_NO_LOG,
						Prefixes.CHAT, sender);
				return;
			}

			// Create log
			ChatLog.createForUser(user);
			BungeeChat.send(Strings.USER_CHATLOG_CREATED.replace("%user", user)
							.replace("%folder", ChatLog.getCache(user).getFolder()),
					Prefixes.CHAT, sender);
		}
		else{
			context.sendUsage();
		}
	}

}
