package de.superioz.network.bungee.command;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.common.KickHandler;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 04.05.2016.
 */
@Command(label = "kick",
		desc = "Kicks someone",
		permission = Permissions.COMMAND_KICK,
		flags = {"r"},
		usage = "<Name> (-r <Grund>)",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class KickCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("kick <Name>", "Kick einen Spieler", sender);
			HelpManager.sendLine("kick <Name> -r <Grund>", "Kickt einen Spieler mit Grund", sender);
			return;
		}
		String name = context.getArgument(1);
		ProxiedPlayer target;

		// Check target
		if((target = BungeeLibrary.proxy().getPlayer(name)) == null){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.GLOBAL, sender);
			return;
		}
		boolean isConsole = !(sender instanceof ProxiedPlayer);
		PlayerProfile targetProfile = ProfileManager.getProfile(target.getUniqueId());
		PlayerProfile senderProfile = isConsole ? ProfileManager.CONSOLE_PROFILE
				: ProfileManager.getProfile(((ProxiedPlayer)sender).getUniqueId());
		if(!isConsole && targetProfile.getRealGroup().getRank()
				>= senderProfile.getRealGroup().getRank()){
			BungeeChat.send(Strings.CANNOT_KICK_PLAYER, Prefixes.GLOBAL, sender);
			return;
		}

		// Get reason
		String reason = "/";
		if(context.hasFlag("r")){
			CommandFlag fl = context.getCommandFlag("r");
			if(fl.getArgumentsLength() > 0){
				reason = ListUtil.insert(fl.getArguments(), " ");
			}
		}

		// Kick player
		KickHandler.kickPlayer(sender, target, reason);
	}

}
