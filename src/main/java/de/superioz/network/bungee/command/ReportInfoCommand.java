package de.superioz.network.bungee.command;

import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.report.Report;
import de.superioz.network.bungee.common.report.ReportHandler;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.PageableList;
import de.superioz.sx.java.util.SimpleStringUtils;
import de.superioz.sx.java.util.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;

/**
 * Created on 11.04.2016.
 */
@Command(label = "reportinfo",
		aliases = {"reporti"},
		desc = "Command for informing about reports",
		permission = Permissions.COMMAND_REPORT,
		flags = {"l", "u", "i"},
		usage = "[-l [page] | -u <userName> | -i <reportId>]",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class ReportInfoCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("reportinfo -l [Seite]", "Listet alle Reports", sender);
			HelpManager.sendLine("reportinfo -u <Name>", "Informationen über Spieler", sender);
			HelpManager.sendLine("reportinfo -i <ReportId>", "Informationen über bestimmten Report", sender);
			return;
		}

		if(context.hasFlag("l")){
			this.sendReportList(context);
		}
		else if(context.hasFlag("u")){
			this.sendUserReportInfo(context);
		}
		else if(context.hasFlag("i")){
			this.sendReportInfo(context);
		}
		else{
			int reportsSize = ReportHandler.getReports().size();

			if(reportsSize == 0){
				BungeeChat.send(Strings.NO_REPORTS_CURRENT, Prefixes.REPORT_TEAM, sender);
				return;
			}
			HashMap<BanReason.Parent, Double> map = ReportHandler.getReasonsPerCent();

			// Send message
			BungeeChat.send(Strings.GLOBAL_REPORT_INFO_HEADER, Prefixes.REPORT_TEAM, sender);
			BungeeChat.send(Strings.GLOBAL_REPORT_INFO_VALUE
					.replace("%val", ReportHandler.getReports().size()+""), sender);
			for(BanReason.Parent r : map.keySet()){
				BungeeChat.send(Strings.GLOBAL_REPORT_INFO_PART
						.replace("%reason", r.getName())
						.replace("%percent", map.get(r)+"%"), sender);
			}
			BungeeChat.send(Strings.GLOBAL_REPORT_INFO_HEADER, Prefixes.REPORT_TEAM, sender);
		}
	}

	/**
	 * Send report info from id
	 *
	 * @param context The context
	 */
	public void sendReportInfo(CommandContext context){
		CommandSender sender = context.getSender();

		CommandFlag flag = context.getCommandFlag("i");

		String reportId = "";
		if(flag.getArguments().size() >= 1){
			reportId = flag.getArgument(1);
		}

		// Check reportId
		Report report = ReportHandler.getReportFromId(reportId);
		if(report == null){
			BungeeChat.send(Strings.NO_REPORTS_WITH_ID, Prefixes.REPORT_TEAM, sender);
			return;
		}

		// Send report information
		ProxiedPlayer rSender = report.getFrom();
		ProxiedPlayer rTarget = report.getTarget();
		String senderName = rSender.getName();
		String targetName = rTarget.getName();
		String header = Strings.REPORT_INFO_ID_HEADER.replace("%id", reportId);

		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
		BungeeChat.send(Strings.REPORT_INFO_ID_SENDER
				.replace("%color", ChatManager.getColor(rSender))
				.replace("%name", senderName), sender);
		BungeeChat.send(Strings.REPORT_INFO_ID_TARGET
				.replace("%color", ChatManager.getColor(rTarget))
				.replace("%name", targetName), sender);
		BungeeChat.send(Strings.REPORT_INFO_ID_REASON
				.replace("%reason", report.getReason().getNiceName()), sender);
		BungeeChat.send(Strings.REPORT_INFO_ID_SERVER
				.replace("%server", report.getServer().getName()), sender);
		BungeeChat.send(Strings.REPORT_INFO_ID_TIME
				.replace("%time", TimeUtils.fromTimestamp(report.getTimeStamp(), "dd.MM.yyyy HH:mm:ss")), sender);
		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
	}

	/**
	 * Sends user report info from name
	 *
	 * @param context The context
	 */
	public void sendUserReportInfo(CommandContext context){
		CommandSender sender = context.getSender();

		CommandFlag flag = context.getCommandFlag("u");

		String userName = "";
		if(flag.getArguments().size() >= 1){
			userName = flag.getArgument(1);
		}
		Report r = ReportHandler.getReport(userName);

		// Check report
		if(r == null){
			BungeeChat.send(Strings.NO_REPORTS_FROM_PLAYER, Prefixes.REPORT_TEAM, sender);
			return;
		}

		// Send information
		ProxiedPlayer target = r.getTarget();
		String color = ChatManager.getColor(target);
		String header = Strings.REPORT_INFO_USER_HEADER
				.replace("%color", color)
				.replace("%name", target.getName());

		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
		BungeeChat.send(Strings.REPORT_INFO_USER_SENT
				.replace("%val", ReportHandler.getReportSenderSize(target) + ""), sender);
		BungeeChat.send(Strings.REPORT_INFO_USER_REPORTED
				.replace("%val", ReportHandler.getReportTargetSize(target) + "")
				.replace("%h", ReportHandler.getReportTargetTimeDiff(target) + ""), sender);
		BungeeChat.send(Strings.REPORT_INFO_USER_MOST_REASON
				.replace("%reason", ReportHandler.getMostReason(target).getNiceName()), sender);
		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
	}

	/**
	 * Sends report list to commandContext
	 *
	 * @param context The context
	 */
	public void sendReportList(CommandContext context){
		CommandSender sender = context.getSender();
		CommandFlag flag = context.getCommandFlag("l");

		// Get page
		int page = 1;
		if(flag.getArguments().size() >= 1){
			String pageString = flag.getArgument(1);

			if(SimpleStringUtils.isInteger(pageString)){
				page = Integer.valueOf(pageString);
			}
		}

		// Check
		PageableList<Report> reports = new PageableList<>(10, ReportHandler.getReports());
		if(reports.getObjects().size() == 0){
			BungeeChat.send(Strings.NO_REPORTS_CURRENT, Prefixes.REPORT_TEAM, sender);
			return;
		}
		if(!reports.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.REPORT_TEAM, sender);
			return;
		}

		// Send message
		String header = Strings.REPORT_LIST_HEADER
				.replace("%page", page + "")
				.replace("%max", reports.getTotalPages() + "");
		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
		for(Report r : reports.calculatePage(page)){
			if(r == null){
				continue;
			}
			// Report list part
			String id = r.getId();
			ProxiedPlayer target = r.getTarget();
			String color = ChatManager.getColor(target);
			String reason = r.getReason().getNiceName();
			String patternMessage = Strings.REPORT_LIST_PART
					.replace("%color", color)
					.replace("%name", target.getName())
					.replace("%reason", reason);

			if(!(sender instanceof ProxiedPlayer)){
				BungeeChat.send(patternMessage.replace("%id%", "&a" + id), sender);
				continue;
			}

			// Send message with events
			WrappedMessage message = new WrappedMessage(patternMessage)
					.replace("id", "&a" + id,
							new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reportinfo -i " + id),
							new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									TextComponent.fromLegacyText(Strings.CLICK_FOR_MORE_REPORT_INFO)))
					.applyReplacements();
			BungeeChat.send(message, context.getSenderAsPlayer());
		}
		BungeeChat.send(header, Prefixes.REPORT_TEAM, sender);
	}

}
