package de.superioz.network.bungee.command;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.PageableList;
import de.superioz.sx.java.util.SimpleStringUtils;
import net.md_5.bungee.api.CommandSender;

/**
 * Created on 01.05.2016.
 */
@Command(label = "permission",
		aliases = {"perm"},
		flags = {"?"},
		desc = "Organises the permission system",
		permission = Permissions.COMMAND_PERMISSION,
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class PermissionCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Send help
		HelpManager.sendHeader(context);
		HelpManager.sendLine("perm addgroup <Name>", "Erstellt eine Gruppe", sender);
		HelpManager.sendLine("perm addgroup <Name> -a", "Erstellt eine künstliche Gruppe", sender);
		HelpManager.sendLine("perm delgroup <Name>", "Löscht eine Gruppe", sender);
		HelpManager.sendLine("perm listgroups [Seite]", "Listet alle Gruppen auf", sender);
	}

	@Command(label = "listgroups",
			aliases = {"lg"},
			desc = "Lists all groups",
			permission = Permissions.COMMAND_PERMISSION,
			commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
	public void listgroups(CommandContext context){
		CommandSender sender = context.getSender();
		int page = context.getInt(1, 1);

		// List
		PageableList<PermissionGroup> groups = new PageableList<>(10, GroupManager.getGroups());
		if(!groups.firstCheckPage(page)){
			BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.PERM, sender);
			return;
		}

		String header = Strings.GROUP_LIST_HEADER
				.replace("%page", page + "")
				.replace("%max", groups.getTotalPages() + "");
		BungeeChat.send(header, Prefixes.PERM, sender);
		for(PermissionGroup g : groups.calculatePage(page)){
			if(g == null) continue;

			String msg = Strings.INHERITANCE_LIST_OF_PART
					.replace("%name", g.getName()).replace("%rank", g.getRank() + "");
			BungeeChat.send(msg, sender);
		}
		BungeeChat.send(header, Prefixes.PERM, sender);
	}

	@Command(label = "addgroup",
			aliases = {"ag"},
			flags = {"a"},
			desc = "Adds a group",
			permission = Permissions.COMMAND_PERMISSION,
			commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
	public void addGroup(CommandContext context){
		CommandSender sender = context.getSender();

		// Send help
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("perm addgroup <Name> (-a)", sender);
			return;
		}
		String name = context.getArgument(1);
		if(name.length() > 32)
			name = name.substring(0, 32);

		boolean artifact = false;
		if(context.hasFlag("a"))
			artifact = true;

		if(GroupManager.hasGroup(name)){
			BungeeChat.send(Strings.GROUP_ALREADY_EXISTS, Prefixes.PERM, sender);
			return;
		}

		PermissionGroup group = GroupManager.createGroup(name, GroupManager.getNewRank(), false, artifact);
		if(group != null){
			BungeeChat.send(Strings.CREATED_GROUP
					.replace("%group", group.getName()), Prefixes.PERM, sender);
		}
		else{
			BungeeChat.send(Strings.COULDNT_CREATE_GROUP, Prefixes.PERM, sender);
		}
	}

	@Command(label = "delgroup",
			aliases = {"dg"},
			desc = "Removes a group",
			permission = Permissions.COMMAND_PERMISSION,
			commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
	public void delGroup(CommandContext context){
		CommandSender sender = context.getSender();

		// Send help
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("perm delgroup <Name>", sender);
			return;
		}
		String name = context.getArgument(1);
		if(!GroupManager.hasGroup(name)){
			BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, sender);
			return;
		}

		if(GroupManager.deleteGroup(name)){
			BungeeChat.send(Strings.DELETED_GROUP
					.replace("%group", name), Prefixes.PERM, sender);
		}
		else{
			BungeeChat.send(Strings.COULDNT_DELETE_GROUP, Prefixes.PERM, sender);
		}
	}

}
