package de.superioz.network.bungee.command;

import de.superioz.network.api.NickManager;
import de.superioz.network.api.netty.NioPacket;
import de.superioz.network.api.netty.NioPacketType;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bukkit.common.GameProfileBuilder;
import de.superioz.sx.bukkit.common.protocol.WrappedGameProfile;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.Consumer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 03.05.2016.
 */
@Command(label = "nick",
		desc = "Nick yourself",
		permission = Permissions.COMMAND_NICK,
		usage = "<Name>",
		commandTarget = AllowedCommandSender.PLAYER)
public class NickCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		final ProxiedPlayer sender = context.getSenderAsPlayer();

		// Send help
		if(context.getArgumentsLength() < 1){
			// CHECK PROFILE
			if(!NickManager.hasNick(sender.getUniqueId())){
				BungeeChat.send(Strings.YOU_ARENT_NICKED, Prefixes.NICK, sender);
				return;
			}

			BungeeChat.send(Strings.REMOVE_NICKNAME, Prefixes.NICK, sender);
			// SEND PACKET TO SERVER TO UNNICK PLAYER
			NetworkBungee.s.send(new NioPacket(NioPacketType.REQUEST, "unnick", sender.getName()),
					new Consumer<NioPacket>() {
						@Override
						public void accept(NioPacket packet){
							boolean b = packet.getContent().equals("1");
							if(b){
								BungeeChat.send(Strings.NICK_NAME_REMOVED, Prefixes.NICK, sender);
							}
							else{
								BungeeChat.send(Strings.COULDNT_REMOVE_NICK, Prefixes.NICK, sender);
							}
						}
					});
			return;
		}
		final String name = context.getArgument(1);

		if(BungeeLibrary.proxy().getPlayer(name) != null){
			// CANNOT NICK AS THIS PLAYER
			BungeeChat.send(Strings.CANNOT_NICK_AS_PLAYER, Prefixes.NICK, sender);
			return;
		}

		BungeeChat.send(Strings.SET_NICKNAME, Prefixes.NICK, sender);
		// SEND PACKET TO SERVER TO NICK PLAYER
		NetworkBungee.s.send(new NioPacket(NioPacketType.REQUEST, "nick", sender.getName() + ";" + name),
				new Consumer<NioPacket>() {
					@Override
					public void accept(NioPacket packet){
						boolean b = packet.getContent().equals("1");
						if(b){
							BungeeChat.send(Strings.NICKNAME_SETTED
									.replace("%nick", name), Prefixes.NICK, sender);
						}
						else{
							BungeeChat.send(Strings.COULDNT_SET_NICKNAME, Prefixes.NICK, sender);
						}
					}
				});
	}

}
