package de.superioz.network.bungee.command;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 03.05.2016.
 */
@Command(label = "realname",
		desc = "Gets the realname of someone",
		permission = Permissions.COMMAND_REALNAME,
		usage = "<Spieler>",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class RealNameCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Send help
		if(context.getArgumentsLength() < 1){
			HelpManager.sendOneLine("realname <Spieler>", sender);
			return;
		}
		String name = context.getArgument(1);
		ProxiedPlayer player = null;
		for(ProxiedPlayer p : BungeeLibrary.proxy().getPlayers()){
			PlayerProfile prof = ProfileManager.getProfile(p.getUniqueId());

			if(prof != null && (prof.getNickName().isEmpty()
					|| prof.getNickName().equals(name))){
				player = p;
				break;
			}
		}
		PlayerProfile profile;

		if(player == null){
			// PLAYER NOT ONLINE
			BungeeChat.send(Strings.PLAYER_NOT_ONLINE, Prefixes.NICK, sender);
			return;
		}
		if((profile = ProfileManager.getProfile(player.getUniqueId())) == null){
			// PLAYER DOESNT EXIST
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.NICK, sender);
			return;
		}

		if(profile.getNickName().isEmpty()){
			BungeeChat.send(Strings.PLAYER_ISNT_NICKED, Prefixes.NICK, sender);
			return;
		}

		// Message
		BungeeChat.send(Strings.REALNAME_OF
				.replace("%pl", name)
				.replace("%name", profile.getName()), Prefixes.NICK, sender);
	}

}
