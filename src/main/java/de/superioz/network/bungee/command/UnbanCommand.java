package de.superioz.network.bungee.command;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.api.registry.ban.BanType;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.BanHandler;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.chat.TeamChat;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.bungee.util.ChatUtil;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 14.04.2016.
 */
@Command(label = "unban",
		desc = "Deletes given ban from mongo",
		permission = Permissions.COMMAND_UNBAN,
		flags = {"i", "n"},
		usage = "<reason> (-i <id> | -n <name> <type>)",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class UnbanCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("unban <Grund> -i <BannId>", "Löscht einen Bann", sender);
			HelpManager.sendLine("unban <Grund> -n <Name> <BannTyp>", "Entbannt einen Spieler", sender);
			return;
		}

		// Var
		String unbanned = null;
		BanType type = null;
		BanReason.Repeal unbanReason;

		// Get reason
		if(context.getArgumentsLength() < 1
				|| (unbanReason = BanReason.Repeal.fromName(context.getArgument(1))) == null){
			BungeeChat.send(Strings.AVAILABLE_REASONS
					.replace("%reasons", ListUtil.insert(BanReason.Repeal.getValues(), ", ")), Prefixes.BAN, sender);
			return;
		}
		boolean f = false;

		if(context.hasFlag("n")){
			CommandFlag flag = context.getCommandFlag("n");

			// Check name
			String name;
			if(flag.getArguments().size() < 1
					|| !ProfileManager.contains(name = flag.getArgument(1))){
				BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.BAN, sender);
				return;
			}
			PlayerProfile target = ProfileManager.getProfile(name);

			// Check type
			if(!(flag.getArguments().size() > 1)
					|| (type = BanType.fromName(flag.getArgument(2))) == null){
				BungeeChat.send(Strings.AVAILABLE_REASONS
						.replace("%reasons", ListUtil.insert(BanType.getValues(), ", ")), Prefixes.BAN, sender);
				return;
			}

			// Check is banned
			if(!BanManager.isBanned(target.getId(), type, true)){
				BungeeChat.send(Strings.PLAYER_NOT_BANNED, Prefixes.BAN, sender);
				return;
			}

			// Unban
			unbanned = target.getName();
			if(f = BanManager.unbanPlayer(target.getId(), type))
				BungeeChat.send(Strings.PLAYER_UNBANNED, Prefixes.BAN, sender);
			else
				BungeeChat.send("&cKonnte Spieler nicht entbannen.", Prefixes.BAN, sender);
		}
		else if(context.hasFlag("i")){
			CommandFlag flag = context.getCommandFlag("i");

			// Check id
			int id;

			Ban ban;
			if(flag.getArguments().size() < 1
					|| (ban = BanManager.getBanFromId(id = Integer.parseInt(flag.getArgument(1)))) == null){
				BungeeChat.send(Strings.NO_BAN_WITH_ID, Prefixes.BAN, sender);
				return;
			}

			// Unban
			unbanned = ProfileManager.getName(ban.getBannedId());
			type = ban.getType();
			if(f = BanManager.removeBan(id))
				BungeeChat.send(Strings.PLAYER_UNBANNED, Prefixes.BAN, sender);
			else
				BungeeChat.send("&cKonnte Spieler nicht entbannen.", Prefixes.BAN, sender);
		}
		else{
			context.sendUsage();
		}

		// Send message
		if(f){
			// Get color
			String senderColor = NetworkBungee.c.getString(ConfigManager.CHAT_CONSOLE_COLOR);
			if(sender instanceof ProxiedPlayer){
				senderColor = ChatManager.getColor(context.getSenderAsPlayer());
			}
			String targetColor = ChatManager.getColor(ProfileManager.getUniqueId(unbanned));

			assert unbanned != null;
			String message = Strings.TEAM_UNBANNED_PLAYER
					.replace("%sColor", senderColor)
					.replace("%sName", sender.getName())
					.replace("%tColor", targetColor)
					.replace("%tName", unbanned)
					.replace("%type", type.getName())
					.replace("%reason", unbanReason.name());
			TeamChat.sendMessage(ChatUtil.fabulize(message), Prefixes.BAN);
		}
	}

}
