package de.superioz.network.bungee.command;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.BanDuration;
import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.api.registry.ban.BanType;
import de.superioz.network.bungee.common.BanHandler;
import de.superioz.network.bungee.common.report.ReportReason;
import de.superioz.network.bungee.common.report.ReportReasonManager;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;

import java.util.List;

/**
 * Created on 16.04.2016.
 */
@Command(label = "tempban",
		desc = "Bans someone with specific time",
		flags = {"s", "m", "h", "d", "y", "p", "ip"},
		permission = Permissions.COMMAND_TEMPBAN,
		usage = "<name> <reason> <time> [-ip]",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class TempBanCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("tempban <Name> <Grund> <Zeit>", "Bannt einen Spieler temporär", sender);
			HelpManager.sendLine("tempban <Name> <Grund> <Zeit> -ip", "Bannt die IP eines Spielers temporär", sender);
			HelpManager.sendLine("tempban <Name> <Grund> -p", "Bannt einen Spieler permanent", sender);
			HelpManager.sendLine("tempban <Name> <Grund> -p -ip", "Bannt die IP eines Spielers permanent", sender);
			return;
		}

		// Check target
		if(!ProfileManager.contains(context.getArgument(1))){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.BAN, sender);
			return;
		}
		PlayerProfile target = ProfileManager.getProfile(context.getArgument(1));

		// Check reason
		ReportReason reason;
		if(context.getArgumentsLength() < 2
				|| (reason = ReportReasonManager.fromStringExcept(context.getArgument(2), BanReason.Parent.CHAT_BEHAVIOUR)) == null){
			List<ReportReason> reasons = ReportReasonManager.getReasonsExcept(BanReason.Parent.CHAT_BEHAVIOUR);
			BungeeChat.send(Strings.AVAILABLE_REASONS
					.replace("%reasons", ListUtil.insert(ReportReasonManager.getValues(reasons), ", ")), Prefixes.BAN, sender);
			return;
		}
		BanReason.Parent parent = reason.getParent();

		// Check time
		if(context.getArgumentsLength() < 3){
			List<String> flags = BanDuration.Type.getValues();

			BungeeChat.send("&7Verfügbare Zeiteinheiten: &e" + flags, Prefixes.BAN, sender);
			return;
		}
		long duration = BanDuration.fromCommandContext(context);

		// Get type
		BanType type = BanType.GLOBAL_UUID;
		if(context.hasFlag("ip")
				&& sender.hasPermission(Permissions.COMMAND_IPBAN)){
			type = BanType.GLOBAL_IP;
		}

		// Bans the user
		boolean b = BanHandler.banUser(sender, target, parent, reason, duration, type);
		if(b){
			BungeeChat.send(Strings.PLAYER_BANNED, Prefixes.BAN, sender);
		}
	}

}
