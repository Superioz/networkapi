package de.superioz.network.bungee.command;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.bungee.common.BanHandler;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;

/**
 * Created on 16.04.2016.
 */
@Command(label = "baninfo",
		desc = "Shows a ban information",
		permission = Permissions.COMMAND_BAN_INFO,
		usage = "(-n <name> | -i <id>)",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class BanInfoCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendHeader(context);
			HelpManager.sendLine("baninfo -n <Name>", "Informationen von einem Spieler", sender);
			HelpManager.sendLine("baninfo -i <BannId>", "Informationen eines bestimmten Banns", sender);
			return;
		}

		if(context.hasFlag("n")){
			this.sendPlayerBanInformation(context);
		}
		else if(context.hasFlag("i")){
			this.sendBanInformation(context);
		}
		else{
			context.sendUsage();
		}
	}

	/**
	 * Sends the ban information
	 *
	 * @param context The context
	 */
	private void sendBanInformation(CommandContext context){
		CommandSender sender = context.getSender();
		CommandFlag flag = context.getCommandFlag("i");
		Ban ban;

		if(flag.getArguments().size() < 1
				|| (ban = BanManager.getBanFromId(Integer.parseInt(flag.getArgument(1)))) == null){
			BungeeChat.send(Strings.NO_BAN_WITH_ID, Prefixes.BAN, sender);
			return;
		}

		// Ban information
		BungeeChat.send(Strings.BAN_INFORMATION_I_HEADER
				.replace("%id", ban.getHexId()), Prefixes.BAN, sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_PLAYER
				.replace("%color", ChatManager.getColor(ban.getBanned().getUniqueId()))
				.replace("%name", ban.getBanned().getName()), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_SENDER
				.replace("%color", ChatManager.getColor(ban.getBanSender().getUniqueId()))
				.replace("%name", ban.getBanSender().getName()), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_TYPE
				.replace("%type", ban.getType().getNiceName()), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_REASON
				.replace("%reason", ban.getChildReason()), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_FROM
				.replace("%date", TimeUtils.getCurrentTime(ban.getTimeFrom(), "dd.MM.yyyy HH:mm:ss")), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_UNTIL
				.replace("%date", TimeUtils.getCurrentTime(ban.getTimeUntil(), "dd.MM.yyyy HH:mm:ss")), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_I_HEADER
				.replace("%id", ban.getHexId()), Prefixes.BAN, sender);
	}

	/**
	 * Sends ban information for player given in context
	 *
	 * @param context The context
	 */
	private void sendPlayerBanInformation(CommandContext context){
		CommandSender sender = context.getSender();
		PlayerProfile profile;
		CommandFlag flag = context.getCommandFlag("n");

		if(flag.getArguments().size() < 1
				|| (profile = ProfileManager.getProfile(flag.getArgument(1))) == null){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.BAN, sender);
			return;
		}

		// Ban informationen
		List<Ban> bans = profile.getBans();

		if(bans == null || bans.isEmpty()){
			BungeeChat.send(Strings.PLAYER_WASNT_BANNED_EVER, Prefixes.BAN, sender);
			return;
		}
		int banSize = bans.size();
		String mostBanType = banSize == 0 ? "/" : profile.getMostBanType().getName();
		String chatBanned = Strings.BAN_INFORMATION_P_CHAT + " " + (profile.isChatBanned() ? "&aJa" : "&cNein");
		String globalBanned = Strings.BAN_INFORMATION_P_GLOBAL + " " + (profile.isGlobalBanned() ? "&aJa" : "&cNein");

		BungeeChat.send(Strings.BAN_INFORMATION_P_HEADER
				.replace("%name", profile.getName()), Prefixes.BAN, sender);
		BungeeChat.send(Strings.BAN_INFORMATION_P_SIZE
				.replace("%size", banSize + ""), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_P_POINTS
				.replace("%points", BanManager.getBanPoints(profile.getId()) + ""), sender);
		BungeeChat.send(Strings.BAN_INFORMATION_P_MOSTTYPE
				.replace("%type", mostBanType), sender);

		// Ban state
		if(profile.isChatBanned()){
			chatBanned += " &7(%id%&7)";
			Ban chatBan = BanManager.getChatBan(profile.getId());

			if(sender instanceof ProxiedPlayer){
				BungeeChat.send(new WrappedMessage(chatBanned)
						.replace("id", "&a" + chatBan.getHexId(),
								new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/baninfo -i " + chatBan.getId()),
								new HoverEvent(HoverEvent.Action.SHOW_TEXT,
										TextComponent.fromLegacyText(Strings.CLICK_FOR_MORE_BAN_INFOS)))
						.applyReplacements(), sender);
			}
			else{
				chatBanned = chatBanned.replace("%id%", "&a" + chatBan.getHexId());
				BungeeChat.send(chatBanned, sender);
			}
		}
		else{
			BungeeChat.send(chatBanned, sender);
		}

		// Is global banned
		if(profile.isGlobalBanned()){
			globalBanned += " &7(%id%&7)";
			Ban globalBan = BanManager.getBan(profile.getId());

			if(sender instanceof ProxiedPlayer)
				BungeeChat.send(new WrappedMessage(globalBanned)
						.replace("id", "&a" + globalBan.getHexId(),
								new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/baninfo -i " + globalBan.getId()),
								new HoverEvent(HoverEvent.Action.SHOW_TEXT,
										TextComponent.fromLegacyText(Strings.CLICK_FOR_MORE_BAN_INFOS)))
						.applyReplacements(), sender);
			else{
				globalBanned = globalBanned.replace("%id%", "&a" + globalBan.getHexId());
				BungeeChat.send(globalBanned, sender);
			}
		}
		else{
			BungeeChat.send(globalBanned, sender);
		}

		BungeeChat.send(Strings.BAN_INFORMATION_P_HEADER
				.replace("%name", profile.getName()), Prefixes.BAN, sender);
	}

}
