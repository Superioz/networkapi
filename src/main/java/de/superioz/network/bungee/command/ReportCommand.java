package de.superioz.network.bungee.command;

import de.superioz.network.bungee.common.report.ReportHandler;
import de.superioz.network.bungee.common.report.ReportReason;
import de.superioz.network.bungee.common.report.ReportReasonManager;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.bungee.command.verification.MethodVerifier;
import de.superioz.sx.bungee.command.verification.MethodVerifierEvent;
import de.superioz.sx.java.util.Consumer;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 09.04.2016.
 */
@Command(label = "report",
		desc = "Command for reporting players",
		permission = Permissions.COMMAND_REPORT,
		usage = "<player> <reason>",
		commandTarget = AllowedCommandSender.PLAYER)
public class ReportCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();
		final ProxiedPlayer player = context.getSenderAsPlayer();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("report <Name> <Grund>", sender);
			return;
		}


		// Reason info
		if(context.getArgumentsLength() == 1){
			// Show reasons
			BungeeChat.send(Strings.AVAILABLE_REASONS
					.replace("%reasons", ListUtil.insert(ReportReasonManager.getValues(), ", ")), Prefixes.REPORT_USER, player);
			return;
		}

		// Check target
		final ProxiedPlayer target = BungeeLibrary.proxy().getPlayer(context.getArgument(1));
		if(target == null){
			BungeeChat.send(Strings.PLAYER_NOT_ONLINE, Prefixes.REPORT_USER, player);
			return;
		}
		if(target.getUniqueId().equals(player.getUniqueId())
				|| target.hasPermission(Permissions.REPORT_BYPASS)){
			BungeeChat.send(Strings.YOU_CANNOT_REPORT_THIS, Prefixes.REPORT_USER, player);
			return;
		}

		// Check reason
		final ReportReason reason = ReportReasonManager.fromString(context.getArgument(2));
		if(reason == null){
			BungeeChat.send(Strings.REASON_DOESNT_EXIST, Prefixes.REPORT_USER, player);
			return;
		}
		if(ReportHandler.hasReport(player, target)){
			BungeeChat.send(Strings.ALREADY_REPORTED, Prefixes.REPORT_USER, player);
			return;
		}

		// Verify to report player
		int id = MethodVerifier.verify(player, new Consumer<MethodVerifierEvent>() {
			@Override
			public void accept(MethodVerifierEvent event){
				if(event.isResult()){
					ReportHandler.addReport(player, target, reason);
					BungeeChat.send(Strings.PLAYER_REPORTED, Prefixes.REPORT_USER, player);
				}
				else{
					BungeeChat.send(Strings.REPORT_CANCELLED, Prefixes.REPORT_USER, player);
				}
			}
		});

		// Send message to accept
		String accept = "/" + MethodVerifier.getCommand(id, true);
		String decline = "/" + MethodVerifier.getCommand(id, false);
		WrappedMessage msg = new WrappedMessage(Strings.SURE_TO_REPORT)
				.replace("y", "&aJa",
						new ClickEvent(ClickEvent.Action.RUN_COMMAND, accept),
						new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Strings.CLICK_TO_ACCEPT)))
				.replace("n", "&cNein",
						new ClickEvent(ClickEvent.Action.RUN_COMMAND, decline),
						new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Strings.CLICK_TO_DECLINE)))
				.applyReplacements();
		BungeeChat.send(msg, Prefixes.REPORT_USER, player);
	}

}
