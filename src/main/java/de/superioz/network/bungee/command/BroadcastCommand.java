package de.superioz.network.bungee.command;

import de.superioz.network.bungee.common.chat.ChatLog;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 09.04.2016.
 */
@Command(label = "broadcast",
		aliases = {"bc"},
		desc = "Sends a message to all server",
		permission = Permissions.COMMAND_BROADCAST,
		usage = "<message>",
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class BroadcastCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender sender = context.getSender();

		// Check help
		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("broadcast <Nachricht>", sender);
			return;
		}

		String pattern = Strings.PATTERN_BROADCAST;
		String message = ListUtil.insert(context.getArguments(), " ");
		pattern = pattern.replace("%msg", message);
		BungeeChat.broadcast(pattern);

		if(context.getSender() instanceof ProxiedPlayer){
			//
			// Logging
			//
			ChatLog.log(context.getSenderAsPlayer(),
					"/" + context.getCommand().getLabel() + " " + message, ChatLog.Level.DEBUG);
		}
	}

}
