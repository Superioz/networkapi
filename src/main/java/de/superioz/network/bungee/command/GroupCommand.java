package de.superioz.network.bungee.command;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PermissionWrap;
import de.superioz.network.bungee.util.HelpManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.ListUtil;
import de.superioz.sx.java.util.PageableList;
import net.md_5.bungee.api.CommandSender;

/**
 * Created on 01.05.2016.
 */
@Command(label = "group",
		aliases = {"g"},
		flags = {"n", "r", "cp", "cs", "tp", "ts",
				"c", "d", "p", "i", "?", "t"},
		desc = "Commands for groups",
		permission = Permissions.COMMAND_PERMISSION_GROUP,
		commandTarget = AllowedCommandSender.PLAYER_AND_CONSOLE)
public class GroupCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		CommandSender player = context.getSender();
		PermissionGroup group;

		if(context.getArgumentsLength() == 0){
			HelpManager.sendOneLine("group -? [Seite]", player);
			return;
		}

		// Check help
		if(context.hasFlag("?")){
			CommandFlag flag = context.getCommandFlag("?");
			int page = flag.getInt(1, 1);
			HelpManager.sendHelp(player, HelpManager.Type.GROUP_SYSTEM, page);
			return;
		}

		// Check group
		if((group = GroupManager.getGroup(context.getArgument(1))) == null){
			BungeeChat.send(Strings.GROUP_DOESNT_EXIST, Prefixes.PERM, player);
			return;
		}

		// Check subCommand
		if(context.getArguments().length < 2){
			BungeeChat.send(Strings.SUB_COMMAND_DOESNT_EXIST, Prefixes.PERM, player);
			return;
		}
		String subCommand = context.getArgument(2);

		// Shows information about the group
		if(subCommand.equalsIgnoreCase("info")){
			this.info(player, group);
		}

		// Modifies values of the group
		else if(subCommand.equalsIgnoreCase("modify")){
			this.modify(group, context);
			BungeeChat.send(Strings.GROUP_DATA_UPDATED, Prefixes.PERM, player);
		}

		// Adds permissions and/or inheritances
		else if(subCommand.equalsIgnoreCase("add")){
			this.add(group, context);
			BungeeChat.send(Strings.GROUP_DATA_UPDATED, Prefixes.PERM, player);
		}

		// Removes permissions and/or inheritances
		else if(subCommand.equalsIgnoreCase("remove")){
			this.remove(group, context);
			BungeeChat.send(Strings.GROUP_DATA_UPDATED, Prefixes.PERM, player);
		}

		// List permissions/inheritances of this group
		else if(subCommand.equalsIgnoreCase("list")){
			this.list(player, group, context);
		}
		else{
			BungeeChat.send(Strings.SUB_COMMAND_DOESNT_EXIST, Prefixes.PERM, player);
		}
	}

	/**
	 * Lists all permissions or inheritances of this group
	 *
	 * @param group   The group
	 * @param context The context
	 */
	public void list(CommandSender player, PermissionGroup group, CommandContext context){
		if(context.hasFlag("p")){
			CommandFlag f = context.getCommandFlag("p");

			if(group.getPermissions().size() == 0){
				BungeeChat.send(Strings.GROUP_HASNT_PERM, Prefixes.PERM, player);
				return;
			}
			PageableList<PermissionWrap> list = new PageableList<>(10, group.getPermissions());
			int page = f.getInt(1, 1);

			// Check page
			if(!list.firstCheckPage(page)){
				BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.PERM, player);
				return;
			}

			String header = Strings.PERMISSION_LIST_OF
					.replace("%group", group.getName())
					.replace("%page", page + "")
					.replace("%max", list.getTotalPages() + "");
			BungeeChat.send(header, Prefixes.PERM, player);
			for(PermissionWrap wrap : list.calculatePage(page)){
				if(wrap == null) continue;
				String perm = wrap.getPermission();
				perm = (wrap.isNegatePermission() ? "&c" : "&a") + perm;

				BungeeChat.send(Strings.PERMISSION_LIST_OF_PART
						.replace("%perm", perm)
						.replace("%type", wrap.getType().getNiceName()), player);
			}
			BungeeChat.send(header, Prefixes.PERM, player);
		}
		else if(context.hasFlag("i")){
			CommandFlag f = context.getCommandFlag("i");

			if(group.getInheritances().size() == 0){
				BungeeChat.send(Strings.GROUP_HASNT_INHERITANCES, Prefixes.PERM, player);
				return;
			}
			PageableList<String> list = new PageableList<>(10, group.getInheritances());
			int page = f.getInt(1, 1);

			// Check page
			if(!list.firstCheckPage(page)){
				BungeeChat.send(Strings.PAGE_DOESNT_EXIST, Prefixes.PERM, player);
				return;
			}

			String header = Strings.INHERITANCE_LIST_OF
					.replace("%group", group.getName())
					.replace("%page", page + "")
					.replace("%max", list.getTotalPages() + "");
			BungeeChat.send(header, Prefixes.PERM, player);
			for(String s : list.calculatePage(page)){
				if(s == null) continue;
				PermissionGroup g = GroupManager.getGroup(s);

				if(g != null)
					BungeeChat.send(Strings.INHERITANCE_LIST_OF_PART
							.replace("%name", g.getName())
							.replace("%rank", g.getRank() + ""), player);
			}
			BungeeChat.send(header, Prefixes.PERM, player);
		}
	}

	/**
	 * Removes permissions or inheritances to given group
	 *
	 * @param group   The group
	 * @param context The context
	 */
	public void remove(PermissionGroup group, CommandContext context){
		PermissionType type = PermissionType.BUKKIT;
		if(context.hasFlag("t")){
			type = PermissionType.values()[context.getInt(1, 0)];
		}

		if(context.hasFlag("p")){
			CommandFlag f = context.getCommandFlag("p");
			for(String s : f.getArguments()){
				group.removePermission(s, type);
			}
		}
		if(context.hasFlag("i")){
			CommandFlag f = context.getCommandFlag("i");
			for(String s : f.getArguments()){
				group.removeInheritance(s);
			}
		}
		group.updateData();
	}

	/**
	 * Adds permissions or inheritances to given group
	 *
	 * @param group   The group
	 * @param context The context
	 */
	public void add(PermissionGroup group, CommandContext context){
		PermissionType type = PermissionType.BUKKIT;
		if(context.hasFlag("t")){
			type = PermissionType.values()[context.getCommandFlag("t").getInt(1, 0)];
		}

		if(context.hasFlag("p")){
			CommandFlag f = context.getCommandFlag("p");
			for(String s : f.getArguments()){
				group.addPermission(s, type);
			}
		}
		if(context.hasFlag("i")){
			CommandFlag f = context.getCommandFlag("i");
			for(String s : f.getArguments()){
				group.addInheritance(s);
			}
		}
		group.updateData();
	}

	/**
	 * Modifies given group with given values
	 *
	 * @param group   The group
	 * @param context The context with the values
	 */
	public int modify(PermissionGroup group, CommandContext context){
		int ind = 0;

		// Set the name of the group
		if(context.hasFlag("n")){
			CommandFlag flag = context.getCommandFlag("n");
			String name;

			if(flag.getArgumentsLength() < 1
					|| (name = flag.getArgument(1)).isEmpty()){
				return 0;
			}
			group.setName(name);
			ind++;
		}

		// Set the rank of the group
		if(context.hasFlag("r")){
			CommandFlag flag = context.getCommandFlag("r");
			int r = flag.getInt(1, 1);
			group.setRank(r);
			ind++;
		}

		// Set the chatprefix of the group
		if(context.hasFlag("cp")){
			CommandFlag flag = context.getCommandFlag("cp");
			group.setChatPrefix(ListUtil.insert(flag.getArguments(), " "));
			ind++;
		}

		// Set the chat suffix of the group
		if(context.hasFlag("cs")){
			CommandFlag flag = context.getCommandFlag("cs");
			group.setChatSuffix(ListUtil.insert(flag.getArguments(), " "));
			ind++;
		}

		// Set the tab prefix
		if(context.hasFlag("tp")){
			CommandFlag flag = context.getCommandFlag("tp");
			group.setTabPrefix(ListUtil.insert(flag.getArguments(), " "));
			ind++;
		}

		// Set the tab suffix
		if(context.hasFlag("ts")){
			CommandFlag flag = context.getCommandFlag("ts");
			group.setTabSuffix(ListUtil.insert(flag.getArguments(), " "));
			ind++;
		}

		// Set the color
		if(context.hasFlag("c")){
			CommandFlag flag = context.getCommandFlag("c");
			String arg = "";

			if(flag.getArgumentsLength() > 0){
				arg = flag.getArgument(1);
			}
			group.setColor(arg);
			ind++;
		}

		// Set if the group is the default group
		if(context.hasFlag("d")){
			CommandFlag flag = context.getCommandFlag("d");
			boolean val = flag.getBoolean(1, false);
			group.setDefault(val);
			ind++;
		}
		group.updateData();
		return ind;
	}

	/**
	 * Sends group information from given group to given player
	 *
	 * @param player The player
	 * @param group  The group
	 */
	public void info(CommandSender player, PermissionGroup group){
		// Send message
		BungeeChat.send(Strings.GROUP_INFORMATION_HEADER
				.replace("%group", group.getName()), Prefixes.PERM, player);
		BungeeChat.send(Strings.GROUP_INFORMATION_RANK
				.replace("%rank", group.getRank()+""), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_COLOR
				.replace("%color", group.getColor()), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_CPREFIX
				.replace("%s", group.getChatPrefix()), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_CSUFFIX
				.replace("%s", group.getChatSuffix()), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_TPREFIX
				.replace("%s", group.getTabPrefix()), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_TSUFFIX
				.replace("%s", group.getTabSuffix()), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_ISDEFAULT
				.replace("%s", group.isDefault() ? "&aJa" : "&cNein"), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_ISARTIFACT
				.replace("%s", group.isArtificially() ? "&aJa" : "&cNein"), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_PERMISSIONS
				.replace("%v", group.getPermissions().size()+""), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_INHERITANCES
				.replace("%v", group.getInheritances().size()+""), player);
		BungeeChat.send(Strings.GROUP_INFORMATION_HEADER
				.replace("%group", group.getName()), Prefixes.PERM, player);
	}
}
