package de.superioz.network.bungee.event;

import de.superioz.network.api.registry.Friend;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created on 01.05.2016.
 */
@Getter
public class FriendUpdateEvent extends Event {

	private Friend player;
	private Friend target;
	private Type type;

	/**
	 * Calls this event when @player adds/removes friend @target
	 *
	 * @param type   Type of update
	 * @param player The player who added/removed given target
	 * @param target The target friend
	 */
	public FriendUpdateEvent(Type type, Friend player, Friend target){
		this.type = type;
		this.player = player;
		this.target = target;
	}

	/**
	 * Event type
	 */
	public enum Type {

		ADD_FRIEND,
		REMOVE_FRIEND,

	}

}
