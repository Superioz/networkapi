package de.superioz.network.bungee.event;

import de.superioz.network.api.registry.ban.Ban;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created on 01.05.2016.
 */
@Getter
public class BanPlayerEvent extends Event implements Cancellable {

	private Ban ban;
	private boolean cancelled = false;

	public BanPlayerEvent(Ban ban){
		this.ban = ban;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		this.cancelled = b;
	}


}
