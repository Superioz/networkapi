package de.superioz.network.bungee.event;

import de.superioz.network.api.registry.Friend;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created on 01.05.2016.
 */
@Getter
public class FriendStateChangeEvent extends Event {

	private Friend friend;
	private State state;

	/**
	 * Fires when a friend goes online/offline
	 *
	 * @param friend Who changed his state?
	 * @param state  The new state (e.g. from offline -> online)
	 */
	public FriendStateChangeEvent(Friend friend, State state){
		this.friend = friend;
		this.state = state;
	}

	/**
	 * State of state changed event
	 */
	public enum State {

		ONLINE,
		OFFLINE

	}

}
