package de.superioz.network.bungee.event;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created on 01.05.2016.
 */
@Getter
public class ChatSendEvent extends Event implements Cancellable {

	private boolean cancelled = false;
	private ProxiedPlayer sender;
	private String message;
	private Channel channel;

	public ChatSendEvent(ProxiedPlayer sender, Channel channel, String message){
		this.sender = sender;
		this.channel = channel;
		this.message = message;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}

	public enum Channel {

		BROADCAST,
		CHAT,
		TEAM

	}

}
