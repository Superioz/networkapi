package de.superioz.network.bungee.event;

import de.superioz.network.bungee.common.chat.ChatLog;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created on 01.05.2016.
 */
@Getter
public class ChatLogEvent extends Event implements Cancellable {

	private String message;
	private ProxiedPlayer player;
	private ChatLog.Level level;
	private boolean cancelled = false;

	/**
	 * Fires when the message for given player should be logged
	 *
	 * @param message The message to log (e.g. a message he sent)
	 * @param player  The player
	 * @param level   The log level
	 */
	public ChatLogEvent(String message, ProxiedPlayer player, ChatLog.Level level){
		this.message = message;
		this.player = player;
		this.level = level;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}
}

