package de.superioz.network.bungee.event;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 02.05.2016.
 */
@Getter
public class SetTabHeaderEvent extends Event implements Cancellable {

	private List<String> headerLines;
	private List<String> footerLines;
	private ProxiedPlayer player;
	private Server server;

	private boolean cancelled = false;

	public SetTabHeaderEvent(List<String> headerLines, List<String> footerLines, ProxiedPlayer player, Server server){
		this.headerLines = getLines(headerLines);
		this.footerLines = getLines(footerLines);
		this.player = player;
		this.server = server;
	}

	/**
	 * Get formatted lines
	 *
	 * @param lines The lines
	 * @return The list of strings
	 */
	private List<String> getLines(List<String> lines){
		List<String> l = new ArrayList<>();
		for(int i = 0; i < lines.size() - 1; i++){
			l.add(lines.get(i) + "\n");
		}
		l.add(lines.get(lines.size() - 1));
		return l;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}
}
