package de.superioz.network.bungee.common;

import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.chat.TeamChat;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.util.ChatUtil;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;

/**
 * Created on 04.05.2016.
 */
public class KickHandler {

	/**
	 * Kick given player
	 *
	 * @param player The player
	 * @param reason The reason
	 */
	public static void kickPlayer(CommandSender from, ProxiedPlayer player, String reason){
		BaseComponent component = new WrappedMessage(getMessage(reason)).duplicate();
		player.disconnect(component);

		// Send message to team
		TeamChat.sendMessage(ChatUtil.colored(Strings.PATTERN_KICK
				.replace("%sender", from.getName())
				.replace("%c", ChatManager.getColor(player))
				.replace("%p", player.getName())
				.replace("%reason", reason)), Prefixes.TEAM);
	}

	/**
	 * Get message as string
	 *
	 * @param reason The reason
	 * @return The string
	 */
	private static String getMessage(String reason){
		List<String> lines = LanguageManager.getKickMessageFile().readln();
		lines = ListUtil.replace(lines, "%reason", reason);
		return ListUtil.insert(lines, "\n");
	}

}
