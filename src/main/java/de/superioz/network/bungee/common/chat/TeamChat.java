package de.superioz.network.bungee.common.chat;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.GroupManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.chat.SpecialCharacter;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.bungee.util.ChatUtil;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 08.04.2016.
 */
public class TeamChat {

	/**
	 * Gets the message to send with specific sender
	 *
	 * @param sender  The sender
	 * @param message The message
	 */
	private static String getMessage(ProxiedPlayer sender, String message){
		String newMessage = Strings.PATTERN_TEAM_CHAT;

		// Check permissions
		message = sender.hasPermission(Permissions.CHAT_COLOR_CODES) ? ChatUtil.colored(message) : message;
		message = sender.hasPermission(Permissions.CHAT_SPECIAL_CHARS) ? SpecialCharacter.apply(message) : message;

		// Get color
		PermissionGroup group = GroupManager.getGroup(sender.getUniqueId());
		if(group != null){
			String color = group.getColor();
			newMessage = newMessage.replace("%color", color);
		}

		// Replace variables
		newMessage = newMessage.replace("%name", sender.getName());
		newMessage = ChatUtil.fabulize(newMessage);
		newMessage = newMessage.replace("%msg", message);

		return newMessage;
	}

	/**
	 * Logs the message to teamchat
	 *
	 * @param message The message
	 * @param level   The logging level
	 */
	public static void log(String message, LoggingLevel level, String prefix){
		if(!isLogging() || !(level == getLoggingLevel())){
			return;
		}
		sendMessage(ChatUtil.fabulize(message), prefix);
	}

	/**
	 * Sends given message to teamchat for given sender
	 *
	 * @param sender The sender
	 * @param msg    The message
	 */
	public static void sendMessageFrom(String msg, ProxiedPlayer sender){
		String message = getMessage(sender, msg);
		sendMessage(message, Prefixes.TEAM);
	}

	/**
	 * Sends a message to the team
	 *
	 * @param component The message
	 */
	public static void sendMessage(BaseComponent[] component){
		if(!isEnabled()){
			return;
		}
		List<ProxiedPlayer> teamMembers = getTeamMembers();

		// Sending
		BungeeChat.send(component, teamMembers.toArray(new ProxiedPlayer[teamMembers.size()]));
	}

	public static void sendMessage(WrappedMessage message){
		if(!isEnabled()){
			return;
		}
		List<ProxiedPlayer> teamMembers = getTeamMembers();

		// Sending
		BungeeChat.send(message, teamMembers.toArray(new ProxiedPlayer[teamMembers.size()]));
	}

	public static void sendMessage(String message){
		sendMessage(TextComponent.fromLegacyText(message));
	}

	/**
	 * Sends message to team with prefix
	 *
	 * @param message The message
	 * @param prefix  The prefix
	 */
	public static void sendMessage(BaseComponent[] message, String prefix){
		prefix = prefix + " &r&7";
		prefix = ChatUtil.fabulize(prefix);
		String fullMessage = TextComponent.toLegacyText(message);

		sendMessage(prefix + fullMessage);
	}

	public static void sendMessage(WrappedMessage message, String prefix){
		sendMessage(message.setPrefix(prefix, null, null));
	}

	public static void sendMessage(String message, String prefix){
		prefix = prefix + " &r&7";
		prefix = ChatUtil.fabulize(prefix);

		sendMessage(prefix + message);
	}

	/**
	 * Get the logging level
	 *
	 * @return Result
	 */
	public static LoggingLevel getLoggingLevel(){
		int level = NetworkBungee.c.getConfig().getInt(ConfigManager.CHAT_TEAM_LOGGING_LEVEL);

		if(level <= 1){
			return LoggingLevel.EVERYTHING;
		}
		else{
			return LoggingLevel.ONLY_IMPORTANT;
		}
	}

	/**
	 * Get all members with teamchat permission
	 *
	 * @return The list of player
	 */
	public static List<ProxiedPlayer> getTeamMembers(){
		List<ProxiedPlayer> user = new ArrayList<>();

		for(ProxiedPlayer p : NetworkBungee.h.getProxy().getPlayers()){
			if(!BanManager.isBanned(ProfileManager.getProfile(p.getUniqueId()))
					&& !BanManager.isBanned(p.getAddress().getAddress().getHostAddress())
					&& p.hasPermission(Permissions.CHAT_TEAMCHAT)){
				user.add(p);
			}
		}
		return user;
	}

	/**
	 * Checks if the teamchat logging is enabled
	 *
	 * @return The result
	 */
	public static boolean isLogging(){
		return NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_TEAM_LOGGING);
	}

	/**
	 * Checks if the teamchat is enabled
	 *
	 * @return The result
	 */
	public static boolean isEnabled(){
		return NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_TEAM_ENABLED);
	}

	/**
	 * Level of logging
	 */
	public enum LoggingLevel {

		EVERYTHING(1),
		ONLY_IMPORTANT(2);

		private int i;

		LoggingLevel(int i){
			this.i = i;
		}

		public int getLevel(){
			return i;
		}

	}

}
