package de.superioz.network.bungee.common;

import de.superioz.network.api.mongo.MongoConfigData;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.util.*;
import lombok.Getter;
import net.md_5.bungee.config.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Created on 29.04.2016.
 */
@Getter
public class ConfigManager {

	public static final String MAX_BAN_POINTS = "maxBanPoints";
	public static final String DEBUG_MODE = "debugMode";
	public static final String DATABASE_PREFIX = "mongo.prefix";
	public static final String DATABASE_HOSTNAME = "mongo.hostname";
	public static final String DATABASE_PORT = "mongo.port";
	public static final String DATABASE_DATABASE = "mongo.database";
	public static final String DATABASE_USERNAME = "mongo.username";
	public static final String DATABASE_PASSWORD = "mongo.password";
	public static final String CHAT_CONSOLE_COLOR = "chat.consoleColor";
	public static final String CHAT_PATTERN = "chat.pattern";
	public static final String CHAT_CAPS_PROTECTION = "chat.caps-protection";
	public static final String CHAT_SPAM_PROTECTION = "chat.spam-protection";
	public static final String CHAT_MESSAGE_DELAY = "chat.message-delay";
	public static final String CHAT_BLOCKED_WORDS_ENABLED = "chat.blocked-words.enabled";
	public static final String CHAT_BLOCKED_WORDS_LIST = "chat.blocked-words.list";
	public static final String CHAT_ANTI_ADVERTISEMENT = "chat.anti-advertisement.enabled";
	public static final String CHAT_CHATLOG = "chat.chatlog.enabled";
	public static final String CHAT_CHATLOG_LEVEL = "chat.chatlog.level";
	public static final String CHAT_TEAM_ENABLED = "chat.teamchat.enabled";
	public static final String CHAT_TEAM_LOGGING = "chat.teamchat.logging.enabled";
	public static final String CHAT_TEAM_LOGGING_LEVEL = "chat.teamchat.logging.level";
	public static final String REPORT_REASONS = "report.reasons.";
	public static final String TABLIST_HEADER = "tabHeader";
	public static final String TABLIST_FOOTER = "tabFooter";
	public static final String MOTD = "motd";
	public static final String FAVICON = "favicon";

	private ConfigurationProvider provider;
	private Configuration config;
	private File file;

	private String fileName;
	private String resourcePath;
	private MongoConfigData configData;

	/**
	 * Class to manage a configManager file in bungeecord
	 *
	 * @param fileName The name of the file
	 */
	public ConfigManager(String fileName, String resourcePath){
		this.fileName = fileName;
		this.resourcePath = resourcePath;
		this.load();
		new File(NetworkBungee.h.getDataFolder() + "/favicons").mkdirs();
	}

	/**
	 * Loads the configuration
	 */
	public void load(){
		this.loadFile(getFileName());
		this.provider = ConfigurationProvider.getProvider(YamlConfiguration.class);

		// Load the configManager
		try{
			this.config = provider.load(file);
		}
		catch(IOException e){
			e.printStackTrace();
		}

		// Load properties
		LanguageManager.initialise();
		Strings.load();
		Prefixes.load();
		HelpManager.init();
	}

	/**
	 * Reloads the file
	 */
	public void reload(){
		this.load();
	}

	/**
	 * Saves the file
	 */
	public void save(){
		try{
			getProvider().save(getConfig(), getFile());
		}
		catch(IOException e){
			//
		}
	}

	/**
	 * Get a string with replaced variables
	 *
	 * @param path The path
	 * @return The string
	 */
	public String getString(String path){
		return getConfig().getString(path);
	}

	/**
	 * Loads the file to manage with given filename (in datafolder)
	 *
	 * @param fileName The filename
	 */
	private void loadFile(String fileName){
		NetworkBungee manager = NetworkBungee.h;
		File dataFolder = manager.getDataFolder();

		if(!dataFolder.exists()){
			dataFolder.mkdirs();
		}
		File file = new File(dataFolder, fileName);
		String resource = getResourcePath().isEmpty() ? fileName : getResourcePath() + "/" + fileName;

		if(!file.exists()){
			try(InputStream in = manager.getResourceAsStream(resource)){
				Files.copy(in, file.toPath());
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}

		this.file = file;
	}

//	private void copyDefaultsFrom(InputStream in, File file){
//		if(in != null && file != null){
//			try{
//				FileOutputStream e = new FileOutputStream(file);
//				byte[] buffer = new byte[1024];
//
//				int len;
//				while((len = in.read(buffer)) > 0){
//					e.write(buffer, 0, len);
//				}
//
//				e.close();
//				in.close();
//			}
//			catch(Exception var6){
//				var6.printStackTrace();
//			}
//
//		}
//	}

	/**
	 * This method gets all important things for the mongoDatabase
	 * from the config. It'll be used to send it across the network
	 * to synchronize the database.
	 *
	 * @return The data object
	 */
	public MongoConfigData getDatabaseConfig(boolean override){
		if(override || configData == null){
			configData = new MongoConfigData(
					config.getString(DATABASE_PREFIX),
					config.getString(DATABASE_HOSTNAME),
					config.getInt(DATABASE_PORT),
					config.getString(DATABASE_DATABASE),
					config.getString(DATABASE_USERNAME),
					config.getString(DATABASE_PASSWORD)
			);
		}
		return configData;
	}

}
