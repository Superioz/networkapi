package de.superioz.network.bungee.common;

import de.superioz.sx.java.util.IdentifierUtil;

import java.util.HashMap;

/**
 * Created on 30.04.2016.
 */
public class IDManager {

	private static HashMap<Type, Integer> identifierMap = new HashMap<>();
	private static final int ID_MIN_LENGTH = 4;

	/**
	 * Generates a new id
	 *
	 * @param type The type
	 * @return The id
	 */
	public static String generateId(Type type){
		int currentId;
		if(!identifierMap.containsKey(type)){
			currentId = 1;
		}
		else{
			currentId = identifierMap.get(type) + 1;
		}
		identifierMap.put(type, currentId);
		return IdentifierUtil.toString(currentId, ID_MIN_LENGTH);
	}

	/**
	 * Get the id from given string
	 *
	 * @param s The string
	 * @return The id
	 */
	public static int fromId(String s){
		return IdentifierUtil.parseInt(s);
	}

	// The type of id
	public enum Type {

		REPORT

	}

}
