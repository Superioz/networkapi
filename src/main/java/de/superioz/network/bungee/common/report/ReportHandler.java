package de.superioz.network.bungee.common.report;

import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.java.util.ListUtil;
import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 09.04.2016.
 */
public class ReportHandler {

	@Getter
	private static List<Report> reports = new ArrayList<>();

	/**
	 * Adds a report to the list and informs the team
	 *
	 * @param sender The sender
	 * @param target The target
	 * @param reason The reason
	 */
	public static void addReport(ProxiedPlayer sender, ProxiedPlayer target, ReportReason reason){
		Report report = new Report(reason, target.getServer().getInfo(), sender, target);
		reports.add(report);

		// Send message to team
//		String fullMessage = LanguageManager.getString()USER_REPORTED_USER.replace("%sender", sender.getName())
//				.replace("%reason", reason.getNiceName());
//		WrappedMessage message = new WrappedMessage(fullMessage)
//				.replace("target", ChatManager.getColor(target) + target.getName(),
//						new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/jump " + target.getName()),
//						new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(LanguageManager.getString()CLICK_TO_TELEPORT)))
//				.replace("id", "&a" + report.getId(),
//						new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reportinfo -i " + report.getId()),
//						new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(LanguageManager.getString()CLICK_FOR_MORE_REPORT_INFO)))
//				.applyReplacements();
//		TeamChat.sendMessage(message, Prefixes.(REPORT_TEAM);
	}

	/**
	 * Get map with per cents and reason
	 *
	 * @return The map
	 */
	public static HashMap<BanReason.Parent, Double> getReasonsPerCent(){
		HashMap<BanReason.Parent, Double> map = new HashMap<>();
		List<Report> reports = getReports();
		int reportSize = reports.size();

		// Get the reports
		for(ReportReason r : ReportReasonManager.getReasons()){
			int size = getReports(r).size();
			double percent = (size / reportSize) * 100;
			map.put(r.getParent(), (double) Math.round(percent * 100) / 100);
		}

		return map;
	}

	/**
	 * Get reports from given reason
	 *
	 * @param reason The reason
	 * @return The list of reports
	 */
	public static List<Report> getReports(ReportReason reason){
		List<Report> reports = new ArrayList<>();
		for(Report r : getReports()){
			if(r.getReason() == reason){
				reports.add(r);
			}
		}
		return reports;
	}

	/**
	 * Get reports where given is the target (size)
	 *
	 * @param target The target
	 * @return The size
	 */
	public static int getReportTargetSize(ProxiedPlayer target){
		return getReportsFromTarget(target).size();
	}

	/**
	 * Gets a report from given id
	 *
	 * @param reportId The id
	 * @return The report
	 */
	public static Report getReportFromId(String reportId){
		for(Report r : getReports()){
			if(r.getId().equalsIgnoreCase(reportId)){
				return r;
			}
		}
		return null;
	}

	/**
	 * Get the time diff from first to last report
	 *
	 * @param target The report target
	 * @return The diff in hours
	 */
	public static double getReportTargetTimeDiff(ProxiedPlayer target){
		Report first = getReportFromTimeAndTarget(target, true);
		Report last = getReportFromTimeAndTarget(target, false);

		double diff = last.getTimeStamp() - first.getTimeStamp();
		double seconds = diff / 1000D;
		return seconds / 60D / 60D;
	}

	/**
	 * Get the first report for this player
	 *
	 * @param target The target
	 * @return The report
	 */
	public static Report getReportFromTimeAndTarget(ProxiedPlayer target, boolean before){
		long timeStamp = -1;
		Report report = null;

		for(Report r : getReportsFromTarget(target)){
			long t = r.getTimeStamp();
			if(timeStamp == -1
					|| (before && t < timeStamp)
					|| (!before && t > timeStamp)){
				timeStamp = t;
				report = r;
			}
		}
		return report;
	}

	/**
	 * Get reports where given is the sender (size)
	 *
	 * @param sender The sender
	 * @return The size
	 */
	public static int getReportSenderSize(ProxiedPlayer sender){
		return getReportsFromSender(sender).size();
	}

	/**
	 * Get the reason which was most choosen in reports against given player
	 *
	 * @param target The player
	 * @return The reason
	 */
	public static ReportReason getMostReason(ProxiedPlayer target){
		List<ReportReason> reasons = new ArrayList<>();
		for(Report r : getReportsFromTarget(target)){
			ReportReason reason = r.getReason();
			reasons.add(reason);
		}

		return ListUtil.getMostlyRepeated(reasons);
	}

	/**
	 * Get report for given name
	 *
	 * @param targetName The name
	 * @return The report
	 */
	public static Report getReport(String targetName){
		for(Report r : getReports()){
			if(r.getTarget().getName().equalsIgnoreCase(targetName)){
				return r;
			}
		}
		return null;
	}

	/**
	 * Gets report with given values
	 *
	 * @param sender The sender
	 * @param target The target
	 * @param reason The reason
	 * @return The report
	 */
	public static Report getReport(ProxiedPlayer sender, ProxiedPlayer target, ReportReason reason){
		Report report = new Report(reason, target.getServer().getInfo(), sender, target);

		// Gets uniqueId
		while(hasReport(report.getId())){
			report.generateId();
		}
		return report;
	}

	/**
	 * Checks if reports contains report with given id
	 *
	 * @param id The id
	 * @return The result
	 */
	public static boolean hasReport(String id){
		for(Report r : getReports()){
			if(r.getId().equalsIgnoreCase(id)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if report already exists
	 *
	 * @param sender The sender
	 * @param target The to-reported
	 * @return The result
	 */
	public static boolean hasReport(ProxiedPlayer sender, ProxiedPlayer target){
		for(Report r : getReportsFromSender(sender)){
			if(r.getTarget().getUniqueId().equals(target.getUniqueId())){
				return true;
			}
		}
		return false;
	}

	/**
	 * Get reports with given player as target
	 *
	 * @param player The player
	 * @return The list of reports
	 */
	public static List<Report> getReportsFromTarget(ProxiedPlayer player){
		List<Report> playerReports = new ArrayList<>();

		for(Report r : reports){
			if(r.getTarget().getUniqueId().equals(player.getUniqueId())){
				playerReports.add(r);
			}
		}
		return playerReports;
	}

	/**
	 * Get reports from given player
	 *
	 * @param player The player
	 * @return The list of reports
	 */
	public static List<Report> getReportsFromSender(ProxiedPlayer player){
		List<Report> playerReports = new ArrayList<>();

		for(Report r : reports){
			if(r.getFrom().getUniqueId().equals(player.getUniqueId())){
				playerReports.add(r);
			}
		}
		return playerReports;
	}

	/**
	 * Get reports from given reason
	 *
	 * @param reason The reason
	 * @return The list of reports
	 */
	public static List<Report> getReportsFromReason(ReportReason reason){
		List<Report> playerReports = new ArrayList<>();

		for(Report r : reports){
			if(r.getReason() == reason){
				playerReports.add(r);
			}
		}
		return playerReports;
	}

	/**
	 * Get reports from given server
	 *
	 * @param server The server
	 * @return The list of reports
	 */
	public static List<Report> getReportsFromServer(ServerInfo server){
		List<Report> playerReports = new ArrayList<>();

		for(Report r : reports){
			if(r.getServer().getAddress().equals(server.getAddress())){
				playerReports.add(r);
			}
		}
		return playerReports;
	}


}
