package de.superioz.network.bungee.common.report;

import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;

/**
 * Reason for a report
 */
@Getter
public class ReportReason {

	private String name;
	private BanReason.Parent parent;

	public ReportReason(String name, BanReason.Parent parent){
		this.name = name;
		this.parent = parent;
	}

	/**
	 * Get name of the reason
	 *
	 * @return The reason
	 */
	public String getName(){
		return name.toUpperCase();
	}

	/**
	 * Get the name with nice uppercase
	 *
	 * @return Get the name
	 */
	public String getNiceName(){
		return SimpleStringUtils.upperFirstLetterSpaced(name, "_");
	}

}
