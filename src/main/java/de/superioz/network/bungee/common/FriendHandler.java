package de.superioz.network.bungee.common;

import de.superioz.network.api.FriendManager;
import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.chat.ChatLog;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created on 30.04.2016.
 */
public class FriendHandler {

	private static HashMap<UUID, UUID> lastPrivateMessage = new HashMap<>();

	/**
	 * Get the friend player from given proxied player
	 *
	 * @param player The player
	 * @return The friend
	 */
	public static Friend getFriend(ProxiedPlayer player){
		return FriendManager.getEntry(player.getUniqueId());
	}

	/**
	 * Check target registry
	 *
	 * @param commandSender The command sender
	 * @param name          The target name
	 * @return The result
	 */
	public static boolean checkTargetFriend(ProxiedPlayer commandSender, String name, boolean onlineCheck, boolean friendCheck){
		Friend friendPlayer = getFriend(commandSender);
		if(!FriendManager.hasEntry(name)){
			BungeeChat.send(Strings.PLAYER_DOESNT_EXIST, Prefixes.FRIEND, commandSender);
			return false;
		}
		Friend targetFriend = FriendManager.getEntry(name);
		if(friendCheck){
			if(!friendPlayer.hasFriend(targetFriend.getUniqueId())){
				BungeeChat.send(Strings.FRIEND_NOT_FOUND, Prefixes.FRIEND, commandSender);
				return false;
			}
		}
		if(onlineCheck){
			ProxiedPlayer target = NetworkBungee.h.getProxy().getPlayer(name);
			if(target == null){
				BungeeChat.send(Strings.PLAYER_NOT_ONLINE, Prefixes.FRIEND, commandSender);
				return false;
			}
		}
		return true;
	}

	public static boolean checkTargetFriend(ProxiedPlayer commandSender, String name, boolean onlineCheck){
		return checkTargetFriend(commandSender, name, onlineCheck, true);
	}

	public static boolean checkTargetFriend(ProxiedPlayer commandSender, String name){
		return checkTargetFriend(commandSender, name, false, true);
	}

	/**
	 * Writes a message
	 *
	 * @param from From
	 * @param name Name of receiver
	 */
	public static void writeMessage(Friend from, String name, String message){
		// Check target
		ProxiedPlayer fromPlayer = BungeeLibrary.proxy().getPlayer(from.getUniqueId());
		if(!FriendHandler.checkTargetFriend(fromPlayer, name, true, true)){
			return;
		}
		Friend to = FriendManager.getEntry(name);
		ProxiedPlayer toPlayer = BungeeLibrary.proxy().getPlayer(to.getUniqueId());

		// Get message
		String patternSend = Strings.PATTERN_MESSAGE_SEND
				.replace("%color", to.getChatColor())
				.replace("%target", to.getName())
				.replace("%msg", message);
		String patternReceive = Strings.PATTERN_MESSAGE_RECEIVE
				.replace("%color", from.getChatColor())
				.replace("%sender", from.getName())
				.replace("%msg", message);

		// Send message
		BungeeChat.send(patternSend, Prefixes.FRIEND, fromPlayer);
		BungeeChat.send(patternReceive, Prefixes.FRIEND, toPlayer);

		// Log
		ChatLog.log(fromPlayer,
				"/msg" + " " + name + " " + message, ChatLog.Level.DEBUG);

		lastPrivateMessage.put(to.getUniqueId(), from.getUniqueId());
	}

	/**
	 * Replies to a message
	 *
	 * @param from    From
	 * @param message The message
	 */
	public static void replyMessage(Friend from, String message){
		if(!hasLastPrivateMessage(from)){
			ProxiedPlayer fromPlayer = BungeeLibrary.proxy().getPlayer(from.getUniqueId());
			BungeeChat.send(Strings.NO_LAST_MESSAGE_TO_REPLY, Prefixes.FRIEND, fromPlayer);
			return;
		}
		Friend to = getLastPrivate(from);
		writeMessage(from, to.getName(), message);
	}

	/**
	 * Get last private message sender
	 *
	 * @param player The player
	 * @return The registry
	 */
	public static Friend getLastPrivate(Friend player){
		return FriendManager.getEntry(lastPrivateMessage.get(player.getUniqueId()));
	}

	/**
	 * Checks if the map contains the player
	 *
	 * @param player The player
	 * @return The result
	 */
	public static boolean hasLastPrivateMessage(Friend player){
		return lastPrivateMessage.containsKey(player.getUniqueId());
	}

}
