package de.superioz.network.bungee.common.chat;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.event.ChatLogEvent;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.java.file.LogCache;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for chatlog
 */
@Getter
public class ChatLog {

	private static Map<ServerInfo, LogCache> chatLog = new HashMap<>();
	private static Map<String, LogCache> userLog = new HashMap<>();

	private static final String CHATLOG_FORMAT = "%msg";

	/**
	 * Chatlogs the given message
	 *
	 * @param player     The player
	 * @param logMessage The message
	 */
	public static void log(ProxiedPlayer player, String logMessage, Level level){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_CHATLOG);
		int allowedLevel = NetworkBungee.c.getConfig().getInt(ConfigManager.CHAT_CHATLOG_LEVEL);
		ServerInfo serverInfo = player.getServer().getInfo();

		// Check if it's enabled
		if(!enabled || !(allowedLevel == 1 || level.getValue() == allowedLevel)){
			return;
		}

		// Get the logCache
		LogCache serverCache = getCache(serverInfo);
		LogCache userCache = getCache(player.getName());

		// Event
		ChatLogEvent event = new ChatLogEvent(logMessage, player, level);
		BungeeLibrary.callEvent(event);

		if(event.isCancelled()) return;
		serverCache.log(CHATLOG_FORMAT.replace("%msg", ChatColor.stripColor(logMessage)));
		userCache.log(CHATLOG_FORMAT.replace("%msg", ChatColor.stripColor(serverInfo.getName() + " | " + logMessage)));
	}

	/**
	 * Creates the log file for given server
	 *
	 * @param serverName The name of the server
	 */
	public static File create(String serverName){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_CHATLOG);
		ServerInfo server = NetworkBungee.h.getProxy().getServerInfo(serverName);

		if(server == null || !hasCache(server)
				|| !enabled){
			return null;
		}
		LogCache cache = chatLog.get(server);
		return cache.build();
	}

	/**
	 * Creates the log file for given user
	 *
	 * @param user The name of the user
	 */
	public static File createForUser(String user){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_CHATLOG);
		if(!enabled || !hasCache(user)){
			return null;
		}

		LogCache cache = userLog.get(user);
		return cache.build();
	}

	/**
	 * Checks if serverInfo has cache
	 *
	 * @param info The server
	 * @return The result
	 */
	public static boolean hasCache(ServerInfo info){
		return chatLog.containsKey(info);
	}

	/**
	 * Checks if user has cache
	 *
	 * @param user The user
	 * @return The result
	 */
	public static boolean hasCache(String user){
		return userLog.containsKey(user);
	}

	/**
	 * Get the cache for this server
	 *
	 * @param serverInfo The server
	 * @return The cache
	 */
	public static LogCache getCache(ServerInfo serverInfo){
		LogCache cache;
		if(chatLog.containsKey(serverInfo)){
			cache = chatLog.get(serverInfo);
		}
		else{
			cache = new LogCache(NetworkBungee.h.getDataFolder(),
					"chatlogs", "Chatlog of " + serverInfo.getName() + " (%time)");
			chatLog.put(serverInfo, cache);
		}

		return cache;
	}

	/**
	 * Gets a user cache for given user
	 *
	 * @param user The user
	 * @return The log cache
	 */
	public static LogCache getCache(String user){
		LogCache cache;
		if(userLog.containsKey(user)){
			cache = userLog.get(user);
		}
		else{
			cache = new LogCache(NetworkBungee.h.getDataFolder(),
					"chatlogs", "Chatlog for " + user + " (%time)");
			userLog.put(user, cache);
		}

		return cache;
	}

	/**
	 * Get the path of the logfile
	 *
	 * @param server The server
	 * @return The result
	 */
	public static String getPath(ServerInfo server, File file){
		LogCache cache = getCache(server);
		return cache.getFolder() + "/" + file.getName();
	}

	/**
	 * Level of logging
	 */
	public enum Level {

		DEBUG(-1),
		MESSAGE(0),
		ALL(1);

		private int i;

		Level(int i){
			this.i = i;
		}

		public int getValue(){
			return i;
		}

	}

}
