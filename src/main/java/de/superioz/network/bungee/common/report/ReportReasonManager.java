package de.superioz.network.bungee.common.report;

import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import lombok.Getter;
import net.md_5.bungee.config.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created on 13.04.2016.
 */
public class ReportReasonManager {

	@Getter
	private static List<ReportReason> reportReasons = new ArrayList<>();

	/**
	 * Initialises the reportReasons
	 */
	public static void init(){
		// Config
		String path = ConfigManager.REPORT_REASONS;
		ConfigManager manager = NetworkBungee.c;
		Configuration configuration = manager.getConfig();

		// Set default
		List<ReportReason> reportReasons = new ArrayList<>();
		for(BanReason.Parent reason : BanReason.Parent.values()){
			String fullPath = path + reason.getName();

			if(configuration.getStringList(fullPath).isEmpty()){
				configuration.set(fullPath, Collections.singletonList(reason.getName()));
			}
			else{
				List<String> r = configuration.getStringList(fullPath);

				for(String s : r){
					reportReasons.add(new ReportReason(s, reason));
				}
			}
		}

		manager.save();
		ReportReasonManager.reportReasons = reportReasons;
	}

	/**
	 * Get a reason from a string
	 *
	 * @param s The string
	 * @return The reason
	 */
	public static ReportReason fromString(String s, BanReason.Parent... banReasons){
		for(ReportReason r : reportReasons){
			if(r.getName().equalsIgnoreCase(s)
					&& (banReasons.length == 0
					|| Arrays.asList(banReasons).contains(r.getParent()))){
				return r;
			}
		}
		return null;
	}

	public static ReportReason fromStringExcept(String s, BanReason.Parent... banReasons){
		for(ReportReason r : reportReasons){
			if(r.getName().equalsIgnoreCase(s)
					&& (banReasons.length == 0
					|| !Arrays.asList(banReasons).contains(r.getParent()))){
				return r;
			}
		}
		return null;
	}

	/**
	 * Get reportReasons from parent
	 *
	 * @param banReasons The parents
	 * @return The list of reportReasons
	 */
	public static List<ReportReason> getReasons(BanReason.Parent... banReasons){
		List<ReportReason> reportReasons = new ArrayList<>();
		for(ReportReason r : ReportReasonManager.reportReasons){
			if(banReasons.length == 0
					|| Arrays.asList(banReasons).contains(r.getParent())){
				reportReasons.add(r);
			}
		}
		return reportReasons;
	}

	public static List<ReportReason> getReasonsExcept(BanReason.Parent... banReasons){
		List<ReportReason> reportReasons = new ArrayList<>();
		for(ReportReason r : ReportReasonManager.reportReasons){
			if(!Arrays.asList(banReasons).contains(r.getParent())){
				reportReasons.add(r);
			}
		}
		return reportReasons;
	}

	/**
	 * Get a reason from parent
	 *
	 * @param parent The parent
	 * @return The reason
	 */
	public static List<ReportReason> fromParent(BanReason.Parent parent){
		List<ReportReason> l = new ArrayList<>();
		for(ReportReason r : reportReasons){
			if(r.getParent() == parent){
				l.add(r);
			}
		}
		return l;
	}

	/**
	 * Get all values as uppercase values
	 *
	 * @return The list of reportReasons
	 */
	public static List<String> getValues(List<ReportReason> reasons){
		List<String> values = new ArrayList<>();

		for(ReportReason r : reasons){
			values.add(r.getName());
		}
		return values;
	}

	public static List<String> getValues(){
		return getValues(reportReasons);
	}

}
