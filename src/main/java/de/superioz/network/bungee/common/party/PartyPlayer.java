package de.superioz.network.bungee.common.party;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 04.05.2016.
 */
@Getter
public class PartyPlayer {

	private ProxiedPlayer player;
	private Map<String, Party> requests = new HashMap<>();

	public PartyPlayer(ProxiedPlayer player){
		this.player = player;
	}

	/**
	 * Creates a party
	 *
	 * @return The party
	 */
	public Party createParty(){
		PartyHandler.createParty(player);
		return getParty();
	}

	/**
	 * Leaves the party
	 */
	public void leaveParty(){
		if(!hasParty()) return;
		getParty().leaveMember(getPlayer());
	}

	/**
	 * Deletes the party
	 */
	public void deleteParty(){
		if(!hasParty()) return;
		if(!isLeader()){
			getParty().quitMember(getPlayer());
		}
		else{
			getParty().disband();
		}
	}

	/**
	 * Checks if the request is active
	 *
	 * @param name The name
	 * @return The result
	 */
	public boolean requestActive(String name){
		if(!hasRequest(name)) return false;
		Party request = getRequest(name);
		if(request == null || !request.getInvitations().contains(this)){
			requests.remove(name);
			return false;
		}
		return true;
	}

	/**
	 * Accept a request from given player
	 *
	 * @param name The player
	 * @return The result
	 */
	public boolean acceptRequest(String name){
		if(!hasRequest(name)) return false;
		Party party = getRequest(name);
		party.getInvitations().remove(this);
		requests.clear();
		return party.addMember(getPlayer());
	}

	/**
	 * Deny request from given player
	 *
	 * @param player The player
	 */
	public void denyRequest(String player){
		if(!hasRequest(player)) return;
		Party party = getRequest(player);
		party.getInvitations().remove(this);
		removeRequest(player);
	}

	/**
	 * Add request with given values
	 *
	 * @param player The player
	 * @param party  The party
	 */
	public void addRequest(String player, Party party){
		if(hasRequest(player)) return;
		requests.put(player, party);
	}

	/**
	 * Is the player the party leader
	 *
	 * @return The result
	 */
	public boolean isLeader(){
		return hasParty() && getParty().isLeader(getPlayer());
	}

	/**
	 * Get the party from this player
	 *
	 * @return The party
	 */
	public Party getParty(){
		return PartyHandler.getParty(getPlayer());
	}

	/**
	 * Checks if the player has a party
	 *
	 * @return The result
	 */
	public boolean hasParty(){
		return PartyHandler.hasParty(getPlayer());
	}

	/**
	 * Get the request from given player
	 *
	 * @param name The player
	 * @return The party
	 */
	public Party getRequest(String name){
		if(!hasRequest(name)) return null;
		return requests.get(name);
	}

	/**
	 * Remove request with given name if exists
	 *
	 * @param name The name of the party leader
	 */
	public void removeRequest(String name){
		if(!hasRequest(name)) return;
		requests.remove(name);
	}

	/**
	 * Removes a request with given party
	 *
	 * @param party The party
	 */
	public void removeRequest(Party party){
		boolean remove = false;
		for(Party p : getRequests().values()){
			if(p.equals(party)){
				remove = true;
				break;
			}
		}
		if(remove) getRequests().values().remove(party);
	}


	/**
	 * Checks if player has request from given player
	 *
	 * @param name The player
	 * @return The result
	 */
	public boolean hasRequest(String name){
		return requests.containsKey(name);
	}

	public boolean hasRequest(Party party){
		return requests.containsValue(party);
	}

}
