package de.superioz.network.bungee.common.chat;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NickManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.event.ChatSendEvent;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.network.bungee.util.Permissions;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.SpecialCharacter;
import de.superioz.sx.bungee.util.ChatUtil;
import de.superioz.sx.java.util.SimpleStringUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created on 06.04.2016.
 */
public class ChatManager {

	private static Map<ProxiedPlayer, Long> lastWritten = new HashMap<>();
	private static Map<ProxiedPlayer, String> lastMessage = new HashMap<>();

	/**
	 * Check if given message contains advertisement
	 *
	 * @param sender  The sender
	 * @param message The message
	 * @return The result
	 */
	public static String checkAntiAdvertisement(ProxiedPlayer sender, String message){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_ANTI_ADVERTISEMENT);
		if(!enabled || sender.hasPermission(Permissions.CHAT_ADVERTISEMENT_BYPASS)){
			return "";
		}
		return SimpleStringUtils.containsAddress(message);
	}

	/**
	 * Check if the message contains swearing
	 *
	 * @param sender  The sender
	 * @param message The message
	 * @return The result
	 */
	public static String checkAntiSwearing(ProxiedPlayer sender, String message){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_BLOCKED_WORDS_ENABLED);
		if(!enabled || sender.hasPermission(Permissions.CHAT_BLOCKED_WORDS_BYPASS)){
			return "";
		}
		List<String> blockedWords = NetworkBungee.c.getConfig().getStringList(ConfigManager.CHAT_BLOCKED_WORDS_LIST);

		// Check for blocked words
		return SimpleStringUtils.checkForBlockedWords(message, blockedWords);
	}

	/**
	 * Apply anti caps on given message
	 *
	 * @param sender  The sender
	 * @param message The message
	 * @return The message without caps
	 */
	public static String applyAntiCaps(ProxiedPlayer sender, String message){
		boolean enabled = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_CAPS_PROTECTION);
		if(!enabled || sender.hasPermission(Permissions.CHAT_CAPS_BYPASS)){
			return message;
		}

		// Check caps letters
		int caps = SimpleStringUtils.getCapsInString(message);
		int wordCount = SimpleStringUtils.getWordCount(message, 2);
		if(caps > wordCount){
			return message.toLowerCase();
		}
		return message;
	}

	/**
	 * Get the last written message
	 *
	 * @param player The player
	 * @return The message
	 */
	public static String getLastMessage(ProxiedPlayer player){
		boolean spamBlocked = NetworkBungee.c.getConfig().getBoolean(ConfigManager.CHAT_SPAM_PROTECTION);
		if(!lastMessage.containsKey(player)
				|| !spamBlocked
				|| player.hasPermission(Permissions.CHAT_SPAM_BYPASS)){
			return "";
		}
		return lastMessage.get(player);
	}

	/**
	 * Check if given player is allowed to write
	 *
	 * @param player The player
	 * @return The result
	 */
	public static double checkWriteDelay(ProxiedPlayer player){
		if(!lastWritten.containsKey(player)
				|| player.hasPermission(Permissions.CHAT_DELAY_BYPASS)){
			return 0;
		}
		double before = lastWritten.get(player);
		double now = System.currentTimeMillis();
		double diff = (now - before) / 1000D;
		double minDiff = NetworkBungee.c.getConfig().getDouble(ConfigManager.CHAT_MESSAGE_DELAY);

		if(diff < minDiff){
			return minDiff - diff;
		}
		return 0;
	}

	/**
	 * Puts the given player into the message write map
	 *
	 * @param player The player
	 */
	public static void onChat(ProxiedPlayer player, String message){
		long timeStamp = System.currentTimeMillis();

		lastWritten.put(player, timeStamp);
		lastMessage.put(player, message);

		// Log to file
		ChatLog.log(player,
				player.getName() + ": " + ChatColor.stripColor(message),
				ChatLog.Level.MESSAGE);
	}

	/**
	 * Get chat message pattern
	 *
	 * @return The message pattern
	 */
	public static String getPattern(){
		ConfigManager config = NetworkBungee.c;
		return config.getString(ConfigManager.CHAT_PATTERN);
	}

	/**
	 * Get the chat color of given value
	 *
	 * @param uuid The uuid
	 * @return The color as string
	 */
	public static String getColor(UUID uuid){
		String color = GroupManager.getGroup(uuid).getColor();
		if(color.isEmpty()) color = "&r";
		return color;
	}

	public static String getColor(ProxiedPlayer player){
		return getColor(player.getUniqueId());
	}

	public static String getColor(String name){
		return getColor(ProfileManager.getUniqueId(name));
	}

	/**
	 * Get the message to send from the chatManager (with chatPattern)
	 *
	 * @param sender  The sender
	 * @param message The message
	 * @return The message with replaced variables
	 */
	public static String getMessage(ProxiedPlayer sender, String message){
		PermissionGroup group = GroupManager.getGroup(sender.getUniqueId());
		boolean isNicked = NickManager.hasNick(sender.getUniqueId());

		String pattern = getPattern();
		String color = group.getColor();
		String prefix = group.getChatPrefix();
		String suffix = group.getChatSuffix();

		// Check permissions
		message = sender.hasPermission(Permissions.CHAT_COLOR_CODES) ? ChatUtil.colored(message) : message;
		message = sender.hasPermission(Permissions.CHAT_SPECIAL_CHARS) ? SpecialCharacter.apply(message) : message;

		// Replace variables
		pattern = pattern.replace("%name", isNicked ? NickManager.getNick(sender.getUniqueId()) : sender.getName());
		pattern = pattern.replace("%prefix", prefix);
		pattern = pattern.replace("%suffix", suffix);
		pattern = pattern.replace("%color", color);
		pattern = ChatUtil.fabulize(pattern);
		pattern = pattern.replace("%msg", message);

		return pattern;
	}

}
