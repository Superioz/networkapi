package de.superioz.network.bungee.common;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NetworkAPI;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.api.registry.ban.BanDuration;
import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.api.registry.ban.BanType;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.chat.TeamChat;
import de.superioz.network.bungee.common.report.ReportReason;
import de.superioz.network.bungee.event.BanPlayerEvent;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.bungee.util.ChatUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.minecraft.server.v1_8_R3.NetworkManager;

/**
 * Created on 30.04.2016.
 */
public class BanHandler {

	/**
	 * Check if the target can be banned
	 *
	 * @param senderProfile The sender profile
	 * @param targetProfile The ban target
	 * @param type          The type
	 * @param sender        The sender
	 */
	public static boolean checkTarget(PlayerProfile senderProfile,
	                               PlayerProfile targetProfile,
	                               BanType type,
	                               CommandSender sender){
		// Check rank
		boolean allowedToBan;
		PermissionGroup targetGroup = GroupManager.getGroup(targetProfile.getUniqueId());
		if(senderProfile.getId() == 0){
			allowedToBan = true;
		}
		else{
			PermissionGroup senderGroup = GroupManager.getGroup(senderProfile.getUniqueId());

			allowedToBan = senderGroup == null || targetGroup == null
					|| senderGroup.getRank() > targetGroup.getRank();
		}
		if(!allowedToBan){
			BungeeChat.send(Strings.CANNOT_BAN_PLAYER, Prefixes.BAN, sender);
			allowedToBan = false;
		}

		// Check ban
		if(BanManager.isBanned(targetProfile.getId(), type, false)){
			if(type.compare(BanType.GLOBAL_UUID)){
				BungeeChat.send(Strings.PLAYER_ALREADY_BANNED, Prefixes.BAN, sender);
				allowedToBan = false;
			}
			else{
				BungeeChat.send(Strings.PLAYER_ALREADY_CHAT_BANNED, Prefixes.BAN, sender);
				allowedToBan = false;
			}
		}
		return allowedToBan;
	}

	/**
	 * Bans a user
	 *
	 * @param sender        The ban sender
	 * @param targetProfile The ban target
	 * @param reasonType    The reason type
	 * @param childReason   The reason
	 * @param type          The ban type
	 */
	public static boolean banUser(CommandSender sender, PlayerProfile targetProfile,
	                              BanReason.Parent reasonType, ReportReason childReason, long duration,
	                              BanType type){
		//
		// Check sender
		//
		PlayerProfile senderProfile;
		if(!ProfileManager.contains(sender.getName())){
			senderProfile = ProfileManager.CONSOLE_PROFILE;
		}
		else{
			senderProfile = ProfileManager.getProfile(sender.getName());
		}

		//
		// Check target
		//
		if(!checkTarget(senderProfile, targetProfile, type, sender)){
			return false;
		}

		//
		// Reason and Type
		//
		int reasonParentId = reasonType.getId();
		String childReasonName = childReason.getName();
		int banTypeId = type.getId();

		//
		// Duration
		//
		BanDuration banDuration = BanDuration.fromParentReason(reasonType);
		assert banDuration != null;
		int currentBanPoints = BanManager.getBanPoints(targetProfile.getId());
		long fromTimeStamp = System.currentTimeMillis();
		long untilTimeStamp = fromTimeStamp;
		int factor = 1;

		// Check duration
		if(duration < 0){
			untilTimeStamp = -1;
		}
		else{
			// Check ban points
			if(type != BanType.CHAT){
				int reasonBanPoints = reasonType.getBanPoints();
				currentBanPoints += reasonBanPoints;
				int maxBanPoints = NetworkBungee.c.getConfig().getInt(ConfigManager.MAX_BAN_POINTS);

				if(currentBanPoints >= maxBanPoints){
					untilTimeStamp = -1;
				}
				else if(currentBanPoints > 0){
					factor = (currentBanPoints / reasonBanPoints);
				}
			}
			else{
				factor = BanManager.getBans(targetProfile.getId(), BanType.CHAT).size();
			}

			// Check factor
			if(duration > 0){
				factor = 0;
			}
		}
		if(factor > 0
				&& untilTimeStamp != -1){
			untilTimeStamp = fromTimeStamp + (banDuration.getMillis() * factor);
		}

		//
		// Write ban into mongo
		//
		Ban ban = NetworkAPI.Registry.BAN.getBan(BanManager.getNextId(), targetProfile.getId(),
				targetProfile.getIp(), senderProfile.getId(), reasonParentId, childReasonName,
				fromTimeStamp, untilTimeStamp, banTypeId);

		// Ban Event
		BanPlayerEvent event = new BanPlayerEvent(ban);
		BungeeLibrary.callEvent(event);

		if(event.isCancelled()){
			return false;
		}
		NetworkAPI.Registry.BAN.write(ban);


		//
		// Check ban type and if the player is online
		//
		if(ban.getType() == BanType.GLOBAL_UUID){
			ProxiedPlayer targetPlayer = BungeeLibrary.proxy().getPlayer(targetProfile.getUniqueId());
			if(targetPlayer != null){
				targetPlayer.disconnect(ban.getMessage());
			}
		}
		else if(ban.getType() == BanType.GLOBAL_IP){
			String ip = targetProfile.getIp();

			for(ProxiedPlayer p : BungeeLibrary.proxy().getPlayers()){
				String pIp = p.getAddress().getAddress().getHostAddress();
				if(pIp.equals(ip)){
					p.disconnect(ban.getMessage());
				}
			}
		}

		//
		// Get color
		//
		String senderColor = NetworkBungee.c.getString(ConfigManager.CHAT_CONSOLE_COLOR);
		if(sender instanceof ProxiedPlayer){
			senderColor = ChatManager.getColor((ProxiedPlayer) sender);
		}
		String targetColor = ChatManager.getColor(targetProfile.getUniqueId());

		// Get time
		String time = BanDuration.fromDifference(ban.getTimeDifference());

		//
		// Send message
		//
		String message = Strings.TEAM_BANNED_PLAYER
				.replace("%sColor", senderColor)
				.replace("%sName", sender.getName())
				.replace("%tColor", targetColor)
				.replace("%tName", targetProfile.getName())
				.replace("%type", ban.getType().getNiceName())
				.replace("%until", ban.isPermanent() ? "Permanent" : time);
		TeamChat.sendMessage(ChatUtil.fabulize(message), Prefixes.BAN);
		return true;
	}

}
