package de.superioz.network.bungee.common.party;

import de.superioz.network.api.SerializableParty;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.java.util.RandomUtil;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created on 04.05.2016.
 */
@Getter
public class Party {

	private ProxiedPlayer leader;
	private List<ProxiedPlayer> member;
	private List<PartyPlayer> invitations = new ArrayList<>();

	private long timeStamp;
	private String rootOwner;
	private boolean disbanded = false;

	public Party(ProxiedPlayer leader){
		this.leader = leader;
		this.member = new ArrayList<>();

		this.rootOwner = leader.getName();
		this.timeStamp = System.currentTimeMillis();
	}

	/**
	 * Serializes this party
	 *
	 * @return The party
	 */
	public SerializableParty serialize(){
		UUID leader = getLeader().getUniqueId();
		List<UUID> members = new ArrayList<>();
		List<UUID> invitations = new ArrayList<>();

		for(ProxiedPlayer m : getMember()){
			members.add(m.getUniqueId());
		}
		for(PartyPlayer i : getInvitations()){
			invitations.add(i.getPlayer().getUniqueId());
		}
		return new SerializableParty(
				leader, members, invitations, timeStamp, rootOwner, disbanded
		);
	}

	/**
	 * Get the server where the leader is currently online
	 *
	 * @return The server
	 */
	public Server getServer(){
		return getLeader().getServer();
	}

	/**
	 * Synchronizes the server
	 */
	public void synchronizeServer(){
		Server server = getServer();
		for(ProxiedPlayer member : getMember()){
			member.connect(server.getInfo());
		}
	}

	/**
	 * Wait for party timeout
	 *
	 * @param invite The invitation (NOT NECESSARY)
	 * @param from   The invitation sender (NOT NECESSARY)
	 * @param player The party player (NOT NECESSARY)
	 */
	public void waitForTimeout(final boolean invite,
	                           final ProxiedPlayer from, final PartyPlayer player){
		BungeeLibrary.proxy().getScheduler().schedule(BungeeLibrary.plugin(), new Runnable() {
			@Override
			public void run(){
				if(invite){
					if(from != null
							&& player != null){
						invitations.remove(player);
						player.removeRequest(from.getName());
					}
				}

				// Disband party if too few players are inside the party)
				if(isDisbanded()) return;
				if(getMember().size() == 0){
					BungeeChat.send(Strings.PARTY_TIMEOUT, Prefixes.PARTY, getLeader());
					disband();
				}
			}
		}, 30, TimeUnit.SECONDS);
	}

	public void waitForTimeout(){
		waitForTimeout(false, null, null);
	}

	/**
	 * Send invitation to given party player
	 *
	 * @param from   The from
	 * @param player The player
	 */
	public void sendInvitation(final ProxiedPlayer from, final PartyPlayer player){
		String name = from.getName();
		if(invitations.contains(player)) return;
		if(player.hasRequest(name)) return;
		this.invitations.add(player);
		player.addRequest(from.getName(), this);

		// Invitation send and now wait for respond
		// After 60 seconds delete the invitation
		this.waitForTimeout(true, from, player);
	}

	/**
	 * Clear all invitations
	 */
	public void clearInvitations(){
		for(PartyPlayer player : invitations){
			player.removeRequest(this);
		}
		invitations.clear();
	}

	/**
	 * Get all members of the party (including the leader)
	 *
	 * @return The list of players
	 */
	public List<ProxiedPlayer> getFullMember(){
		List<ProxiedPlayer> pl = new ArrayList<>(member);
		pl.add(leader);
		return pl;
	}

	/**
	 * Checks if the target player is the party leader
	 *
	 * @param player The player
	 * @return The result
	 */
	public boolean isLeader(ProxiedPlayer player){
		return leader.equals(player);
	}

	/**
	 * Adds a member to the party
	 *
	 * @param player The player
	 */
	public boolean addMember(ProxiedPlayer player){
		if(member.contains(player)) return false;
		member.add(player);
		return true;
	}

	/**
	 * Removes a member from list
	 *
	 * @param player The player
	 * @return The result
	 */
	public boolean quitMember(ProxiedPlayer player){
		if(!member.contains(player)) return false;
		member.remove(player);
		return true;
	}

	/**
	 * Leaves the party gracefully
	 *
	 * @param player The player
	 */
	public void leaveMember(ProxiedPlayer player){
		boolean f = isLeader(player);
		this.quitMember(player);
		if(getFullMember().size() == 0){
			this.disband();
			return;
		}
		else{
			waitForTimeout();
		}
		if(!f) return;

		// Promote new leader
		this.leader = null;
		ProxiedPlayer newLeader = getMember().get(RandomUtil.getInteger(0, getMember().size()));
		this.promote(newLeader);

		// Send information
		send(Strings.PARTY_PLAYER_PROMOTED
				.replace("%c", ChatManager.getColor(newLeader))
				.replace("%p", newLeader.getName()));
	}

	/**
	 * Disbands the party
	 */
	public void disband(){
		for(ProxiedPlayer member : new ArrayList<>(getMember())){
			quitMember(member);
		}
		this.clearInvitations();
		PartyHandler.removeParty(this);
		this.disbanded = true;
	}

	/**
	 * Promotes given player to party leader
	 *
	 * @param player The player
	 */
	public void promote(ProxiedPlayer player){
		if(leader != null && leader.equals(player)) return;
		if(!member.contains(player)) return;
		ProxiedPlayer l = getLeader();
		if(l != null) this.addMember(l);
		this.quitMember(player);
		this.leader = player;
	}

	/**
	 * Sends a message to all players
	 *
	 * @param message The message
	 */
	public void send(String message){
		for(ProxiedPlayer p : getFullMember()){
			BungeeChat.send(message, Prefixes.PARTY, p);
		}
	}

	/**
	 * Sends a chat message to all party players
	 *
	 * @param sender  The sender
	 * @param message The message
	 */
	public void chat(ProxiedPlayer sender, String message){
		String pattern = Strings.PATTERN_PARTY_CHAT;
		pattern = pattern.replace("%color", ChatManager.getColor(sender));
		pattern = pattern.replace("%name", sender.getName());
		pattern = pattern.replace("%msg", message);

		for(ProxiedPlayer p : getFullMember()){
			BungeeChat.send(pattern, Prefixes.PARTY, p);
		}
	}

}
