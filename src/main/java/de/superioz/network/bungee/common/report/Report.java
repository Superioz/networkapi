package de.superioz.network.bungee.common.report;

import de.superioz.network.bungee.common.IDManager;
import lombok.Getter;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 09.04.2016.
 */
@Getter
public class Report {

	private ReportReason reason;
	private ServerInfo server;
	private ProxiedPlayer from;
	private ProxiedPlayer target;
	private long timeStamp;
	private String id;

	/**
	 * Constructor of a report
	 *
	 * @param reason The reason
	 * @param info   The server
	 * @param from   Report sender
	 * @param target The report target
	 */
	public Report(ReportReason reason, ServerInfo info, ProxiedPlayer from, ProxiedPlayer target){
		this.reason = reason;
		this.server = info;
		this.from = from;
		this.target = target;
		this.timeStamp = System.currentTimeMillis();
		this.generateId();
	}

	/**
	 * Generates the id
	 */
	public void generateId(){
		this.id = IDManager.generateId(IDManager.Type.REPORT);
	}

	/**
	 * Compares with another report
	 *
	 * @param report The report
	 * @return The result
	 */
	public boolean compare(Report report){
		return getId().equalsIgnoreCase(report.getId());
	}

}
