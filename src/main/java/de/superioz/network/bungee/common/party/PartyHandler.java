package de.superioz.network.bungee.common.party;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.*;

/**
 * Created on 03.05.2016.
 */
public class PartyHandler {

	private static final Map<UUID, PartyPlayer> PLAYERS = new HashMap<>();
	private static final Set<Party> PARTIES = new HashSet<>();

	/**
	 * Removes given party from list
	 *
	 * @param party The party
	 */
	public static void removeParty(Party party){
		if(!PARTIES.contains(party)) return;
		PARTIES.remove(party);
	}

	/**
	 * Creates a party
	 *
	 * @param creator The creator (the leader)
	 */
	public static void createParty(ProxiedPlayer creator){
		if(hasParty(creator)) return;
		PARTIES.add(new Party(creator));
	}

	/**
	 * Gets the party of given player
	 *
	 * @param player The player
	 * @return The party
	 */
	public static Party getParty(ProxiedPlayer player){
		for(Party p : PARTIES){
			if(p.getFullMember().contains(player)){
				return p;
			}
		}
		return null;
	}

	public static Party getParty(UUID uuid){
		for(Party p : PARTIES){
			for(ProxiedPlayer pl : p.getFullMember()){
				if(pl.getUniqueId().equals(uuid))
					return p;
			}
		}
		return null;
	}

	public static Party getParty(PartyPlayer player){
		return getParty(player.getPlayer());
	}

	/**
	 * Checks if given player is in a party
	 *
	 * @param player The player
	 * @return The result
	 */
	public static boolean hasParty(ProxiedPlayer player){
		return getParty(player) != null;
	}

	public static boolean hasParty(PartyPlayer player){
		return hasParty(player.getPlayer());
	}

	public static boolean hasParty(UUID uuid){
		return getParty(uuid) != null;
	}


	/**
	 * Get party player of given player
	 *
	 * @param player The player
	 * @return The party player
	 */
	public static PartyPlayer getPartyPlayer(ProxiedPlayer player){
		UUID uuid = player.getUniqueId();
		if(!PLAYERS.containsKey(uuid)){
			PartyPlayer p = new PartyPlayer(player);
			PLAYERS.put(uuid, p);
			return p;
		}
		return PLAYERS.get(uuid);
	}

	/**
	 * Removes the proxied player from map
	 *
	 * @param player The player
	 */
	public static void remove(ProxiedPlayer player){
		if(PLAYERS.containsKey(player.getUniqueId()))
			PLAYERS.remove(player.getUniqueId());
	}

}
