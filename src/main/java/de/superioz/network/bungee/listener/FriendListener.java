package de.superioz.network.bungee.listener;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.common.FriendHandler;
import de.superioz.network.bungee.event.FriendStateChangeEvent;
import de.superioz.network.bungee.event.FriendUpdateEvent;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created on 17.04.2016.
 */
public class FriendListener implements Listener {

	private static List<UUID> ONLINE_PLAYER = new ArrayList<>();

	@EventHandler
	public void onProxyJoin(ServerConnectedEvent event){
		ProxiedPlayer player = event.getPlayer();

		if(!ProfileManager.contains(player.getUniqueId())){
			return;
		}
		Friend friend = FriendHandler.getFriend(player);

		// Send online state
		if(!ONLINE_PLAYER.contains(friend.getUniqueId())){
			ONLINE_PLAYER.add(friend.getUniqueId());

			// Event
			FriendStateChangeEvent friendEvent = new FriendStateChangeEvent(friend, FriendStateChangeEvent.State.ONLINE);
			BungeeLibrary.callEvent(friendEvent);
		}

		// Get requests
		if(friend.getRequests().size() > 0){
			BungeeChat.send(Strings.PENDING_REQUESTS_SIZE
					.replace("%size", friend.getRequests().size()+""), Prefixes.FRIEND, player);
			BungeeChat.send(Strings.PENDING_REQUESTS_COMMAND, Prefixes.FRIEND, player);
		}
	}

	@EventHandler
	public void onProxyQuit(PlayerDisconnectEvent event){
		ProxiedPlayer player = event.getPlayer();

		if(!ProfileManager.contains(player.getUniqueId())){
			return;
		}
		Friend friend = FriendHandler.getFriend(player);

		if(ONLINE_PLAYER.contains(friend.getUniqueId())){
			ONLINE_PLAYER.remove(friend.getUniqueId());

			// Event
			FriendStateChangeEvent friendEvent = new FriendStateChangeEvent(friend, FriendStateChangeEvent.State.OFFLINE);
			BungeeLibrary.callEvent(friendEvent);
		}
	}

	@EventHandler
	public void onStateChange(FriendStateChangeEvent event){
		Friend friend = event.getFriend();
		FriendStateChangeEvent.State state = event.getState();

		// Inform friends
		for(Friend f : friend.getFriends()){
			if(f == null) continue;
			if(!ProfileManager.hasProfile(f.getPlayerId())) continue;

			ProxiedPlayer p = BungeeLibrary.proxy().getPlayer(f.getUniqueId());
			if(p != null)
				BungeeChat.send((state == FriendStateChangeEvent.State.ONLINE
						? Strings.FRIEND_NOW_ONLINE : Strings.FRIEND_NOW_OFFLINE)
						.replace("%color", friend.getChatColor())
						.replace("%name", friend.getName()), Prefixes.FRIEND, p);
		}
	}

	@EventHandler
	public void onUpdate(FriendUpdateEvent event){
		Friend from = event.getPlayer();
		Friend updatedFriend = event.getTarget();

		if(event.getType() == FriendUpdateEvent.Type.ADD_FRIEND){
			// SEND MESSAGE
			ProxiedPlayer player = BungeeLibrary.proxy().getPlayer(updatedFriend.getName());

			if(player != null){
				BungeeChat.send(Strings.PLAYER_ACCEPTED_REQUEST
						.replace("%color", from.getChatColor())
						.replace("%name", from.getName()), Prefixes.FRIEND, player);
			}
		}
	}

}
