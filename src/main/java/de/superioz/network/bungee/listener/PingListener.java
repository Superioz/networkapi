package de.superioz.network.bungee.listener;

import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.sx.bungee.util.ChatUtil;
import de.superioz.sx.java.util.RandomUtil;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created on 04.05.2016.
 */
public class PingListener implements Listener {

	@EventHandler
	public void onPing(ProxyPingEvent event){
		ServerPing ping = event.getResponse();

		// GET MOTD
		List<String> motds = NetworkBungee.c.getConfig().getStringList(ConfigManager.MOTD);
		if(motds != null && !motds.isEmpty()){
			int r = RandomUtil.getInteger(0, motds.size());

			// Set motds
			String current = motds.get(r);
			ping.setDescription(ChatUtil.colored(current));
		}

		// GET FAVICON
		String path = NetworkBungee.c.getConfig().getString(ConfigManager.FAVICON);
		String fullPath = NetworkBungee.h.getDataFolder() + "/favicons/" + path;
		File f = new File(fullPath);
		if(f.exists()){
			try{
				BufferedImage image = ImageIO.read(new File(fullPath));
				ping.setFavicon(Favicon.create(image));
			}
			catch(IOException e){
				//
			}
		}
	}

}
