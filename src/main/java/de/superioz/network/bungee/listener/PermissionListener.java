package de.superioz.network.bungee.listener;

import de.superioz.network.api.PermissionManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PlayerProfile;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created on 01.05.2016.
 */
public class PermissionListener implements Listener {

	@EventHandler
	public void onPermissionCheck(PermissionCheckEvent event){
		CommandSender sender = event.getSender();

		if(sender instanceof ProxiedPlayer){
			ProxiedPlayer player = (ProxiedPlayer) sender;
			PlayerProfile profile;

			if(!ProfileManager.hasProfile(player.getUniqueId())
					|| (profile = ProfileManager.getProfile(player.getUniqueId())) == null){
				return;
			}

			event.setHasPermission(PermissionManager.hasPermission(profile,
					event.getPermission(), PermissionType.BUNGEECORD));
			return;
		}
		event.setHasPermission(true);
	}

}
