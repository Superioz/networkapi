package de.superioz.network.bungee.listener;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.bungee.common.chat.ChatLog;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.common.chat.TeamChat;
import de.superioz.network.bungee.event.ChatSendEvent;
import de.superioz.network.bungee.util.Debugger;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.network.bungee.util.Strings;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created on 30.04.2016.
 */
public class ChatListener implements Listener {

	@EventHandler
	public void onChat(ChatEvent event){
		String message = event.getMessage();

		if(!(event.getSender() instanceof ProxiedPlayer)){
			return;
		}

		// Check if it's chat MESSAGE
		if(!event.isCommand()){
			if(event.isCancelled()){
				return;
			}
			event.setCancelled(true);
			ProxiedPlayer sender = (ProxiedPlayer) event.getSender();

			// Event
			ChatSendEvent chatSendEvent = new ChatSendEvent(sender, ChatSendEvent.Channel.CHAT, message);
			BungeeLibrary.callEvent(chatSendEvent);
			if(chatSendEvent.isCancelled()) return;

			// Send message
			Debugger.write("Send chat message (" + ((ProxiedPlayer) event.getSender()).getName() + ")");
			long t = System.currentTimeMillis();

			String fullMessage = ChatManager.getMessage(sender, message);
			for(ProxiedPlayer p : sender.getServer().getInfo().getPlayers()){
				p.sendMessage(TextComponent.fromLegacyText(fullMessage));
			}
			Debugger.write("Message sent. (" + (System.currentTimeMillis()-t) + "ms)");
			ChatManager.onChat(sender, message);
		}
	}

	@EventHandler
	public void onChatSend(ChatSendEvent event){
		ProxiedPlayer sender = event.getSender();
		String message = event.getMessage();

		if(event.getChannel() == ChatSendEvent.Channel.CHAT){
			// Check if player is chatbanned
			if(BanManager.isChatBanned(ProfileManager.getId(sender.getUniqueId()))){
				Ban ban = BanManager.getChatBan(sender.getUniqueId());
				String date = ban.getUntilDate();
				String time = ban.getUntilTime();

				// Send
				BungeeChat.send(Strings.YOU_ARE_CHAT_BANNED, sender);
				BungeeChat.send(Strings.CHAT_BAN_EXPIRE
						.replace("%date", date)
						.replace("%time", time), sender);
				event.setCancelled(true);
				return;
			}

			//
			// Check if the delay is correct
			//
			double delay = ChatManager.checkWriteDelay(sender);
			if(delay > 0){
				BungeeChat.send(Strings.YOU_CAN_WRITE_AGAIN_IN
						.replace("%delay", delay+""), Prefixes.CHAT, sender);
				event.setCancelled(true);
				return;
			}

			//
			// Check last and current message
			//
			String lastMessage = ChatManager.getLastMessage(sender);
			if(!lastMessage.isEmpty()
					&& lastMessage.equalsIgnoreCase(message)){
				BungeeChat.send(Strings.YOU_ALREADY_WROTE_THAT, Prefixes.CHAT, sender);
				event.setCancelled(true);
				return;
			}
			message = ChatManager.applyAntiCaps(sender, message);

			//
			// Check blocked words
			//
			String foundSwearing = ChatManager.checkAntiSwearing(sender, message);
			if(!foundSwearing.isEmpty()){
				BungeeChat.send(Strings.DONT_USE_BLOCKED_WORDS, Prefixes.CHAT, sender);

				//
				// Logging
				//
				ChatLog.log(sender, sender.getName() + " used a blocked word. "
						+ "(" + foundSwearing + ")", ChatLog.Level.DEBUG);
				event.setCancelled(true);
				return;
			}

			//
			// Check advertisement
			//
			String foundAddress = ChatManager.checkAntiAdvertisement(sender, message);
			if(!foundAddress.isEmpty()){
				BungeeChat.send(Strings.DONT_USE_ADVERT, Prefixes.CHAT, sender);

				//
				// Logging
				//
				ChatLog.log(sender,
						sender.getName() + " sent advertisement. "
								+ "(" + foundAddress + ")", ChatLog.Level.DEBUG);
				TeamChat.log(Strings.TEAMCHAT_NOT_ENABLED
						.replace("%color", ChatManager.getColor(sender))
						.replace("%sender", sender.getName())
						.replace("%d", foundAddress), TeamChat.LoggingLevel.EVERYTHING, Prefixes.ADVERTISEMENT);
				event.setCancelled(true);
			}
		}
	}

}
