package de.superioz.network.bungee.listener;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.GroupManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PermissionWrap;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.network.bungee.common.BanHandler;
import de.superioz.network.bungee.common.ConfigManager;
import de.superioz.network.bungee.event.SetTabHeaderEvent;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.SpecialCharacter;
import de.superioz.sx.bungee.util.ChatUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created on 30.04.2016.
 */
public class JoinListener implements Listener {

	@EventHandler
	public void onJoin(ServerConnectedEvent event){
		ProxiedPlayer player = event.getPlayer();

		// Write into database
		NetworkBungee.writeIntoDatabase(player);
		PlayerProfile profile = ProfileManager.getProfile(player.getUniqueId());
		GroupManager.getGroup(profile);

		// Set tab and footer
		List<String> hList = NetworkBungee.c.getConfig().getStringList(ConfigManager.TABLIST_HEADER);
		List<String> fList = NetworkBungee.c.getConfig().getStringList(ConfigManager.TABLIST_FOOTER);
		BungeeLibrary.callEvent(new SetTabHeaderEvent(hList, fList, player, event.getServer()));
	}

	@EventHandler
	public void onPostLogin(LoginEvent event){
		Connection connection = event.getConnection();
		UUID uuid = event.getConnection().getUniqueId();
		String ip = connection.getAddress().getAddress().getHostAddress();
		int playerId = -1;

		// Get playerId
		if(ProfileManager.contains(uuid)){
			playerId = ProfileManager.getId(uuid);
		}

		// Check ban
		if(!BanManager.isBanned(playerId)
				&& !BanManager.isBanned(ip)){
			return;
		}

		// Get the ban
		Ban ban = null;
		if(BanManager.isBanned(ip)){
			ban = BanManager.getBan(ip);
		}
		else if(!(playerId < 0) && BanManager.isBanned(playerId)){
			ban = BanManager.getBan(playerId);
		}

		// Disconnect
		if(ban != null){
			event.setCancelReason(ChatUtil.colored(SpecialCharacter.apply(ban.getMessageAsString())));
			event.setCancelled(true);
		}
	}

}
