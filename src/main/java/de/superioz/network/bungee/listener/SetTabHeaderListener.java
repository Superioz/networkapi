package de.superioz.network.bungee.listener;

import de.superioz.network.bungee.event.SetTabHeaderEvent;
import de.superioz.sx.bungee.util.ChatUtil;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 02.05.2016.
 */
public class SetTabHeaderListener implements Listener {

	@EventHandler
	public void onTabHeader(SetTabHeaderEvent event){
		List<String> hList = event.getHeaderLines();
		List<String> fList = event.getFooterLines();
		ProxiedPlayer player = event.getPlayer();
		Server server = event.getServer();

		List<BaseComponent> hBaseComponents = new ArrayList<>();
		List<BaseComponent> fBaseComponents = new ArrayList<>();

		for(String s : hList){
			fBaseComponents.add(getComponent(s, server, player));
		}
		for(String s : fList){
			hBaseComponents.add(getComponent(s, server, player));
		}

		// Set tab
		player.setTabHeader(fBaseComponents.toArray(new BaseComponent[]{}),
				hBaseComponents.toArray(new BaseComponent[]{}));
	}

	/**
	 * Turns the given string into a text component
	 *
	 * @param s      The string
	 * @param server The server
	 * @return The component
	 */
	private TextComponent getComponent(String s, Server server, ProxiedPlayer player){
		s = s.replace("%server", server.getInfo().getName());
		s = s.replace("%player", player.getName());

		return new TextComponent(ChatUtil.fabulize(s));
	}

}
