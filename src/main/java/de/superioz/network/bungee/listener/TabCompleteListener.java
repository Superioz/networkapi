package de.superioz.network.bungee.listener;

import de.superioz.network.api.registry.ban.BanReason;
import de.superioz.network.bungee.common.report.ReportReason;
import de.superioz.network.bungee.common.report.ReportReasonManager;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.Arrays;
import java.util.List;

/**
 * Created on 16.04.2016.
 */
public class TabCompleteListener implements Listener {

	@EventHandler(priority = EventPriority.HIGH)
	public void onTab(TabCompleteEvent event){
		if(!event.getSuggestions().isEmpty()){
			return;
		}
		final String[] args = event.getCursor().split(" ");
		final String command = args[0].replace("/", "");

		// Reason on argument /command name reason
		if(this.check(command, "ban", "tempban", "chatban")){
			List<ReportReason> reasons = command.equalsIgnoreCase("chatban")
					? ReportReasonManager.getReasons(BanReason.Parent.CHAT_BEHAVIOUR)
					: ReportReasonManager.getReasonsExcept(BanReason.Parent.CHAT_BEHAVIOUR);
			this.check(ReportReasonManager.getValues(reasons), 2, event);
		}
		else if(this.check(command, "unban")){
			this.check(BanReason.Repeal.getValues(), 1, event);
		}
	}

	/**
	 * Check command
	 *
	 * @param command  The command
	 * @param commands The other commands
	 * @return The result
	 */
	private boolean check(String command, String... commands){
		return Arrays.asList(commands).contains(command.toLowerCase());
	}

	/**
	 * Check values
	 *
	 * @param values The values
	 * @param length The command arg length
	 * @param event  The event
	 */
	private void check(List<String> values, int length, TabCompleteEvent event){
		final String fullMessage = event.getCursor();
		final String[] args = event.getCursor().split(" ");
		final int l = args.length;
		final String checked = (args.length > 0 ? args[args.length - 1] : event.getCursor()).toLowerCase();

		for(String s : values){
			String n = s.toLowerCase();
			if((l == length+1 && n.startsWith(checked))
					|| (l == length && fullMessage.endsWith(" "))){
				event.getSuggestions().add(n);
			}
		}
	}

}
