package de.superioz.network.bungee.listener;

import de.superioz.network.bungee.common.party.Party;
import de.superioz.network.bungee.common.party.PartyHandler;
import de.superioz.network.bungee.common.party.PartyPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created on 04.05.2016.
 */
public class PartyListener implements Listener {

	@EventHandler
	public void onQuit(PlayerDisconnectEvent event){
		ProxiedPlayer player = event.getPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		if(!partyPlayer.hasParty()){
			partyPlayer.deleteParty();
		}
		PartyHandler.remove(player);
	}

	@EventHandler
	public void onJoin(ServerConnectedEvent event){
		ProxiedPlayer player = event.getPlayer();
		PartyPlayer partyPlayer = PartyHandler.getPartyPlayer(player);

		if(!PartyHandler.hasParty(partyPlayer)) return;
		Party party = partyPlayer.getParty();
		if(!party.isLeader(player)) return;
		party.synchronizeServer();
	}

}
