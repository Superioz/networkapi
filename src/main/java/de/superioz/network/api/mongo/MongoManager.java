package de.superioz.network.api.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import de.superioz.sx.java.database.MongoDB;
import lombok.Getter;

import java.util.Collection;

/**
 * Created on 20.04.2016.
 *
 * This class is for connecting to the mongo database
 */
@Getter
public class MongoManager {

	private MongoDB server;
	private MongoClient client;

	private String collectionPrefix = "network_";
	private String serverAddress = "localhost";
	private int port = MongoDB.DEFAULT_PORT;
	private String username = "";
	private String password = "";
	private String database = "";

	/**
	 * Manager for handling the mongo database
	 *
	 * @param collectionPrefix The collection prefix
	 * @param serverAddress    The serveraddress
	 * @param port             The port
	 * @param username         The username
	 * @param password         The password
	 * @param database         The database
	 */
	public MongoManager(String collectionPrefix,
	                    String serverAddress, int port,
	                    String username, String password, String database){
		if(!collectionPrefix.isEmpty())
			this.collectionPrefix = collectionPrefix;
		if(!serverAddress.isEmpty())
			this.serverAddress = serverAddress;
		if(port != -1)
			this.port = port;
		this.username = username;
		this.password = password;
		this.database = database;
	}

	public MongoManager(String serverAddress, int port, String username,
	                    String password, String database){
		this("", serverAddress, port, username, password, database);
	}

	public MongoManager(int port, String username,
	                    String password, String database){
		this("", "", port, username, password, database);
	}

	public MongoManager(String username,
	                    String password, String database){
		this("", "", -1, username, password, database);
	}

	public MongoManager(String database){
		this("", "", -1, "", "", database);
	}

	/**
	 * Connect to mongo
	 *
	 * @return The mongo
	 */
	public boolean connect(){
		if(!username.isEmpty()){
			server = new MongoDB(getServerAddress(), getPort(), getUsername(), getPassword(), getDatabase());
		}
		else{
			server = new MongoDB(getServerAddress(), getPort(), getDatabase());
		}
		client = getServer().getClient();
		return true;
	}

	/**
	 * Get collection with given name
	 *
	 * @param name The name
	 * @return The collection
	 */
	public MongoCollection getCollection(String name){
		if(!collectionPrefix.isEmpty()){
			name = collectionPrefix + name;
		}
		return getServer().getCollection(name);
	}

}
