package de.superioz.network.api.mongo;

import lombok.Getter;

/**
 * Created on 29.04.2016.
 */
@Getter
public class MongoConfigData {

	private String databasePrefix;
	private String hostName;
	private int port;
	private String database;
	private String userName;
	private String password;

	/**
	 * Configuration data for connecting with the mongo database
	 *
	 * @param databasePrefix The database prefix
	 * @param hostName       The host name
	 * @param port           The port
	 * @param database       The database
	 * @param userName       The user name
	 */
	public MongoConfigData(String databasePrefix, String hostName, int port, String database, String userName, String password){
		this.databasePrefix = databasePrefix;
		this.hostName = hostName;
		this.port = port;
		this.database = database;
		this.userName = userName;
		this.password = password;
	}

}
