package de.superioz.network.api.mongo;

import com.mongodb.client.MongoCollection;
import de.superioz.network.api.NetworkAPI;

import java.util.List;

/**
 * This class represents a registry
 *
 * A registry is in this project a class for connecting with the mongo database
 * for e.g. fetching playerProfiles (uuid, ip, ..) or for bans
 */
public interface MongoRegistry<T> {

	// The mongo manager
	MongoManager mongoManager = NetworkAPI.getMongoManager();

	/**
	 * Gets the collection
	 *
	 * @return The collection
	 */
	MongoCollection getCollection();

	/**
	 * Get the name of the collection
	 *
	 * @return The name
	 */
	String getName();

	/**
	 * Checks if collection contains smth
	 *
	 * @param index The index
	 * @param value The value
	 * @return The result
	 */
	boolean contains(String index, Object value);

	/**
	 * Reads from the collection
	 *
	 * @param index The index
	 * @param value The value
	 * @return The result as object
	 */
	List<T> read(String index, Object value);

	/**
	 * Deletes an entry
	 *
	 * @param index The index
	 * @param value The value
	 */
	void delete(String index, Object value);

}
