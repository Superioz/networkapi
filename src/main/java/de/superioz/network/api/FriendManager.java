package de.superioz.network.api;

import de.superioz.network.api.registry.Friend;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.sx.bungee.chat.BungeeChat;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.apache.commons.codec.language.bm.Lang;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

/**
 * Created on 30.04.2016.
 */
public class FriendManager {

	/**
	 * Checks if database contains given id
	 *
	 * @param id The id
	 * @return The result
	 */
	public static boolean hasEntry(int id){
		return ProfileManager.contains(id) && NetworkAPI.Registry.FRIEND.containsById(id);
	}

	public static boolean hasEntry(UUID uuid){
		return ProfileManager.contains(uuid) && hasEntry(ProfileManager.getId(uuid));
	}

	public static boolean hasEntry(String name){
		return ProfileManager.contains(name) && hasEntry(ProfileManager.getId(name));
	}

	/**
	 * Get the entry with given id
	 *
	 * @param id The id
	 * @return The friend player
	 */
	public static Friend getEntry(int id){
		return NetworkAPI.Registry.FRIEND.read(id);
	}

	public static Friend getEntry(UUID uuid){
		return getEntry(ProfileManager.getId(uuid));
	}

	public static Friend getEntry(String name){
		return getEntry(ProfileManager.getId(name));
	}

	/**
	 * Either it writes a new entry into the database or
	 * it gets the entry which is already written
	 *
	 * @param id The id
	 * @return The result as friend object
	 */
	public static Friend writeSoft(int id){
		if(hasEntry(id)){
			return getEntry(id);
		}
		return NetworkAPI.Registry.FRIEND.write(id,
				Collections.singletonList(""), Collections.singletonList(""));
	}

}
