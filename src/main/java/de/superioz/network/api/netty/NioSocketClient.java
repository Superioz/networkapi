package de.superioz.network.api.netty;

import de.superioz.network.api.NetworkAPI;
import de.superioz.sx.java.util.Consumer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.Getter;

/**
 * Created on 27.04.2016.
 */
@Getter
public class NioSocketClient {

	private short port;
	private String host;
	private String clientIp;

	private EventLoopGroup group;
	private Channel channel;

	public NioSocketClient(short port, String host, String clientIp){
		this.port = port;
		this.host = host;
		this.clientIp = clientIp;
	}

	/**
	 * Connects to server
	 *
	 * @throws Exception
	 */
	public void connect() throws Exception{
		group = new NioEventLoopGroup();

		Bootstrap bootstrap = new Bootstrap()
				.group(group)
				.channel(NioSocketChannel.class)
				.handler(new SocketClientInitializer());
		channel = bootstrap.connect(getHost(), getPort()).sync().channel();
	}

	/**
	 * Sends a packet to the server
	 *
	 * @param packet The packet
	 */
	public boolean write(NioPacket packet, Consumer<NioPacket> consumer){
		if(!check()){
			return false;
		}

		if(consumer != null
				&& !NioPacketTasks.contains(packet.getId())){
			NioPacketTasks.add(packet.getId(), consumer);
		}

		// Write to channel
		byte[] compressed = packet.compress();
		getChannel().writeAndFlush(compressed);
		return true;
	}

	public boolean write(NioPacket packet){
		return write(packet, null);
	}

	/**
	 * Disconnects from server
	 */
	public void disconnect(){
		this.group.shutdownGracefully();
	}

	/**
	 * Check if the connection is active
	 *
	 * @return The result
	 */
	public boolean check(){
		return getChannel() != null && getChannel().isActive();
	}

	/**
	 * Initializes the client socket
	 */
	private class SocketClientHandler extends SimpleChannelInboundHandler<byte[]> {

		@Override
		protected void channelRead0(ChannelHandlerContext chc, byte[] s) throws Exception{
			NioPacket packet = NioPacket.decompress(s);

			// Is this a simply ping packet?
			if(PingPacket.isPingPacket(packet)){
				/*long timeStamp = Long.valueOf(packet.getContent());
				long current = System.currentTimeMillis();
				long diff = current-timeStamp;*/
				return;
			}

			// Values
			int id = packet.getId();
			NioPacketReceiveEvent.ChannelType type = NioPacketReceiveEvent.ChannelType.SERVANT;
			NioPacketReceiveEvent event = new NioPacketReceiveEvent(packet, chc, type);

			// Is this a task?
			if(packet.getType() == NioPacketType.RESPOND){
				Consumer<NioPacket> consumer = NioPacketTasks.query(id);

				if(consumer != null){
					consumer.accept(packet);
					NioPacketTasks.consumed(id);
				}
			}
			else{
				// Loop through handlers
				for(NioPacketHandler handler : NioPacketHandlerRegistry.getPacketHandlers()){
					if(handler.getType() == type){
						handler.onReceive(event);
					}
				}
			}
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception{
			// super.userEventTriggered(ctx, evt);
			if(evt instanceof IdleStateEvent){
				IdleStateEvent e = (IdleStateEvent) evt;
				if(e.state() == IdleState.READER_IDLE){
					ctx.close();
				}
				else if(e.state() == IdleState.WRITER_IDLE){
					ctx.writeAndFlush(new PingPacket().compress());
				}
			}
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception{
			NetworkAPI.getLogger().warning(cause.getMessage());
		}

	}

	/**
	 * Initializes the socket client
	 */
	private class SocketClientInitializer extends ChannelInitializer<SocketChannel> {

		@Override
		protected void initChannel(SocketChannel socketChannel) throws Exception{
			ChannelPipeline pipeline = socketChannel.pipeline();

			pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(1048576, 0, 4, 0, 4));
			pipeline.addLast("bytesDecoder", new ByteArrayDecoder());

			pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
			pipeline.addLast("bytesEncoder", new ByteArrayEncoder());

			// Set handler
			pipeline.addLast("idleStateHandler", new IdleStateHandler(60, 30, 0));
			pipeline.addLast("handler", new SocketClientHandler());
		}

	}

}
