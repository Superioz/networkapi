package de.superioz.network.api.netty;

/**
 * Created on 29.04.2016.
 */
public class PingPacket extends NioPacket {

	public PingPacket(){
		super(NioPacketType.TASK, "ping", System.currentTimeMillis() + "");
	}

	/**
	 * Checks if given packet is a ping packet
	 *
	 * @param packet The packet
	 * @return The result
	 */
	public static boolean isPingPacket(NioPacket packet){
		return packet.getCommand().equalsIgnoreCase("ping");
	}

}
