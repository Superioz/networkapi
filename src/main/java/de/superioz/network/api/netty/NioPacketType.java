package de.superioz.network.api.netty;

import lombok.Getter;

/**
 * Created on 28.04.2016.
 */
@Getter
public enum NioPacketType {

	REQUEST(0),
	RESPOND(1),
	TASK(2);

	private int id;

	NioPacketType(int id){
		this.id = id;
	}

	/**
	 * Get the packet type from id
	 *
	 * @param id The id
	 * @return The type
	 */
	public static NioPacketType fromId(int id){
		for(NioPacketType t : values()){
			if(t.getId() == id){
				return t;
			}
		}
		return NioPacketType.TASK;
	}

}
