package de.superioz.network.api.netty;

import de.superioz.sx.java.util.JsonUtil;
import de.superioz.sx.java.util.StringCompressor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created on 27.04.2016.
 */
@Getter
@Setter
public class NioPacket {

	private int id;
	private String command;
	private String content;
	private char[] authentification;
	private int typeId;

	/**
	 * Constructor of the packet
	 *
	 * @param content          The content
	 * @param authentification The authentification
	 */
	public NioPacket(NioPacketType type, String command,
	                 String content, char[] authentification, int id){
		this.typeId = type.getId();
		this.command = command;
		this.content = content;
		this.authentification = authentification;

		if(id < 0){
			id = NioPacketIdentifiers.newIdentifier();
		}
		this.id = id;
	}

	public NioPacket(NioPacketType type, String command, String content, char[] authentification){
		this(type, command, content, authentification, -1);
	}

	public NioPacket(NioPacketType type, String command, String content){
		this(type, command, content, null);
	}

	public NioPacket(String command, String content){
		this(NioPacketType.TASK, command, content);
	}

	/**
	 * Get the type from this packet
	 *
	 * @return The type
	 */
	public NioPacketType getType(){
		return NioPacketType.fromId(getTypeId());
	}

	/**
	 * Set the nio packet type
	 *
	 * @param type The type
	 */
	public void setType(NioPacketType type){
		this.typeId = type.getId();
	}

	/**
	 * Returns a json string
	 *
	 * @return The string
	 */
	private String toJson(){
		return JsonUtil.getGson().toJson(this);
	}

	/**
	 * Returns a netty json packet object from json string
	 *
	 * @param json The json
	 * @return The packet
	 */
	private static NioPacket fromJson(String json){
		return JsonUtil.getGson().fromJson(json, NioPacket.class);
	}

	/**
	 * Decompresses given array of bytes
	 * The array of bytes should be the received data from netty pipeline
	 * otherwise it WILL lead to errors
	 *
	 * @param bytes The received data
	 * @return The packet
	 */
	public static NioPacket decompress(byte[] bytes){
		return fromJson(StringCompressor.decompress(bytes));
	}

	/**
	 * Compresses this packet so it can easily be send through a netty pipeline without
	 * getting to big (size of bytes) 'cause a TCP packet can only handle up to ~1500b
	 * safely
	 *
	 * @return The compressed data
	 */
	public byte[] compress(){
		return StringCompressor.compress(toJson());
	}

}
