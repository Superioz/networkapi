package de.superioz.network.api.netty;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 27.04.2016.
 */
@Getter
class NioPacketIdentifiers {

	private static List<Integer> identifier = new ArrayList<>();

	/**
	 * Gets a new identifier
	 *
	 * @return The id
	 */
	static int newIdentifier(){
		int r = 1;

		while(identifier.contains(r)){
			r++;
		}
		identifier.add(r);
		return r;
	}

	/**
	 * Removes an id from list
	 */
	static void idUsed(int id){
		if(identifier.contains(id)){
			identifier.remove(new Integer(id));
		}
	}

}
