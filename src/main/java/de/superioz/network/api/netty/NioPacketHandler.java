package de.superioz.network.api.netty;

/**
 * Created on 27.04.2016.
 */

public interface NioPacketHandler {

	/**
	 * Get the type of the receiver type
	 *
	 * @return The type
	 */
	NioPacketReceiveEvent.ChannelType getType();

	/**
	 * Method when the server or the client receives a packet
	 */
	void onReceive(NioPacketReceiveEvent event);

}
