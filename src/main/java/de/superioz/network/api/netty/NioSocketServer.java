package de.superioz.network.api.netty;

import de.superioz.network.api.NetworkAPI;
import de.superioz.sx.java.util.Consumer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.Getter;

import java.util.Arrays;

/**
 * Created on 27.04.2016.
 */
@Getter
public class NioSocketServer {

	private short port;
	private final char[] authentification;

	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private ServerBootstrap bootstrap;

	private Channel serverChannel;
	private final ChannelGroup channels;

	/**
	 * Constructor of the socket server
	 *
	 * @param port The port
	 */
	public NioSocketServer(short port, String password){
		this.port = port;
		this.channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

		if(password.isEmpty()) authentification = null;
		else authentification = password.toCharArray();
	}

	public NioSocketServer(short port){
		this(port, "");
	}

	/**
	 * Starts the server instance
	 *
	 * @throws Exception When an error upon binding the port etc. happen
	 */
	public void start() throws Exception{
		this.bossGroup = new NioEventLoopGroup();
		this.workerGroup = new NioEventLoopGroup();

		this.bootstrap = new ServerBootstrap()
				.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(new SocketServerInitializer());
		bootstrap.option(ChannelOption.SO_BACKLOG, 50);
		bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
		this.serverChannel = this.bootstrap.bind(port).sync().channel();
	}

	/**
	 * Stops the server
	 */
	public void stop(){
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
	}

	/**
	 * Send to all servers
	 *
	 * @param packet The packet
	 */
	public boolean send(NioPacket packet, Consumer<NioPacket> consumer){
		if(!check()){
			return false;
		}

		if(consumer != null
				&& !NioPacketTasks.contains(packet.getId())){
			NioPacketTasks.add(packet.getId(), consumer);
		}
		for(Channel channel : getChannels()){
			channel.writeAndFlush(packet.compress());
		}
		return true;
	}

	public boolean send(NioPacket packet){
		return send(packet, null);
	}

	/**
	 * Checks if the channel is active
	 *
	 * @return The result
	 */
	public boolean check(){
		return getServerChannel().isActive();
	}

	/**
	 * Class for handling a socket server (input, exceptions ..)
	 */
	@Getter
	private class SocketServerHandler extends SimpleChannelInboundHandler<byte[]> {

		@Override
		protected void channelRead0(ChannelHandlerContext chc, byte[] s) throws Exception{
			NioPacket packet = NioPacket.decompress(s);

			// Is this a simply ping packet?
			if(PingPacket.isPingPacket(packet)){
				/*long timeStamp = Long.valueOf(packet.getContent());
				long current = System.currentTimeMillis();
				long diff = current-timeStamp;*/
				return;
			}

			// Get values
			int id = packet.getId();
			NioPacketReceiveEvent.ChannelType type = NioPacketReceiveEvent.ChannelType.MASTER;
			NioPacketReceiveEvent event = new NioPacketReceiveEvent(packet, chc, type);

			// Is this a task?
			if(packet.getType() == NioPacketType.RESPOND){
				Consumer<NioPacket> consumer = NioPacketTasks.query(id);

				if(consumer != null){
					consumer.accept(packet);
					NioPacketTasks.consumed(id);
				}
			}
			else{
				// Loop through handlers
				for(NioPacketHandler handler : NioPacketHandlerRegistry.getPacketHandlers()){
					boolean checkAuth = getAuthentification() == null
							|| (getAuthentification() != null
							&& Arrays.equals(getAuthentification(), packet.getAuthentification()));
					if(handler.getType() == type && checkAuth){
						handler.onReceive(event);
					}
				}
			}
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception{
			// super.userEventTriggered(ctx, evt);
			if(evt instanceof IdleStateEvent){
				IdleStateEvent e = (IdleStateEvent) evt;
				if(e.state() == IdleState.READER_IDLE){
					ctx.close();
				}
				else if(e.state() == IdleState.WRITER_IDLE){
					ctx.writeAndFlush(new PingPacket().compress());
				}
			}
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception{
			// super.exceptionCaught(ctx, cause);
		}

		@Override
		public void handlerAdded(ChannelHandlerContext ctx) throws Exception{
			channels.add(ctx.channel());
			NetworkAPI.getLogger().info("Servant joined the netty channel. (" + ctx.channel().remoteAddress() + ")");
		}

		@Override
		public void handlerRemoved(ChannelHandlerContext ctx) throws Exception{
			channels.remove(ctx.channel());
			NetworkAPI.getLogger().info("Servant left the netty channel. (" + ctx.channel().remoteAddress() + ")");
		}

	}

	/**
	 * Class for initialises a server socket (channel)
	 */
	private class SocketServerInitializer extends ChannelInitializer<SocketChannel> {

		@Override
		protected void initChannel(SocketChannel socketChannel) throws Exception{
			ChannelPipeline pipeline = socketChannel.pipeline();

			pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(1048576, 0, 4, 0, 4));
			pipeline.addLast("bytesDecoder", new ByteArrayDecoder());

			pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
			pipeline.addLast("bytesEncoder", new ByteArrayEncoder());

			// Set handler
			pipeline.addLast("idleStateHandler", new IdleStateHandler(60, 30, 0));
			pipeline.addLast("handler", new SocketServerHandler());
		}

	}

}
