package de.superioz.network.api.netty;

import de.superioz.network.api.NetworkAPI;

/**
 * Created on 27.04.2016.
 */
public class NettyHandler {

	public static final short DEFAULT_PORT = 23109;
	public static final String PASSWORD = "fQm5JTQGSj";
	public static final int MINIMUM_TIMEOUT = 20;

	public static final String GET_DATABASE_COMMAND = "getDatabase";
	public static final String GET_PARTY_COMMAND = "getParty";

	/**
	 * Wait for respond ..
	 */
	public static void waitFor(){
		try{
			Thread.sleep(MINIMUM_TIMEOUT);
		}
		catch(InterruptedException e){
			//
		}
	}

	/**
	 * Setups the socket server
	 *
	 * @return The server
	 */
	public static NioSocketServer setupServer(){
		final NioSocketServer server = new NioSocketServer(DEFAULT_PORT, PASSWORD);
		new Thread(new Runnable() {
			@Override
			public void run(){
				try{
					server.start();
				}
				catch(Exception e){
					NetworkAPI.getLogger().severe("Couldn't setup netty server!");
				}
			}
		}).run();
		NetworkAPI.getLogger().info("Netty master server started.");
		return server;
	}

	/**
	 * Creates a new nio socket client
	 *
	 * @param host     The host of the server
	 * @param clientIp The client ip
	 * @return The client
	 */
	public static NioSocketClient createNewClient(String host, String clientIp){
		final NioSocketClient client = new NioSocketClient(DEFAULT_PORT, host, clientIp);
		new Thread(new Runnable() {
			@Override
			public void run(){
				try{
					client.connect();
					NetworkAPI.getLogger().info("Netty client created and connected to master server.");
				}
				catch(Exception e){
					NetworkAPI.getLogger().severe("Couldn't create new netty client instance!");
				}
			}
		}).run();
		return client;
	}

	/**
	 * Registers given packet handler
	 *
	 * @param packetHandler The packet handler
	 */
	public static void registerPacketHandler(NioPacketHandler packetHandler){
		NioPacketHandlerRegistry.add(packetHandler);
	}

}
