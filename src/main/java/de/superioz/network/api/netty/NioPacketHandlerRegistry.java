package de.superioz.network.api.netty;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 27.04.2016.
 */
public class NioPacketHandlerRegistry {

	@Getter
	private static List<NioPacketHandler> packetHandlers = new ArrayList<>();

	/**
	 * Checks if the handler list contains given object
	 *
	 * @param handler The handler
	 * @return The result
	 */
	public static boolean contains(NioPacketHandler handler){
		return packetHandlers.contains(handler);
	}

	/**
	 * Adds a nio packet handler to the list
	 *
	 * @param handler The handler
	 */
	public static void add(NioPacketHandler handler){
		if(!contains(handler)){
			packetHandlers.add(handler);
		}
	}

	/**
	 * Removes a handler from the list
	 *
	 * @param handler The handler
	 */
	public static void remove(NioPacketHandler handler){
		if(contains(handler)){
			packetHandlers.remove(handler);
		}
	}


}
