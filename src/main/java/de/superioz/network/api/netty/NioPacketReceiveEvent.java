package de.superioz.network.api.netty;

import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;

/**
 * Created on 27.04.2016.
 */
@Getter
public class NioPacketReceiveEvent {

	private NioPacket packet;
	private ChannelHandlerContext context;
	private Channel sender;

	private ChannelType receiverType;
	private ChannelType senderType;

	/**
	 * Event when a server or a client receives given packet
	 *
	 * @param packet  The packet
	 * @param context The context
	 */
	public NioPacketReceiveEvent(NioPacket packet, ChannelHandlerContext context, ChannelType type){
		this.packet = packet;
		this.context = context;
		this.sender = context.channel();

		this.receiverType = type;

		if(type == ChannelType.SERVANT) this.senderType = ChannelType.MASTER;
		else this.senderType = ChannelType.SERVANT;
	}

	/**
	 * Respond to the before packet
	 *
	 * @param packet The packet
	 */
	public void respond(NioPacket packet){
		if(getPacket().getType() == NioPacketType.REQUEST){
			packet.setId(getPacket().getId());
			packet.setType(NioPacketType.RESPOND);
		}
		packet.setAuthentification(packet.getAuthentification());

		getSender().writeAndFlush(packet.compress());
	}

	/**
	 * Type of receiver
	 */
	public enum ChannelType {

		MASTER,
		SERVANT

	}

}