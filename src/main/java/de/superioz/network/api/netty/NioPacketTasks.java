package de.superioz.network.api.netty;

import de.superioz.sx.java.util.Consumer;

import java.util.HashMap;

/**
 * Created on 27.04.2016.
 */
class NioPacketTasks {

	private static HashMap<Integer, Consumer<NioPacket>> tasks = new HashMap<>();

	/**
	 * Checks if the tasks map contains given id
	 *
	 * @param id The id
	 * @return The result
	 */
	public static boolean contains(int id){
		return tasks.containsKey(id);
	}

	/**
	 * Get the task with given id
	 *
	 * @param id The id
	 * @return The consumer
	 */
	public static Consumer<NioPacket> query(int id){
		if(contains(id)){
			return tasks.get(id);
		}
		return null;
	}

	/**
	 * Add a task to the tasks map
	 *
	 * @param id       The id
	 * @param consumer The consumer
	 */
	public static void add(int id, Consumer<NioPacket> consumer){
		if(!contains(id)){
			tasks.put(id, consumer);
		}
	}

	/**
	 * Removes given id and the consumer by this id from map
	 *
	 * @param id The id
	 */
	public static void consumed(int id){
		if(contains(id)){
			tasks.remove(id);
		}
		NioPacketIdentifiers.idUsed(id);
	}

}
