package de.superioz.network.api;

import de.superioz.sx.java.util.JsonUtil;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

/**
 * Created on 05.05.2016.
 */
@Getter
public class SerializableParty {

	private UUID leader;
	private List<UUID> members;
	private List<UUID> invitations;

	private long timeStamp;
	private String rootOwner;
	private boolean disbanded = false;

	public SerializableParty(UUID leader, List<UUID> members, List<UUID> invitations, long timeStamp, String rootOwner, boolean disbanded){
		this.leader = leader;
		this.members = members;
		this.invitations = invitations;
		this.timeStamp = timeStamp;
		this.rootOwner = rootOwner;
		this.disbanded = disbanded;
	}

	/**
	 * Get this party as json string
	 *
	 * @return The string
	 */
	public String toJson(){
		return JsonUtil.getGson().toJson(this);
	}

	/**
	 * Get the json string as a serializable party
	 *
	 * @param json The json
	 * @return The party
	 */
	public static SerializableParty fromJson(String json){
		return JsonUtil.getGson().fromJson(json, SerializableParty.class);
	}

}
