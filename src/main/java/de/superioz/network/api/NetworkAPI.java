package de.superioz.network.api;

import de.superioz.network.api.mongo.MongoManager;
import de.superioz.network.api.registry.*;
import de.superioz.network.api.registry.ban.BanRegistry;
import de.superioz.network.api.registry.PermissionRegistry;
import de.superioz.network.bungee.util.Debugger;
import lombok.Getter;
import org.bson.BsonDocument;
import org.bson.Document;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created on 20.04.2016.
 */
public class NetworkAPI {

	@Getter
	private static MongoManager mongoManager;

	@Getter
	private static Logger logger;

	/**
	 * Connects to the database needed for various things
	 *
	 * @param collectionPrefix The collection prefix (e.g. 'network_')
	 * @param serverAddress    The server address (leave blank for localhost)
	 * @param port             The server port (set to -1 for default port)
	 * @param username         The username (leave blank for no credentials)
	 * @param password         The password (not needed if username is empty)
	 * @param database         The database (name of the database)
	 */
	public static void mongoConnect(String collectionPrefix, String serverAddress, int port,
	                                String username, String password, String database){
		if(mongoManager == null){
			mongoManager = new MongoManager(collectionPrefix, serverAddress,
					port, username, password, database);
			mongoManager.connect();
		}

		try{
			Thread.sleep(1000);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		if(checkMongo()){
			getLogger().info("Connected to mongo database.");
		}
		else{
			getLogger().severe("Couldn't connect to mongo database!");
		}
	}

	/**
	 * Check the mongo database connection
	 *
	 * @return The result
	 */
	public static boolean checkMongo(){
		long t = System.currentTimeMillis();
		try{
			mongoManager.getServer().getDatabase().runCommand(new Document("dbStats", 1));
			Debugger.write("MongoDB respond received. (" + (System.currentTimeMillis()-t) + "ms)");
			return true;
		}
		catch(Exception e){
			return false;
		}
	}

	/**
	 * Set the logger
	 *
	 * @param l The logger
	 */
	public static void setLogger(Logger l){
		logger = l;
	}

	/**
	 * Class for every registry (profile, friends, permissions ...)
	 */
	public static class Registry {

		public static final PlayerProfileRegistry PLAYER_PROFILE = new PlayerProfileRegistry();
		public static final FriendRegistry FRIEND = new FriendRegistry();
		public static final BanRegistry BAN = new BanRegistry();
		public static final PermissionRegistry PERMISSION = new PermissionRegistry();

	}

}
