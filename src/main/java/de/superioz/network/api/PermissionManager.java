package de.superioz.network.api;

import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PermissionWrap;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.sx.java.util.ListUtil;

import java.util.Arrays;
import java.util.List;

/**
 * Created on 30.04.2016.
 */
public class PermissionManager {

	/**
	 * Checks if given profile has given permission
	 *
	 * @param profile    The profile
	 * @param permission The permission
	 * @return The result
	 */
	public static boolean hasPermission(PlayerProfile profile, String permission, PermissionType type){
		if(profile == null || GroupManager.getGroup(profile) == null){
			return false;
		}
		GroupManager.synchronize();
		PermissionGroup group = GroupManager.getGroup(profile);

		// This should be a permission like minecraft.command.gamemode
		// Now check if the group has a permission like '*', 'minecraft.*' or 'minecraft.command.*'

		// Split the permission
		String[] spl = permission.split("\\.");
		for(int i = -1; i < spl.length-1; i++){
			String superPerm = i < 0 ? "" : spl[i] + ".";

			if(!(i-1 < 0)){
				String before = ListUtil.insert(Arrays.copyOfRange(spl, 0, i), ".");
				superPerm = before + "." + superPerm;
			}

			superPerm += "*";
			if(group.hasPermission(superPerm, type)){
				return true;
			}
		}
		return group.hasPermission(permission, type);
	}

	/**
	 * Checks if given permission for given profile is set
	 *
	 * @param profile    The profile
	 * @param permission The permission as string
	 * @return The result
	 */
	public static boolean isPermissionSet(PlayerProfile profile, String permission, PermissionType type){
		GroupManager.synchronize();
		PermissionGroup group = GroupManager.getGroup(profile);

		return group.isPermissionSet(permission, type);
	}

	/**
	 * Get all permission wrappers from given profile
	 *
	 * @param profile The profile
	 * @return The list of permissions
	 */
	public static List<PermissionWrap> getPermissions(PlayerProfile profile){
		GroupManager.synchronize();
		PermissionGroup group = GroupManager.getGroup(profile);
		return group.getPermissions(PermissionType.BUKKIT);
	}

}
