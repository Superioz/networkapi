package de.superioz.network.api;

import de.superioz.network.api.registry.SkinProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 03.05.2016.
 */
public class SkinCache {

	private static final Map<String, SkinProperty> CACHED_SKINS = new HashMap<>();

	/**
	 * Checks if map contains given name
	 *
	 * @param name The name
	 * @return The result
	 */
	public static boolean contains(String name){
		return CACHED_SKINS.containsKey(name);
	}

	/**
	 * Get skin property by given name
	 *
	 * @param name The name
	 * @return The skin property
	 */
	public static SkinProperty getProperty(String name){
		if(!contains(name)) return null;
		return CACHED_SKINS.get(name);
	}

	/**
	 * Writes given property into the map
	 *
	 * @param name     The name of the player
	 * @param property The property class
	 */
	public static void writeProperty(String name, SkinProperty property){
		if(contains(name)) return;
		CACHED_SKINS.put(name, property);
	}

}
