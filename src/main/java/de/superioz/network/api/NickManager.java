package de.superioz.network.api;

import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.SkinProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created on 02.05.2016.
 */
public class NickManager {

	public static final String NICK_COMMAND = "nick";
	public static final String UNNICK_COMMAND = "unnick";

	/**
	 * Get the nick name of the given player
	 *
	 * @param uuid The player
	 * @return The nick
	 */
	public static String getNick(UUID uuid){
		if(!ProfileManager.hasProfile(uuid)) return "";
		PlayerProfile profile = ProfileManager.getProfile(uuid);
		return profile.getNickName();
	}

	/**
	 * Set the nick name of the given player
	 *
	 * @param uuid     The uuid
	 * @param nickName The nickname
	 */
	public static void setNick(UUID uuid, String nickName){
		if(!ProfileManager.hasProfile(uuid)){
			return;
		}
		PlayerProfile profile = ProfileManager.getProfile(uuid);
		profile.setNickName(nickName);
	}

	/**
	 * Checks if the given player has a nick name
	 *
	 * @param uuid The player
	 * @return The result
	 */
	public static boolean hasNick(UUID uuid){
		return !getNick(uuid).isEmpty();
	}

	/**
	 * Get the real name of given player
	 *
	 * @param uuid The player
	 * @return The name as string
	 */
	public static String getRealName(UUID uuid){
		if(!ProfileManager.hasProfile(uuid)) return null;
		return ProfileManager.getProfile(uuid).getName();
	}

}
