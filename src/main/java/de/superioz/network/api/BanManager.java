package de.superioz.network.api;

import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.api.registry.ban.BanRegistry;
import de.superioz.network.api.registry.ban.BanType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created on 30.04.2016.
 */
public class BanManager {

	/**
	 * Get the next id
	 *
	 * @return The id
	 */
	public static int getNextId(){
		return (int) (NetworkAPI.Registry.BAN.getCollection().count() + 1);
	}

	/**
	 * Get all bans with given playerid
	 *
	 * @param playerId The player id
	 * @param type     The ban type
	 * @return The list of bans
	 */
	public static List<Ban> getBans(int playerId, BanType type){
		List<Ban> bans = getBans(playerId);
		List<Ban> newBans = new ArrayList<>();

		if(bans != null && !newBans.isEmpty()){
			for(Ban b : bans){
				if(b.getType() == type){
					newBans.add(b);
				}
			}
		}
		return newBans;
	}

	public static List<Ban> getBans(String playerIp, BanType type){
		List<Ban> bans = getBans(playerIp);
		List<Ban> newBans = new ArrayList<>();

		if(bans != null && !newBans.isEmpty()){
			for(Ban b : bans){
				if(b.getType() == type){
					newBans.add(b);
				}
			}
		}
		return newBans;
	}

	/**
	 * Get the ban with given values
	 *
	 * @param playerId The player id
	 * @param type     The ban type
	 * @param exact    Exact this ban type
	 * @return The ban
	 */
	public static Ban getBan(int playerId, BanType type, boolean exact){
		List<Ban> bans = getBans(playerId);
		if(bans == null){
			return null;
		}

		// Check bans
		for(Ban b : bans){
			boolean result = exact ? (type == b.getType()) : (type.compare(b.getType()));

			if(result && !b.isExpired()){
				return b;
			}
		}
		return null;
	}

	/**
	 * Get the ban from given player name and ban type
	 *
	 * @param name  The name
	 * @param type  The bantype
	 * @param exact Exact?
	 * @return The ban
	 */
	public static Ban getBan(String name, BanType type, boolean exact){
		List<Ban> bans = getBans(name);
		if(bans == null){
			return null;
		}

		// Check bans
		for(Ban b : bans){
			boolean result = exact ? (type == b.getType()) : (type.compare(b.getType()));

			if(result && !b.isExpired()){
				return b;
			}
		}
		return null;
	}

	/**
	 * Get the ban for given ip and ban type
	 *
	 * @param ip    The ip
	 * @param type  The ban type
	 * @param exact Exact?
	 * @return The ban
	 */
	public static Ban getIpBan(String ip, BanType type, boolean exact){
		List<Ban> bans = getBans(ip);
		if(bans == null){
			return null;
		}

		// Check bans
		for(Ban b : bans){
			boolean result = exact ? (type == b.getType()) : (type.compare(b.getType()));

			if(result && !b.isExpired()){
				return b;
			}
		}
		return null;
	}

	/**
	 * Get all bans with given playerid
	 *
	 * @param playerId The player id
	 * @return The list of bans
	 */
	public static List<Ban> getBans(int playerId){
		return NetworkAPI.Registry.BAN.readByBannedId(playerId);
	}

	public static List<Ban> getBans(String ip){
		return NetworkAPI.Registry.BAN.readByBannedIp(ip);
	}

	/**
	 * Get ban from given id
	 *
	 * @param banId The ban id
	 * @return The ban
	 */
	public static Ban getBanFromId(int banId){
		return NetworkAPI.Registry.BAN.readById(banId);
	}

	/**
	 * Get ban points from given player
	 *
	 * @param playerId The player id
	 * @return The value of ban points
	 */
	public static int getBanPoints(int playerId){
		List<Ban> bans = getBans(playerId);
		int counter = 0;

		if(bans != null){
			for(Ban b : bans){
				if(b.getType() != BanType.CHAT)
					counter += b.getBanReason().getBanPoints();
			}
		}
		return counter;
	}

	/**
	 * Get player ban with only given player id
	 *
	 * @param playerId The player id
	 * @return The ban
	 */
	public static Ban getBan(int playerId){
		return getBan(playerId, BanType.GLOBAL_UUID, false);
	}

	public static Ban getBan(String name){
		return getBan(name, BanType.GLOBAL_UUID, false);
	}

	/**
	 * Get the chatban from this player id if exists
	 *
	 * @param playerId The player id
	 * @return The ban
	 */
	public static Ban getChatBan(int playerId){
		return getBan(playerId, BanType.CHAT, true);
	}

	public static Ban getChatBan(UUID uuid){
		return getChatBan(ProfileManager.getId(uuid));
	}

	/**
	 * Get ip ban with only given ip
	 *
	 * @param ip The ip
	 * @return The ban
	 */
	public static Ban getIPBan(String ip){
		return getIpBan(ip, BanType.GLOBAL_IP, true);
	}

	/**
	 * Checks if the given player id is banned
	 *
	 * @param playerId The player id
	 * @param type     The ban type (chat, ip)
	 * @param exact    Search for exact ban with ban type?
	 * @return The result
	 */
	public static boolean isBanned(int playerId, BanType type, boolean exact){
		return getBan(playerId, type, exact) != null;
	}

	/**
	 * Checks if the given profile is server banned
	 *
	 * @param profile The profile
	 * @return The result
	 */
	public static boolean isBanned(PlayerProfile profile){
		return isBanned(profile.getId(), BanType.GLOBAL_UUID, false);
	}

	public static boolean isBanned(int id){
		return isBanned(id, BanType.GLOBAL_UUID, false);
	}

	/**
	 * Checks if the given player is chatBanned
	 *
	 * @param playerId The player id
	 * @return The result
	 */
	public static boolean isChatBanned(int playerId){
		return isBanned(playerId, BanType.CHAT, true);
	}

	/**
	 * Checks if the given ip is banned
	 *
	 * @param ip The ip
	 * @return The result
	 */
	public static boolean isBanned(String ip){
		return getIPBan(ip) != null;
	}

	/**
	 * Removes a ban with given
	 *
	 * @param index The index
	 * @param value The value
	 * @param type  The ban type
	 * @return The result
	 */
	public static boolean unban(String index, Object value, BanType type){
		return NetworkAPI.Registry.BAN.delete(index, value, type);
	}

	/**
	 * Unbans a player with given playerId and given ban typer
	 *
	 * @param playerId The player id
	 * @param type     The ban type
	 * @return The result
	 */
	public static boolean unbanPlayer(int playerId, BanType type){
		return unban(BanRegistry.ID_BANNED_ID, playerId, type);
	}

	/**
	 * Removes a ban with given ban id
	 *
	 * @param banId The ban id
	 * @return The result
	 */
	public static boolean removeBan(int banId){
		return unban(BanRegistry.ID_ID, banId, null);
	}

}
