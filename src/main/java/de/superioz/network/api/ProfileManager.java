package de.superioz.network.api;

import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.api.registry.PlayerProfileRegistry;
import org.apache.commons.lang.ObjectUtils;

import java.util.UUID;

/**
 * Created on 30.04.2016.
 */
public class ProfileManager {

	public static final PlayerProfile CONSOLE_PROFILE = new PlayerProfile() {
		@Override
		public UUID getUniqueId(){
			return null;
		}

		@Override
		public int getId(){
			return 0;
		}

		@Override
		public String getName(){
			return "console";
		}

		@Override
		public String getIp(){
			return "localhost";
		}

		@Override
		public String getGroup(){
			return null;
		}

		@Override
		public String getNickName(){
			return "";
		}
	};

	/**
	 * Checks if the database contains given uuid
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public static boolean contains(UUID uuid){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsByUniqueId(uuid);
	}

	public static boolean contains(int id){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsById(id);
	}

	public static boolean contains(String name){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsByName(name);
	}

	/**
	 * Checks if given unique id has a profile
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public static boolean hasProfile(UUID uuid){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsByUniqueId(uuid);
	}

	public static boolean hasProfile(String name){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsByName(name);
	}

	public static boolean hasProfile(int id){
		return NetworkAPI.Registry.PLAYER_PROFILE.containsById(id);
	}

	/**
	 * Get the profile with given unique id
	 *
	 * @param uuid The uuid
	 * @return The profile
	 */
	public static PlayerProfile getProfile(UUID uuid){
		return NetworkAPI.Registry.PLAYER_PROFILE.readByUniqueId(uuid);
	}

	public static PlayerProfile getProfile(String name){
		return NetworkAPI.Registry.PLAYER_PROFILE.readByName(name);
	}

	public static PlayerProfile getProfile(int id){
		return NetworkAPI.Registry.PLAYER_PROFILE.readById(id);
	}

	/**
	 * Get the id by unique id
	 *
	 * @param uuid The unique id
	 * @return The id as integer
	 */
	public static int getId(UUID uuid){
		return NetworkAPI.Registry.PLAYER_PROFILE.getIdByUniqueId(uuid);
	}

	public static int getId(String name){
		return NetworkAPI.Registry.PLAYER_PROFILE.getIdByName(name);
	}

	/**
	 * Get unique id by given id
	 *
	 * @param id The id
	 * @return The uuid
	 */
	public static UUID getUniqueId(int id){
		return NetworkAPI.Registry.PLAYER_PROFILE.getUniqueIdById(id);
	}

	public static UUID getUniqueId(String name){
		return NetworkAPI.Registry.PLAYER_PROFILE.getUniqueIdByName(name);
	}

	/**
	 * Get the name by given uuid
	 *
	 * @param uuid The uuid
	 * @return The name
	 */
	public static String getName(UUID uuid){
		return NetworkAPI.Registry.PLAYER_PROFILE.getNameByUniqueId(uuid);
	}

	public static String getName(int id){
		return NetworkAPI.Registry.PLAYER_PROFILE.getNameById(id);
	}

	/**
	 * Writes or updates the entry with given values and returns the profile then
	 *
	 * @param uuid The unique id
	 * @param name The name
	 * @param ip   The ip
	 * @return The profile
	 */
	public static PlayerProfile writeSoft(UUID uuid, String name, String nick, String ip){
		if(hasProfile(uuid)){
			NetworkAPI.Registry.PLAYER_PROFILE.update(PlayerProfileRegistry.ID_UUID, uuid, name, nick, ip, "");
		}
		else{
			PermissionGroup d = GroupManager.createDefault();
			int id = (int) (NetworkAPI.Registry.PLAYER_PROFILE.count() + 1);
			NetworkAPI.Registry.PLAYER_PROFILE.write(uuid, id, name, nick, ip, d.getName());
		}
		return getProfile(uuid);
	}

	public static PlayerProfile writeSoft(UUID uuid, String name, String ip){
		return writeSoft(uuid, name, null, ip);
	}

}
