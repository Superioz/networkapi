package de.superioz.network.api;

import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PermissionWrap;
import de.superioz.network.api.registry.PlayerProfile;

import java.util.*;

/**
 * Created on 30.04.2016.
 */
public class GroupManager {

	private static List<PermissionGroup> groupList = new ArrayList<>();

	/**
	 * Synchronizes this class
	 * <p>
	 * Get all groups written in the database and write them
	 * to this list
	 */
	public static void synchronize(){
		groupList = NetworkAPI.Registry.PERMISSION.readAll();
		doCleanup();
	}

	/**
	 * Get all groups
	 *
	 * @return The list of groups
	 */
	public static List<PermissionGroup> getGroups(){
		return groupList;
	}

	/**
	 * Gets a new rank
	 *
	 * @return The rank as int
	 */
	public static int getNewRank(){
		return groupList.size() + 123;
	}

	/**
	 * This method checks if some values of the groups are invalid and
	 * fixes them
	 */
	private static void doCleanup(){
		boolean hasDefault = false;
		final int MAX_NAME_LENGTH = 32;
		final int MAX_STRING_LENGTH = 48;
		int changed = 0;

		for(PermissionGroup group : groupList){
			if(group.getName().length() > MAX_NAME_LENGTH){
				group.setName(group.getName().substring(0, MAX_NAME_LENGTH));
				changed++;
			}
			if(group.getChatPrefix().length() > MAX_STRING_LENGTH){
				group.setChatPrefix(group.getChatPrefix().substring(0, MAX_STRING_LENGTH));
				changed++;
			}
			if(group.getChatSuffix().length() > MAX_STRING_LENGTH){
				group.setChatSuffix(group.getChatSuffix().substring(0, MAX_STRING_LENGTH));
				changed++;
			}
			if(group.getTabPrefix().length() > MAX_STRING_LENGTH){
				group.setTabPrefix(group.getTabPrefix().substring(0, MAX_STRING_LENGTH));
				changed++;
			}
			if(group.getTabSuffix().length() > MAX_STRING_LENGTH){
				group.setTabSuffix(group.getTabSuffix().substring(0, MAX_STRING_LENGTH));
				changed++;
			}
			if(!group.getColor().contains("&")
					|| group.getColor().length() > 2){
				group.setColor("");
				changed++;
			}
			if(group.isDefault()){
				if(group.isArtificially() || hasDefault){
					group.setDefault(false);
					changed++;
				}
				if(!hasDefault)
					hasDefault = true;
			}
			for(String s : group.getInheritances()){
				if(!hasGroup(s)){
					group.removeInheritance(s);
					changed++;
				}
			}

			if(changed > 0){
				group.updateData();
				groupList = NetworkAPI.Registry.PERMISSION.readAll();
			}
		}
	}

	/**
	 * Get the group with the given name
	 *
	 * @param name The name
	 * @return The group
	 */
	public static PermissionGroup getGroup(String name){
		for(PermissionGroup g : groupList){
			if(g.getName().equals(name)){
				return g;
			}
		}
		return null;
	}

	/**
	 * Get the group of the given player
	 *
	 * @param profile The profile
	 * @return The permission group
	 */
	public static PermissionGroup getGroup(PlayerProfile profile){
		PermissionGroup g = getGroup(profile.getGroup());
		if(g != null && !g.isArtificially())
			return g;
		return createDefault();
	}

	public static PermissionGroup getGroup(UUID uuid){
		return getGroup(ProfileManager.getProfile(uuid));
	}

	/**
	 * Get the group with the given rank
	 *
	 * @param rank The rank
	 * @return The group
	 */
	public static PermissionGroup getGroup(int rank){
		for(PermissionGroup g : groupList){
			if(g.getRank() == rank){
				return g;
			}
		}
		return null;
	}

	/**
	 * Get the default group
	 *
	 * @return The group
	 */
	public static PermissionGroup getDefault(){
		for(PermissionGroup g : groupList){
			if(g.isDefault() && !g.isArtificially()){
				return g;
			}
		}
		return null;
	}

	/**
	 * Checks if the database contains given group
	 *
	 * @param name The name
	 * @return The result
	 */
	public static boolean hasGroup(String name){
		return getGroup(name) != null;
	}

	/**
	 * Checks if the group already exists
	 *
	 * @param rank The rank
	 * @return The result
	 */
	public static boolean hasGroup(int rank){
		return getGroup(rank) != null;
	}

	/**
	 * Get all permission of the given group
	 *
	 * @param group The group
	 * @return The list of permissions
	 */
	public static List<PermissionWrap> getPermissions(PermissionGroup group){
		List<PermissionWrap> permissions = new ArrayList<>();
		permissions.addAll(group.getPermissions());

		for(PermissionGroup g : getInheritancesRecursive(group)){
			permissions.addAll(g.getPermissions());
		}
		return permissions;
	}


	/**
	 * Creates a new group
	 *
	 * @param name       The name
	 * @param rank       The rank
	 * @param isDefault  Is this the default group
	 * @param isArtifact The artifact
	 * @return The group
	 */
	public static PermissionGroup createGroup(String name, int rank, boolean isDefault, boolean isArtifact){
		if(hasGroup(name)){
			return getGroup(name);
		}
		if(hasGroup(rank)){
			return getGroup(rank);
		}
		PermissionGroup d = NetworkAPI.Registry.PERMISSION.write(name, isDefault, isArtifact, rank,
				"", "", "", "", "", new ArrayList<String>(), new ArrayList<String>());
		synchronize();
		return d;
	}

	/**
	 * Delete group with given name
	 *
	 * @param name The name
	 * @return The result
	 */
	public static boolean deleteGroup(String name){
		if(!hasGroup(name)) return false;
		NetworkAPI.Registry.PERMISSION.delete(name);
		synchronize();
		return !GroupManager.hasGroup(name);
	}

	/**
	 * Get permissions from given group and type
	 *
	 * @param group The group
	 * @param type  The permission type (bukkit, bungee)
	 * @return The list of permissions
	 */
	public static List<PermissionWrap> getPermissions(PermissionGroup group, PermissionType type){
		List<PermissionWrap> permissions = new ArrayList<>();
		permissions.addAll(group.getPermissions(type));

		for(PermissionGroup g : getInheritancesRecursive(group)){
			for(PermissionWrap w : g.getPermissions()){
				if(w.getType() == type){
					permissions.add(w);
				}
			}
		}
		return permissions;
	}

	/**
	 * Get all inheritances from given group (RECURSIVE)
	 *
	 * @param group The group
	 * @return The list of permissionGroups
	 */
	public static Set<PermissionGroup> getInheritancesRecursive(PermissionGroup group){
		Set<String> l = new HashSet<>();
		List<PermissionGroup> current = new ArrayList<>();
		List<PermissionGroup> switchCurrent = new ArrayList<>();

		current.addAll(getInheritances(group));
		while(!current.isEmpty()){
			for(PermissionGroup g : current){
				if(g.getInheritances().size() == 0){
					continue;
				}
				switchCurrent.addAll(getInheritances(g));
			}

			for(PermissionGroup p : current){
				l.add(p.getName());
			}
			current.clear();
			current.addAll(switchCurrent);
			switchCurrent.clear();
		}

		Set<PermissionGroup> inheritances = new HashSet<>();
		for(String s : l){
			inheritances.add(getGroup(s));
		}
		return inheritances;
	}

	/**
	 * Get all inheritances from given group
	 *
	 * @param group The group
	 * @return The list of permissionGroups
	 */
	public static List<PermissionGroup> getInheritances(PermissionGroup group){
		List<PermissionGroup> l = new ArrayList<>();

		for(String s : group.getInheritances()){
			PermissionGroup g;
			if((g = getGroup(s)) != null
					&& !l.contains(g)){
				l.add(g);
			}
		}
		return l;
	}

	/**
	 * Creates a new default group
	 *
	 * @return The group
	 */
	public static PermissionGroup createDefault(){
		PermissionGroup g = getDefault();
		if(g != null){
			return g;
		}

		PermissionGroup d = NetworkAPI.Registry.PERMISSION.write("default", true, false, 999,
				"", "", "", "", "", new ArrayList<String>(), new ArrayList<String>());
		synchronize();
		return d;
	}

}
