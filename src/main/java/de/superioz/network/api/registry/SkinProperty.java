package de.superioz.network.api.registry;

import lombok.Getter;

/**
 * Created on 03.05.2016.
 */
@Getter
public class SkinProperty {

	private String name;
	private String value;
	private String signature;

	public SkinProperty(String name, String signature, String value){
		this.name = name;
		this.signature = signature;
		this.value = value;
	}
	
}
