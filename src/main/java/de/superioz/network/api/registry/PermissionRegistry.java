package de.superioz.network.api.registry;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.superioz.network.api.mongo.MongoRegistry;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
@SuppressWarnings("unchecked")
public class PermissionRegistry implements MongoRegistry<PermissionGroup> {

	public static final String COLLECTION_NAME = "groups";
	public static final String ID_NAME = "name";
	public static final String ID_DEFAULT = "default";
	public static final String ID_ARTIFICIALLY = "artificially";
	public static final String ID_RANK = "rank";
	public static final String ID_CHAT_PREFIX = "chatPrefix";
	public static final String ID_CHAT_SUFFIX = "chatSuffix";
	public static final String ID_TAB_PREFIX = "tabPrefix";
	public static final String ID_TAB_SUFFIX = "tabSuffix";
	public static final String ID_COLOR = "color";
	public static final String ID_PERMISSIONS = "permissions";
	public static final String ID_INHERITANCES = "inheritances";

	@Override
	public MongoCollection getCollection(){
		return mongoManager.getCollection(getName());
	}

	@Override
	public String getName(){
		return COLLECTION_NAME;
	}

	@Override
	public boolean contains(String index, Object value){
		return getCollection().find(Filters.eq(index, value)).iterator().hasNext();
	}

	public boolean contains(String name){
		return contains(ID_NAME, name);
	}

	public boolean contains(int rank){
		return contains(ID_RANK, rank);
	}

	@Override
	public List<PermissionGroup> read(String index, Object value){
		final List<PermissionGroup> groups = new ArrayList<>();

		getCollection().find(Filters.eq(index, value)).forEach(new Block<Document>() {
			@Override
			public void apply(Document document){
				groups.add(getGroup(
						document.getString(ID_NAME),
						document.getBoolean(ID_DEFAULT),
						document.getBoolean(ID_ARTIFICIALLY),
						document.getInteger(ID_RANK),
						document.getString(ID_CHAT_PREFIX),
						document.getString(ID_CHAT_SUFFIX),
						document.getString(ID_TAB_PREFIX),
						document.getString(ID_TAB_SUFFIX),
						document.getString(ID_COLOR),
						(List<String>) document.get(ID_PERMISSIONS),
						(List<String>) document.get(ID_INHERITANCES)));
			}
		});
		return groups;
	}

	public PermissionGroup read(String name){
		return read(ID_NAME, name).get(0);
	}

	public PermissionGroup read(int rank){
		return read(ID_RANK, rank).get(0);
	}

	/**
	 * Gets all groups in the database
	 *
	 * @return The list of groups
	 */
	public List<PermissionGroup> readAll(){
		final List<PermissionGroup> groups = new ArrayList<>();

		getCollection().find().forEach(new Block<Document>() {
			@Override
			public void apply(Document document){
				groups.add(getGroup(
						document.getString(ID_NAME),
						document.getBoolean(ID_DEFAULT),
						document.getBoolean(ID_ARTIFICIALLY),
						document.getInteger(ID_RANK),
						document.getString(ID_CHAT_PREFIX),
						document.getString(ID_CHAT_SUFFIX),
						document.getString(ID_TAB_PREFIX),
						document.getString(ID_TAB_SUFFIX),
						document.getString(ID_COLOR),
						(List<String>) document.get(ID_PERMISSIONS),
						(List<String>) document.get(ID_INHERITANCES)));
			}
		});
		return groups;
	}

	@Override
	public void delete(String index, Object value){
		getCollection().deleteOne(Filters.eq(index, value));
	}

	public void delete(String name){
		delete(ID_NAME, name);
	}

	public void delete(int rank){
		delete(ID_RANK, rank);
	}

	/**
	 * Writes a group into the database
	 *
	 * @param name           The name
	 * @param isDefault      Is this the default group?
	 * @param isArtificially Is the group just artificially (ignores somes values, only for permissions)
	 * @param rank           The rank
	 * @param chatPrefix     The chat prefix
	 * @param chatSuffix     The chat suffix
	 * @param tabPrefix      The tab prefix
	 * @param tabSuffix      The tab suffix
	 * @param color          The color
	 * @param permissions    The permissions
	 * @param inheritances   The inheritances
	 * @return The group
	 */
	public PermissionGroup write(final String name, final boolean isDefault, final boolean isArtificially,
	                             final int rank, final String chatPrefix, final String chatSuffix,
	                             final String tabPrefix, final String tabSuffix, final String color,
	                             final List<String> permissions, final List<String> inheritances){
		if(contains(ID_NAME, name)){
			return read(name);
		}
		else if(contains(ID_RANK, rank)){
			return read(rank);
		}
		PermissionGroup group = getGroup(name, isDefault, isArtificially, rank, chatPrefix, chatSuffix,
				tabPrefix, tabSuffix, color, permissions, inheritances);

		// Write
		getCollection().insertOne(new Document()
				.append(ID_NAME, name)
				.append(ID_DEFAULT, isDefault)
				.append(ID_ARTIFICIALLY, isArtificially)
				.append(ID_RANK, rank)
				.append(ID_CHAT_PREFIX, chatPrefix)
				.append(ID_CHAT_SUFFIX, chatSuffix)
				.append(ID_TAB_PREFIX, tabPrefix)
				.append(ID_TAB_SUFFIX, tabSuffix)
				.append(ID_COLOR, color)
				.append(ID_PERMISSIONS, permissions)
				.append(ID_INHERITANCES, inheritances)
		);
		return group;
	}

	/**
	 * Updates an entry
	 *
	 * @param index          The index for lookup
	 * @param value          The value for lookup
	 * @param name           The name
	 * @param isDefault      Is default
	 * @param isArtificially Is artificially
	 * @param rank           The rank
	 * @param chatPrefix     The chat prefix
	 * @param chatSuffix     The chat suffix
	 * @param tabPrefix      The tab prefix
	 * @param tabSuffix      The tab suffix
	 * @param color          The color
	 * @param permissions    The permissions
	 * @param inheritances   The inheritances
	 */
	public void update(final String index, final Object value, final String name,
	                   final boolean isDefault, final boolean isArtificially,
	                   final int rank, final String chatPrefix, final String chatSuffix,
	                   final String tabPrefix, final String tabSuffix, final String color,
	                   final List<String> permissions, final List<String> inheritances){
		Document updateDoc = new Document();
		if(!name.isEmpty()){
			updateDoc.append(ID_NAME, name);
		}
		updateDoc.append(ID_DEFAULT, isDefault);
		updateDoc.append(ID_ARTIFICIALLY, isArtificially);
		if(rank != -1){
			updateDoc.append(ID_RANK, rank);
		}
		if(!chatPrefix.isEmpty()){
			updateDoc.append(ID_CHAT_PREFIX, chatPrefix);
		}
		if(!chatSuffix.isEmpty()){
			updateDoc.append(ID_CHAT_SUFFIX, chatSuffix);
		}
		if(!tabPrefix.isEmpty()){
			updateDoc.append(ID_TAB_PREFIX, tabPrefix);
		}
		if(!tabSuffix.isEmpty()){
			updateDoc.append(ID_TAB_SUFFIX, tabSuffix);
		}
		if(!color.isEmpty()){
			updateDoc.append(ID_COLOR, color);
		}
		if(permissions != null){
			updateDoc.append(ID_PERMISSIONS, permissions);
		}
		if(inheritances != null){
			updateDoc.append(ID_INHERITANCES, inheritances);
		}

		// Update
		getCollection().updateOne(Filters.eq(index, value),
				new Document("$set", updateDoc));
	}

	/**
	 * Get the group by given values
	 *
	 * @param name           The name
	 * @param isDefault      Is this the default group
	 * @param isArtificially Is the group just artificially (ignores somes values, only for permissions)
	 * @param rank           The rank
	 * @param chatPrefix     The chat prefix (can be empty)
	 * @param chatSuffix     The chat suffix (can be empty)
	 * @param tabPrefix      The tab prefix (can be empty)
	 * @param tabSuffix      The tab suffix (can be empty)
	 * @param color          The color (can be empty)
	 * @param permissions    The permissions (can be empty)
	 * @param inheritances   The inheritances (can be empty)
	 * @return The group
	 */
	private PermissionGroup getGroup(final String name, final boolean isDefault, final boolean isArtificially,
	                                 final int rank, final String chatPrefix, final String chatSuffix,
	                                 final String tabPrefix, final String tabSuffix, final String color,
	                                 final List<String> permissions, final List<String> inheritances){
		return new PermissionGroup(name, isDefault, isArtificially, rank,
				chatPrefix, chatSuffix, tabPrefix, tabSuffix, color, inheritances, permissions);
	}

}
