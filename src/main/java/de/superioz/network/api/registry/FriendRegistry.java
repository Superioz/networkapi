package de.superioz.network.api.registry;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.superioz.network.api.mongo.MongoRegistry;
import de.superioz.sx.java.util.ListUtil;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
@SuppressWarnings("unchecked")
public class FriendRegistry implements MongoRegistry<Friend> {

	public static final String COLLECTION_NAME = "friends";
	public static final String ID_ID = "id";
	public static final String ID_REQUESTS = "requests";
	public static final String ID_FRIENDS = "friends";

	@Override
	public MongoCollection getCollection(){
		return mongoManager.getCollection(getName());
	}

	@Override
	public String getName(){
		return COLLECTION_NAME;
	}

	@Override
	public boolean contains(String index, Object value){
		return getCollection().find(Filters.eq(index, value)).iterator().hasNext();
	}

	public boolean containsById(int id){
		return contains(ID_ID, id);
	}

	@Override
	public List<Friend> read(String index, Object value){
		final List<Friend> friendList = new ArrayList<>();

		getCollection().find(Filters.eq(index, value)).limit(1).forEach(new Block<Document>() {
			@Override
			public void apply(Document document){
				final int id = document.getInteger(ID_ID);
				final String requests = document.getString(ID_REQUESTS);
				final String friends = document.getString(ID_FRIENDS);

				friendList.add(getFriend(id, requests, friends));
			}
		});
		return friendList;
	}

	public Friend read(int id){
		return read(ID_ID, id).get(0);
	}

	@Override
	public void delete(String index, Object value){
		getCollection().deleteOne(Filters.eq(index, value));
	}

	/**
	 * Write into database
	 *  @param id       The player id
	 * @param requests The requests
	 * @param friends  The friends
	 */
	public Friend write(final int id, final List<String> requests, final List<String> friends){
		if(contains(ID_ID, id)){
			return read(id);
		}
		Friend friend = getFriend(id, ListUtil.insert(requests, Friend.SPLITERATOR),
				ListUtil.insert(friends, Friend.SPLITERATOR));

		// Write
		getCollection().insertOne(new Document()
				.append(ID_ID, friend.getPlayerId())
				.append(ID_FRIENDS, friend.getFriendsAsString())
				.append(ID_REQUESTS, friend.getRequestsAsString()));
		return friend;
	}

	/**
	 * Updates entries
	 *
	 * @param index    The index
	 * @param requests The requests
	 * @param friends  The friends
	 */
	public void update(final String index, final Object value,
	                   final String requests, final String friends){
		Document updateDoc = new Document();
		updateDoc.append(ID_REQUESTS, requests);
		updateDoc.append(ID_FRIENDS, friends);

		// Update
		getCollection().updateOne(Filters.eq(index, value),
				new Document("$set", updateDoc));
	}

	/**
	 * Get the friend by given values
	 *
	 * @param id       The id
	 * @param requests The requests
	 * @param friends  The friends
	 * @return The friend
	 */
	private Friend getFriend(final int id, final String requests, final String friends){
		return new Friend(id, requests, friends);
	}

}
