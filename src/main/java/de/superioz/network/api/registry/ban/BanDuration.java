package de.superioz.network.api.registry.ban;

import de.superioz.sx.bungee.command.CommandFlag;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created on 23.04.2016.
 */
@Getter
public class BanDuration {

	private int value;
	private Type type;

	/**
	 * Simply the constructor
	 *
	 * @param value The value
	 * @param type  The type
	 */
	public BanDuration(int value, Type type){
		this.value = value;
		this.type = type;
	}

	/**
	 * Get the milliseconds from durationlist
	 *
	 * @param durationList The list
	 * @return The milliseconds
	 */
	public static long calculateMillis(List<BanDuration> durationList){
		long millis = 0;
		for(BanDuration duration : durationList){
			millis += duration.getMillis();
		}
		return millis;
	}

	/**
	 * Get ban duration from parent
	 *
	 * @param reason The reason
	 * @return The duration
	 */
	public static BanDuration fromParentReason(BanReason.Parent reason){
		switch(reason.getBanPoints()){
			case 1:
				return new BanDuration(3, Type.DAY);
			case 2:
				return new BanDuration(14, Type.DAY);
			case 3:
				return new BanDuration(30, Type.DAY);
		}
		return null;
	}

	/**
	 * Get the time string from time difference
	 *
	 * @param difference The difference
	 * @return The string
	 */
	public static String fromDifference(long difference){
		StringBuilder builder = new StringBuilder("");

		// Get time units
		int seconds = (int) (difference / 1000);
		int day = (int) TimeUnit.SECONDS.toDays(seconds);
		long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
		long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
		long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

		// Append
		if(day > 0){
			builder.append(day).append("d");
		}
		if(hours > 0){
			builder.append(" ").append(hours).append("h");
		}
		if(minute > 0){
			builder.append(" ").append(minute).append("m");
		}
		if(second > 0){
			builder.append(" ").append(second).append("s");
		}

		return builder.toString();
	}

	/**
	 * Get the duration from given commandContext
	 *
	 * @param context The context
	 * @return The milliseconds
	 */
	public static long fromCommandContext(CommandContext context){
		List<BanDuration> durationList = new ArrayList<>();
		if(context.hasFlag(Type.PERMANENT.getName())){
			return Type.PERMANENT.getSeconds();
		}
		for(Type t : Type.values()){
			String identifier = t.getName();

			if(!context.hasFlag(identifier)){
				continue;
			}

			CommandFlag flag = context.getCommandFlag(identifier);
			String arg = flag.getArgument(1);
			int value = 1;
			if(SimpleStringUtils.isInteger(arg)){
				int v = Integer.valueOf(arg);

				if(v > 1){
					value = v;
				}
			}

			durationList.add(new BanDuration(value, t));
		}
		return calculateMillis(durationList);
	}

	/**
	 * Get millis from this duration
	 *
	 * @return The milliseconds
	 */
	public long getMillis(){
		return type.getMillis() * value;
	}

	@Getter
	public enum Type {

		PERMANENT("p", -1),
		SECOND("s", 1),
		MINUTE("m", SECOND.getSeconds() * 60),
		HOUR("h", MINUTE.getSeconds() * 60),
		DAY("d", HOUR.getSeconds() * 24),
		WEEK("w", DAY.getSeconds() * 7),
		MONTH("M", DAY.getSeconds() * 30),
		YEAR("y", MONTH.getSeconds() * 12);

		private String name;
		private int seconds;

		Type(String name, int seconds){
			this.name = name;
			this.seconds = seconds;
		}

		/**
		 * Get all values from enum as strings
		 *
		 * @return The list
		 */
		public static List<String> getValues(){
			List<String> stringList = new ArrayList<>();

			for(BanDuration.Type type : values()){
				stringList.add(type.getName());
			}
			return stringList;
		}

		/**
		 * Get the seconds in millis
		 *
		 * @return The milliseconds
		 */
		public long getMillis(){
			return ((long) getSeconds()) * 1000;
		}

	}

}
