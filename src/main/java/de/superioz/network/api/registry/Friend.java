package de.superioz.network.api.registry;

import de.superioz.network.api.FriendManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.bungee.common.FriendHandler;
import de.superioz.network.bungee.common.chat.ChatManager;
import de.superioz.network.bungee.event.FriendUpdateEvent;
import de.superioz.network.bungee.util.Prefixes;
import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.chat.BungeeChat;
import de.superioz.sx.java.util.ListUtil;
import de.superioz.sx.java.util.SimpleStringUtils;
import de.superioz.network.api.NetworkAPI;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created on 20.04.2016.
 */
@Getter
public class Friend {

	public static final String SPLITERATOR = ";";
	private int playerId;

	private List<Integer> rawRequests;
	private List<Integer> rawFriends;

	/**
	 * Simply the constructor
	 *
	 * @param playerId         The player id
	 * @param requestsAsString The requests
	 * @param friendsAsString  The friends
	 */
	Friend(int playerId, String requestsAsString, String friendsAsString){
		this.playerId = playerId;
		this.rawRequests = getRawRequests(requestsAsString);
		this.rawFriends = getRawFriends(friendsAsString);
	}

	/**
	 * Get the chat color
	 *
	 * @return The color as string
	 */
	public String getChatColor(){
		return ChatManager.getColor(getUniqueId());
	}

	/**
	 * Get registry with name
	 *
	 * @param name The name
	 * @return The registry
	 */
	public Friend getFriend(String name){
		for(Friend friend : getFriends()){
			if(friend.getName().equals(name)){
				return friend;
			}
		}
		return null;
	}

	/**
	 * Get the playerprofile of this friend
	 *
	 * @return The profile
	 */
	public PlayerProfile getProfile(){
		return NetworkAPI.Registry.PLAYER_PROFILE.readById(getPlayerId());
	}

	/**
	 * Gets the unique id
	 *
	 * @return The unique id
	 */
	public UUID getUniqueId(){
		return getProfile().getUniqueId();
	}

	/**
	 * Get the request list as integer list
	 *
	 * @return The integer list
	 */
	private List<Integer> getRawRequests(String req){
		List<Integer> requests = new ArrayList<>();
		String[] spl = req.split(SPLITERATOR);

		for(String s : spl){
			if(SimpleStringUtils.isInteger(s)){
				requests.add(Integer.valueOf(s));
			}
		}

		return requests;
	}

	/**
	 * Get the registry requests as registry list
	 *
	 * @return The request list
	 */
	public List<Friend> getRequests(){
		List<Friend> friends = new ArrayList<>();

		for(Integer id : getRawRequests()){
			Friend f = NetworkAPI.Registry.FRIEND.read(id);

			if(f != null)
				friends.add(f);
		}
		return friends;
	}

	/**
	 * Get registry with uuid
	 *
	 * @param uuid The uuid
	 * @return The registry
	 */
	public Friend getFriend(UUID uuid){
		for(Friend friend : getFriends()){
			if(friend.getUniqueId().equals(uuid)){
				return friend;
			}
		}
		return null;
	}

	/**
	 * Add requests to list
	 *
	 * @param player The player
	 */
	public void addRequest(Friend player){
		if(hasRequest(player.getName())){
			return;
		}
		getRawRequests().add(player.getPlayerId());
		updateData();
	}

	/**
	 * Accepts the registry request
	 *
	 * @param name The name
	 */
	public void acceptRequest(String name){
		if(!hasRequest(name)){
			return;
		}
		Friend request = getRequest(name);

		this.removeRequest(name);
		request.addFriend(this);
		this.addFriend(request);

		// Send info
		BungeeLibrary.callEvent(new FriendUpdateEvent(FriendUpdateEvent.Type.ADD_FRIEND, this, request));
	}

	/**
	 * Adds given player to friend list
	 *
	 * @param player The player
	 */
	public void addFriend(Friend player){
		if(!hasFriend(player.getUniqueId())){
			getRawFriends().add(player.getPlayerId());
			this.updateData();
		}
	}

	/**
	 * Accepts all requests
	 */
	public void acceptAllRequests(){
		for(Friend fp : getRequests()){
			this.removeRequest(fp.getName());
			fp.addFriend(this);
			this.addFriend(fp);
		}
	}

	/**
	 * Remove request with given name
	 *
	 * @param name The name
	 */
	public void removeRequest(String name){
		List<Friend> requests = getRequests();

		for(Friend p : requests){
			if(p.getName().equals(name)){
				getRawRequests().remove(Integer.valueOf(p.getPlayerId()));
				this.updateData();
				break;
			}
		}
	}

	/**
	 * Denies a request
	 *
	 * @param name The name
	 */
	public void denyRequest(String name){
		if(!hasRequest(name)){
			return;
		}
		removeRequest(name);
	}

	/**
	 * Denies all requests
	 */
	public void denyAllRequests(){
		for(Friend p : getRequests()){
			removeRequest(p.getName());
		}
	}

	/**
	 * Removes a registry
	 *
	 * @param name The name
	 */
	public void removeFriend(String name){
		if(ProfileManager.getProfile(name) == null
				|| BungeeLibrary.proxy().getPlayer(name) == null
				|| !hasFriend(ProfileManager.getUniqueId(name))){
			return;
		}
		Friend target = FriendManager.getEntry(name);
		getRawFriends().remove(Integer.valueOf(target.getPlayerId()));
		this.updateData();
		target.removeFriend(getName());
	}

	/**
	 * Checks if this has request with given name
	 *
	 * @param name The name
	 * @return The result
	 */
	public boolean hasRequest(String name){
		return getRequest(name) != null;
	}

	/**
	 * Get request from given name
	 *
	 * @param name The name
	 * @return The player
	 */
	public Friend getRequest(String name){
		for(Friend player : getRequests()){
			if(player.getName().equals(name)){
				return player;
			}
		}
		return null;
	}

	/**
	 * Check if this player has given uuid registry
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public boolean hasFriend(UUID uuid){
		return getFriend(uuid) != null;
	}

	/**
	 * Get requests as string
	 *
	 * @return The string
	 */
	public String getRequestsAsString(){
		List<String> requests = new ArrayList<>();

		for(Integer i : getRawRequests()){
			requests.add(i + "");
		}
		return ListUtil.insert(requests, SPLITERATOR);
	}

	/**
	 * Get the registry list as integer list
	 *
	 * @return The integer list
	 */
	private List<Integer> getRawFriends(String fri){
		List<Integer> friends = new ArrayList<>();
		String[] spl = fri.split(SPLITERATOR);

		for(String s : spl){
			if(SimpleStringUtils.isInteger(s)){
				friends.add(Integer.valueOf(s));
			}
		}

		return friends;
	}

	/**
	 * Get the friends
	 *
	 * @return The friends
	 */
	public List<Friend> getFriends(){
		List<Friend> friends = new ArrayList<>();

		for(Integer id : getRawFriends()){
			Friend f = NetworkAPI.Registry.FRIEND.read(id);

			if(f != null)
				friends.add(f);
		}
		return friends;
	}

	/**
	 * Get friends as string
	 *
	 * @return The string
	 */
	public String getFriendsAsString(){
		List<String> friends = new ArrayList<>();

		for(Integer i : getRawFriends()){
			friends.add(i + "");
		}
		return ListUtil.insert(friends, SPLITERATOR);
	}

	/**
	 * Writes the data into the mongo
	 */
	public Friend updateData(){
		NetworkAPI.Registry.FRIEND.update(FriendRegistry.ID_ID, getPlayerId(),
				getRequestsAsString(), getFriendsAsString());
		return this;
	}

	/**
	 * Get the playerName of this friend
	 *
	 * @return The name
	 */
	public String getName(){
		return ProfileManager.getName(getPlayerId());
	}

}
