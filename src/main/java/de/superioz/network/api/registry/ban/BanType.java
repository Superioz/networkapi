package de.superioz.network.api.registry.ban;

import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
@Getter
public enum BanType {

	CHAT(0, "chat"),
	GLOBAL_UUID(1, "global"),
	GLOBAL_IP(1, "global_ip");

	private int id;
	private int globalId;
	private String name;

	/**
	 * Simply the constructor
	 *
	 * @param globalId The global id of the type
	 * @param name     The name of the ban
	 */
	BanType(int globalId, String name){
		this.id = ordinal();
		this.globalId = globalId;
		this.name = name;
	}

	/**
	 * Get the name in nice form
	 *
	 * @return The string
	 */
	public String getNiceName(){
		return SimpleStringUtils.upperFirstLetterSpaced(getName(), "_");
	}

	/**
	 * Get all values from enum as strings
	 *
	 * @return The list
	 */
	public static List<String> getValues(){
		List<String> stringList = new ArrayList<>();

		for(BanType type : values()){
			stringList.add(type.getName());
		}
		return stringList;
	}

	/**
	 * Compares two ban types
	 *
	 * @param type The other type
	 * @return The result
	 */
	public boolean compare(BanType type){
		return type.getGlobalId() == getGlobalId();
	}

	/**
	 * Get the ban type
	 *
	 * @param name The name
	 * @return The reason
	 */
	public static BanType fromName(String name){
		for(BanType type : values()){
			if(type.getName().equalsIgnoreCase(name)){
				return type;
			}
		}
		return null;
	}

	/**
	 * Get the ban reason
	 *
	 * @param id The id
	 * @return The reason
	 */
	public static BanType fromId(int id){
		for(BanType type : values()){
			if(type.getId() == id){
				return type;
			}
		}
		return null;
	}

}
