package de.superioz.network.api.registry.ban;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.superioz.network.api.mongo.MongoRegistry;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
@SuppressWarnings("unchecked")
public class BanRegistry implements MongoRegistry<Ban> {

	public static final String COLLECTION_NAME = "bans";
	public static final String ID_ID = "id";
	public static final String ID_BANNED_IP = "bannedIp";
	public static final String ID_BANNED_ID = "bannedId";
	public static final String ID_FROM_ID = "fromId";
	public static final String ID_PARENT_REASON = "parentReason";
	public static final String ID_REASON = "reason";
	public static final String ID_TIME_FROM = "timeFrom";
	public static final String ID_TIME_UNTIL = "timeUntil";
	public static final String ID_TYPE_ID = "type";

	@Override
	public MongoCollection getCollection(){
		return mongoManager.getCollection(getName());
	}

	@Override
	public String getName(){
		return COLLECTION_NAME;
	}

	@Override
	public boolean contains(String index, Object value){
		return getCollection().find(Filters.eq(index, value)).iterator().hasNext();
	}

	public boolean containsById(int banId){
		return contains(ID_ID, banId);
	}

	public boolean containsByBannedId(int bannedId){
		return contains(ID_BANNED_ID, bannedId);
	}

	public boolean containsByBannedIp(String bannedIp){
		return contains(ID_BANNED_IP, bannedIp);
	}

	public boolean containsByFromId(int fromId){
		return contains(ID_FROM_ID, fromId);
	}

	public boolean containsByParentReason(int typeId){
		return contains(ID_TYPE_ID, typeId);
	}

	@Override
	public List<Ban> read(String index, Object value){
		final List<Ban> bans = new ArrayList<>();

		getCollection().find(Filters.eq(index, value)).forEach(new Block<Document>() {
			@Override
			public void apply(Document document){
				bans.add(getBan(
						document.getInteger(ID_ID),
						document.getInteger(ID_BANNED_ID),
						document.getString(ID_BANNED_IP),
						document.getInteger(ID_FROM_ID),
						document.getInteger(ID_PARENT_REASON),
						document.getString(ID_REASON),
						document.getLong(ID_TIME_FROM),
						document.getLong(ID_TIME_UNTIL),
						document.getInteger(ID_TYPE_ID)));
			}
		});
		return bans;
	}

	@Override
	public void delete(String index, Object value){
		getCollection().deleteOne(Filters.eq(index, value));
	}

	public boolean delete(String index, Object value, BanType type){
		Bson indexBson = Filters.eq(index, value);
		Bson timeBson = Filters.or(Filters.gt(ID_TIME_UNTIL, System.currentTimeMillis()), Filters.eq(ID_TIME_UNTIL, -1));

		if(type != null){
			return getCollection().deleteOne(Filters.and(indexBson,
					Filters.eq(ID_TYPE_ID, type.getId()), timeBson)).getDeletedCount() > 0;
		}
		return getCollection().deleteOne(Filters.and(indexBson, timeBson)).getDeletedCount() > 0;
	}

	public Ban readById(int banId){
		return read(ID_ID, banId).get(0);
	}

	public List<Ban> readByBannedId(int bannedId){
		return read(ID_BANNED_ID, bannedId);
	}

	public List<Ban> readByBannedIp(String bannedIp){
		return read(ID_BANNED_IP, bannedIp);
	}

	public List<Ban> readByFromId(int fromId){
		return read(ID_FROM_ID, fromId);
	}

	public List<Ban> readByParentReason(int typeId){
		return read(ID_TYPE_ID, typeId);
	}

	/**
	 * Writes into the database
	 *
	 * @param banId          The ban id
	 * @param bannedId       The bannedid
	 * @param bannedIp       The banned ip
	 * @param fromId         The from id
	 * @param parentReasonId The parentreason id
	 * @param childReason    The child reason as string
	 * @param timeFrom       The time from
	 * @param timeUntil      The time until
	 * @param typeId         The type id
	 * @return The ban
	 */
	public Ban write(final int banId, final int bannedId, final String bannedIp,
	                 final int fromId, final int parentReasonId, final String childReason,
	                 final long timeFrom, final long timeUntil, final int typeId){
		if(contains(ID_ID, banId)){
			return read(ID_ID, banId).get(0);
		}
		Ban ban = getBan(banId, bannedId, bannedIp, fromId, parentReasonId, childReason, timeFrom, timeUntil, typeId);

		// Write
		getCollection().insertOne(new Document()
				.append(ID_ID, banId)
				.append(ID_BANNED_ID, bannedId)
				.append(ID_BANNED_IP, bannedIp)
				.append(ID_FROM_ID, fromId)
				.append(ID_PARENT_REASON, parentReasonId)
				.append(ID_REASON, childReason)
				.append(ID_TIME_FROM, timeFrom)
				.append(ID_TIME_UNTIL, timeUntil)
				.append(ID_TYPE_ID, typeId));
		return ban;
	}

	public Ban write(Ban ban){
		return this.write(ban.getId(), ban.getBannedId(), ban.getBannedIp(),
				ban.getFromId(), ban.getParentReasonId(), ban.getChildReason(),
				ban.getTimeFrom(), ban.getTimeUntil(), ban.getTypeId());
	}

	/**
	 * Updates a ban entry
	 *
	 * @param index          The index
	 * @param value          The value
	 * @param parentReasonId The parent reason (Set to '-1' for no change)
	 * @param childReason    The child reason (Leave blank for no change)
	 * @param timeUntil      The time until (Set to '-1' for no change)
	 * @param typeId         The type id (Set to '-1' for no change)
	 */
	public void update(final String index, final String value,
	                   final int parentReasonId, final String childReason,
	                   final long timeUntil, final int typeId){
		Document updateDoc = new Document();
		if(!(parentReasonId < 0)){
			updateDoc.append(ID_PARENT_REASON, parentReasonId);
		}
		if(!childReason.isEmpty()){
			updateDoc.append(ID_REASON, childReason);
		}
		if(!(timeUntil < 0)){
			updateDoc.append(ID_TIME_UNTIL, timeUntil);
		}
		if(!(typeId < 0)){
			updateDoc.append(ID_TYPE_ID, typeId);
		}

		// Update
		getCollection().updateOne(Filters.eq(index, value),
				new Document("$set", updateDoc));
	}

	/**
	 * Get ban by given values
	 *
	 * @param banId          The ban id
	 * @param bannedId       The bannedid
	 * @param bannedIp       The banned ip
	 * @param fromId         The from id
	 * @param parentReasonId The parentreason id
	 * @param childReason    The child reason as string
	 * @param timeFrom       The time from
	 * @param timeUntil      The time until
	 * @param typeId         The type id
	 * @return The ban
	 */
	public Ban getBan(final int banId, final int bannedId, final String bannedIp,
	                   final int fromId, final int parentReasonId, final String childReason,
	                   final long timeFrom, final long timeUntil, final int typeId){
		return new Ban() {
			@Override
			public int getId(){
				return banId;
			}

			@Override
			public int getBannedId(){
				return bannedId;
			}

			@Override
			public String getBannedIp(){
				return bannedIp;
			}

			@Override
			public int getFromId(){
				return fromId;
			}

			@Override
			public int getParentReasonId(){
				return parentReasonId;
			}

			@Override
			public String getChildReason(){
				return childReason;
			}

			@Override
			public long getTimeFrom(){
				return timeFrom;
			}

			@Override
			public long getTimeUntil(){
				return timeUntil;
			}

			@Override
			public int getTypeId(){
				return typeId;
			}
		};
	}

}
