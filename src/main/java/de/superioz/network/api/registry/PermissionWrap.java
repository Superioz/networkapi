package de.superioz.network.api.registry;

import lombok.Getter;

/**
 * Created on 30.04.2016.
 */
@Getter
public class PermissionWrap {

	private String permission;
	private PermissionType type;
	private boolean negatePermission;

	public PermissionWrap(String permission, PermissionType type){
		this.negatePermission = permission.startsWith("-");
		this.permission = negatePermission ? permission.replace("-", "") : permission;
		this.type = type;
	}

	/**
	 * Gets signed permission from all values inside this class
	 * <p>
	 * Signed permission would look like this: "typeId:the.permission"
	 *
	 * @return The permission as string
	 */
	public String toSignedPermission(){
		String perm = (isNegatePermission() ? "-" : "") + permission;
		return type.ordinal() + ":" + perm;
	}

	/**
	 * Get the permission wrap from given signed permission
	 * <p>
	 * A signed permission is a permission like that: "1:test.permission"
	 *
	 * @param s The string
	 * @return The wrapper
	 */
	public static PermissionWrap fromSignedPermission(String s){
		String[] spl = s.split(":", 2);
		PermissionType type = PermissionType.values()[Integer.valueOf(spl[0])];

		return new PermissionWrap(spl[1], type);
	}

}
