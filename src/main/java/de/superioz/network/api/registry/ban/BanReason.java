package de.superioz.network.api.registry.ban;

import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
public class BanReason {

	@Getter
	public enum Parent {

		CHAT_BEHAVIOUR(1, "chat_verhalten"),
		INGAME_BEHAVIOUR(1, "ingame_verhalten"),
		SKIN(2, "skin"),
		NAME(2, "name"),
		CLIENTMODS(3, "clientmods");

		private int id;
		private int banPoints;
		private String name;

		/**
		 * Just the parent ban reason
		 *
		 * @param banPoints How much ban points get the player when banned with this reason?
		 * @param name      The name of the bantype
		 */
		Parent(int banPoints, String name){
			this.banPoints = banPoints;
			this.id = ordinal();
			this.name = name;
		}

		/**
		 * Get the name in nice form
		 *
		 * @return The string
		 */
		public String getNiceName(){
			return SimpleStringUtils.upperFirstLetterSpaced(getName(), "_");
		}

	}

	@Getter
	public static class Child {

		private String reason;

		/**
		 * Just a reason child
		 *
		 * @param reason The reason as string
		 */
		public Child(String reason){
			this.reason = reason;
		}

	}

	@Getter
	public enum Repeal {

		REQUEST,
		MISTAKE;

		private int id;

		/**
		 * If someone unbans somebody
		 */
		Repeal(){
			this.id = ordinal();
		}

		/**
		 * Get the ban reason
		 *
		 * @param name The name
		 * @return The reason
		 */
		public static Repeal fromName(String name){
			for(Repeal reason : values()){
				if(reason.name().equalsIgnoreCase(name)){
					return reason;
				}
			}
			return null;
		}

		/**
		 * Get all values from enum as strings
		 *
		 * @return The list
		 */
		public static List<String> getValues(){
			List<String> stringList = new ArrayList<>();

			for(Repeal reason : values()){
				stringList.add(reason.name());
			}
			return stringList;
		}

	}

}
