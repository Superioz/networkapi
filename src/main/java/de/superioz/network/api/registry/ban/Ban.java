package de.superioz.network.api.registry.ban;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bungee.util.LanguageManager;
import de.superioz.sx.bungee.chat.WrappedMessage;
import de.superioz.sx.java.util.IdentifierUtil;
import de.superioz.sx.java.util.ListUtil;
import de.superioz.sx.java.util.TimeUtils;
import net.md_5.bungee.api.chat.BaseComponent;

import java.util.List;

/**
 * Created on 23.04.2016.
 */
public abstract class Ban {

	/**
	 * Get the id of the ban
	 *
	 * @return The id
	 */
	public abstract int getId();

	public String getHexId(){
		return IdentifierUtil.toString(getId(), 4);
	}

	/**
	 * Get the player profile id of the banned player
	 *
	 * @return The id
	 */
	public abstract int getBannedId();

	/**
	 * Get the ip
	 *
	 * @return The ip
	 */
	public abstract String getBannedIp();

	/**
	 * Get the player profile id of the person who banned the player
	 *
	 * @return The id
	 */
	public abstract int getFromId();

	/**
	 * Get the parent reason id
	 *
	 * @return The parent reason
	 */
	public abstract int getParentReasonId();

	/**
	 * The real reason as string
	 *
	 * @return The string
	 */
	public abstract String getChildReason();

	/**
	 * Get the time when the player got banned
	 *
	 * @return The time as long
	 */
	public abstract long getTimeFrom();

	/**
	 * Get the time when the ban will end
	 *
	 * @return The time as long
	 */
	public abstract long getTimeUntil();

	/**
	 * The type of ban id
	 *
	 * @return The integer
	 */
	public abstract int getTypeId();

	/**
	 * Get the ban type
	 *
	 * @return The type
	 */
	public BanType getType(){
		return BanType.values()[getTypeId()];
	}

	/**
	 * Get ban reason parent
	 *
	 * @return The ban reason
	 */
	public BanReason.Parent getBanReason(){
		return BanReason.Parent.values()[getParentReasonId()];
	}

	/**
	 * Check if the ban is permanent
	 *
	 * @return The result
	 */
	public boolean isPermanent(){
		return getTimeUntil() < 0;
	}

	/**
	 * Get the time difference
	 *
	 * @return The long
	 */
	public long getTimeDifference(){
		if(getTimeUntil() == -1){
			return 0;
		}
		return getTimeUntil() - getTimeFrom();
	}

	/**
	 * Gets the ban message
	 */
	public BaseComponent getMessage(){
		return new WrappedMessage(getMessageAsString()).duplicate();
	}

	/**
	 * Get the ban message as string
	 *
	 * @return The string
	 */
	public String getMessageAsString(){
		List<String> lines = isPermanent()
				? LanguageManager.getPermaBanMessageFile().readln()
				: LanguageManager.getBanMessageFile().readln();

		// Replace variables
		lines = ListUtil.replace(lines, "%reason", getBanReason().getNiceName());
		lines = ListUtil.replace(lines, "%type", getType().getNiceName());
		if(!isPermanent()){
			String day = getUntilDate();
			String time = getUntilTime();

			lines = ListUtil.replace(lines, "%day", day);
			lines = ListUtil.replace(lines, "%time", time);
		}

		// Get component
		return ListUtil.insert(lines, "\n");
	}

	/**
	 * Get the banned person
	 *
	 * @return The profile
	 */
	public PlayerProfile getBanned(){
		return ProfileManager.getProfile(getBannedId());
	}

	/**
	 * Get the banned person
	 *
	 * @return The profile
	 */
	public PlayerProfile getBanSender(){
		if(getFromId() == 0){
			return ProfileManager.CONSOLE_PROFILE;
		}
		return ProfileManager.getProfile(getFromId());
	}

	/**
	 * Checks if the ban is expired
	 *
	 * @return The boolean
	 */
	public boolean isExpired(){
		long current = System.currentTimeMillis();
		long banDuration = getTimeUntil();

		// Check if it's a permaban
		return banDuration >= 0 && current >= banDuration;
	}

	/**
	 * Get when the ban expires
	 *
	 * @return The ban
	 */
	public String getUntilDate(){
		return TimeUtils.fromTimestamp(getTimeUntil(), "dd.MM.yyyy");
	}

	/**
	 * Get when the ban expires
	 *
	 * @return The ban
	 */
	public String getUntilTime(){
		return TimeUtils.fromTimestamp(getTimeUntil(), "HH:mm:ss");
	}

}
