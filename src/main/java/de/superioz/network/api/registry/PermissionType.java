package de.superioz.network.api.registry;

import de.superioz.sx.java.util.SimpleStringUtils;

/**
 * Created on 30.04.2016.
 */
public enum PermissionType {

	BUKKIT,
	BUNGEECORD;

	public String getNiceName(){
		return SimpleStringUtils.upperFirstLetter(name().toLowerCase());
	}

}
