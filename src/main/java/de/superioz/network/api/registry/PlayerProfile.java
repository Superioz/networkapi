package de.superioz.network.api.registry;

import de.superioz.network.api.BanManager;
import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NetworkAPI;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.ban.Ban;
import de.superioz.network.api.registry.ban.BanType;
import de.superioz.sx.java.util.ListUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created on 20.04.2016.
 */
public abstract class PlayerProfile {

	/**
	 * Get the player unique id
	 *
	 * @return The unique id
	 */
	public abstract UUID getUniqueId();

	/**
	 * Get the player profile id
	 *
	 * @return The id
	 */
	public abstract int getId();

	/**
	 * Get the player's last name
	 *
	 * @return The name
	 */
	public abstract String getName();

	/**
	 * Get the current nickname of the player (empty for no nickname)
	 *
	 * @return The name
	 */
	public abstract String getNickName();

	/**
	 * Sets the nickname for this player
	 *
	 * @param name The name
	 * @return The profile
	 */
	public PlayerProfile setNickName(String name){
		if(name == null || name.length() > 16){
			return this;
		}
		NetworkAPI.Registry.PLAYER_PROFILE.update(
				PlayerProfileRegistry.ID_UUID, getUniqueId().toString(),
				"", name, "", "");
		return ProfileManager.getProfile(getUniqueId());
	}

	/**
	 * Get the player's last ip
	 *
	 * @return The ip
	 */
	public abstract String getIp();

	/**
	 * Get the players group
	 *
	 * @return The name of the group
	 */
	public abstract String getGroup();

	/**
	 * Get the real group of this player
	 *
	 * @return The group
	 */
	public PermissionGroup getRealGroup(){
		return GroupManager.getGroup(getGroup());
	}

	/**
	 * Get all bans
	 *
	 * @return The bans
	 */
	public List<Ban> getBans(){
		return BanManager.getBans(getId());
	}

	/**
	 * Get most ban type
	 *
	 * @return The type
	 */
	public BanType getMostBanType(){
		List<BanType> types = new ArrayList<>();
		for(Ban b : getBans()){
			types.add(b.getType());
		}
		return ListUtil.getMostlyRepeated(types);
	}

	/**
	 * Is the player chat banned
	 *
	 * @return The result
	 */
	public boolean isChatBanned(){
		return BanManager.isChatBanned(getId());
	}

	/**
	 * Is the player global banned
	 *
	 * @return The result
	 */
	public boolean isGlobalBanned(){
		return BanManager.isBanned(this)
				|| BanManager.isBanned(getIp());
	}

	/**
	 * Sets the group for this player profile
	 *
	 * @param group The group
	 * @return The profile
	 */
	public PlayerProfile setGroup(PermissionGroup group){
		NetworkAPI.Registry.PLAYER_PROFILE.update(PlayerProfileRegistry.ID_UUID,
				getUniqueId().toString(), getName(), null,
				getIp(), group.getName());
		return ProfileManager.getProfile(getUniqueId());
	}

}
