package de.superioz.network.api.registry;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.superioz.network.api.mongo.MongoRegistry;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created on 20.04.2016.
 */
@SuppressWarnings("unchecked")
public class PlayerProfileRegistry implements MongoRegistry<PlayerProfile> {

	public static final String COLLECTION_NAME = "players";
	public static final String ID_UUID = "uuid";
	public static final String ID_ID = "id";
	public static final String ID_NAME = "name";
	public static final String ID_NICK = "nick";
	public static final String ID_IP = "ip";
	public static final String ID_GROUP = "group";

	@Override
	public MongoCollection getCollection(){
		return mongoManager.getCollection(getName());
	}

	@Override
	public String getName(){
		return COLLECTION_NAME;
	}

	@Override
	public boolean contains(String index, Object value){
		return getCollection().find(Filters.eq(index, value)).iterator().hasNext();
	}

	public boolean containsByUniqueId(UUID uuid){
		return contains(ID_UUID, uuid.toString());
	}

	public boolean containsById(int id){
		return contains(ID_ID, id);
	}

	public boolean containsByName(String name){
		return contains(ID_NAME, name);
	}

	public boolean containsByIp(String ip){
		return contains(ID_IP, ip);
	}

	@Override
	public List<PlayerProfile> read(String index, Object value){
		final PlayerProfile[] profile = new PlayerProfile[1];

		getCollection().find(Filters.eq(index, value)).limit(1).forEach(new Block<Document>() {
			@Override
			public void apply(Document document){
				final UUID uuid = UUID.fromString(document.getString(ID_UUID));
				final int id = document.getInteger(ID_ID);
				final String name = document.getString(ID_NAME);
				final String nick = document.getString(ID_NICK);
				final String ip = document.getString(ID_IP);
				final String group = document.getString(ID_GROUP);

				profile[0] = getProfile(uuid, id, name, nick, ip, group);
			}
		});
		return Arrays.asList(profile);
	}

	public PlayerProfile readByUniqueId(UUID uuid){
		return read(ID_UUID, uuid.toString()).get(0);
	}

	public PlayerProfile readById(int id){
		return read(ID_ID, id).get(0);
	}

	public PlayerProfile readByName(String name){
		return read(ID_NAME, name).get(0);
	}

	public PlayerProfile readByIp(String ip){
		return read(ID_IP, ip).get(0);
	}

	@Override
	public void delete(String index, Object value){
		getCollection().deleteOne(Filters.eq(index, value));
	}

	/**
	 * Count all values in the collection
	 *
	 * @return The count
	 */
	public long count(){
		return getCollection().count();
	}

	/**
	 * Write into the database
	 *
	 * @param uuid The uuid
	 * @param id   The id
	 * @param name The name
	 * @param ip   The ip
	 */
	public void write(final UUID uuid, final int id,
	                  final String name, final String nick,
	                  final String ip, final String group){
		PlayerProfile profile = getProfile(uuid, id, name, nick, ip, group);

		// Write
		getCollection().insertOne(new Document()
				.append(ID_UUID, "" + profile.getUniqueId())
				.append(ID_ID, profile.getId())
				.append(ID_NAME, profile.getName())
				.append(ID_NICK, profile.getNickName())
				.append(ID_IP, "" + profile.getIp())
				.append(ID_GROUP, profile.getGroup()));
	}

	/**
	 * Updates entries
	 *
	 * @param index The index
	 * @param name  The name
	 * @param ip    The ip
	 */
	public void update(final String index, final Object value,
	                   final String name, final String nick,
	                   final String ip,
	                   final String group){
		Document updateDoc = new Document();
		if(!name.isEmpty()){
			updateDoc.append(ID_NAME, name);
		}
		if(nick != null){
			updateDoc.append(ID_NICK, nick);
		}
		if(!ip.isEmpty()){
			updateDoc.append(ID_IP, ip);
		}
		if(!group.isEmpty()){
			updateDoc.append(ID_GROUP, group);
		}

		// Update
		getCollection().updateOne(Filters.eq(index, value),
				new Document("$set", updateDoc));
	}

	/**
	 * Get the profile by given values
	 *
	 * @param uuid The uuid
	 * @param id   The id
	 * @param name The name
	 * @param ip   The ip
	 * @return The result
	 */
	private PlayerProfile getProfile(final UUID uuid, final int id,
	                                 final String name, final String nick,
	                                 final String ip,
	                                 final String group){
		return new PlayerProfile() {
			@Override
			public UUID getUniqueId(){
				return uuid;
			}

			@Override
			public int getId(){
				return id;
			}

			@Override
			public String getName(){
				return name;
			}

			@Override
			public String getIp(){
				return ip;
			}

			@Override
			public String getGroup(){
				return group;
			}

			@Override
			public String getNickName(){
				return nick;
			}
		};
	}

	// == Intern methods

	/*
	Get unique id by values
	 */

	public UUID getUniqueIdById(int id) throws NullPointerException{
		return readById(id).getUniqueId();
	}

	public UUID getUniqueIdByName(String name) throws NullPointerException{
		return readByName(name).getUniqueId();
	}

	public UUID getUniqueIdByIp(String ip) throws NullPointerException{
		return readByIp(ip).getUniqueId();
	}

	/*
	Get id by values
	 */

	public int getIdByUniqueId(UUID uuid) throws NullPointerException{
		return readByUniqueId(uuid).getId();
	}

	public int getIdByName(String name) throws NullPointerException{
		return readByName(name).getId();
	}

	public int getIdByIp(String ip) throws NullPointerException{
		return readByIp(ip).getId();
	}

	/*
	Get name by values
	 */

	public String getNameById(int id) throws NullPointerException{
		return readById(id).getName();
	}

	public String getNameByUniqueId(UUID uuid) throws NullPointerException{
		return readByUniqueId(uuid).getName();
	}

	public String getNameByIp(String ip) throws NullPointerException{
		return readByIp(ip).getName();
	}

	/*
	Get ip by values
	 */

	public String getIpByUniqueId(UUID uuid) throws NullPointerException{
		return readByUniqueId(uuid).getIp();
	}

	public String getIpByName(String name) throws NullPointerException{
		return readByName(name).getIp();
	}

	public String getIpById(int id) throws NullPointerException{
		return readById(id).getIp();
	}

}
