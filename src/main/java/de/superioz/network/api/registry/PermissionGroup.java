package de.superioz.network.api.registry;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NetworkAPI;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23.04.2016.
 */
@Getter
@Setter
public class PermissionGroup {

	private String name;
	private boolean isDefault;
	private boolean isArtificially;
	private int rank;
	private String chatPrefix;
	private String chatSuffix;
	private String tabPrefix;
	private String tabSuffix;
	private String color;
	public List<String> permissions;
	private List<String> inheritances;

	/**
	 * Constructor for the permission group
	 *
	 * @param name           The name
	 * @param isDefault      The is default
	 * @param isArtificially The is artifically
	 * @param rank           The rank
	 * @param chatPrefix     The chat prefix
	 * @param chatSuffix     The chat suffix
	 * @param tabPrefix      The tab prefix
	 * @param tabSuffix      The tab suffix
	 * @param color          The color
	 * @param inheritances   The inheritances
	 * @param permissions    The permissions
	 */
	PermissionGroup(String name, boolean isDefault, boolean isArtificially,
	                int rank, String chatPrefix, String chatSuffix, String tabPrefix, String tabSuffix,
	                String color, List<String> inheritances, List<String> permissions){
		this.name = name;
		this.isDefault = isDefault;
		this.isArtificially = isArtificially;
		this.rank = rank;
		this.chatPrefix = chatPrefix;
		this.chatSuffix = chatSuffix;
		this.tabPrefix = tabPrefix;
		this.tabSuffix = tabSuffix;
		this.color = color;
		this.inheritances = inheritances;
		this.permissions = permissions;
	}

	/**
	 * Add a perm (shouldn't be longer than 32)
	 *
	 * @param perm The permission
	 */
	public void addPermission(String perm, PermissionType type){
		if(perm.isEmpty() || perm.length() > 32){
			return;
		}
		PermissionWrap wrap = new PermissionWrap(perm, type);
		String p = wrap.toSignedPermission();

		if(!permissions.contains(p)){
			permissions.add(p);
		}
	}

	/**
	 * Removes a permission
	 *
	 * @param perm The permission
	 */
	public void removePermission(String perm, PermissionType type){
		PermissionWrap wrap = new PermissionWrap(perm, type);
		String p = wrap.toSignedPermission();

		if(permissions.contains(p)){
			permissions.remove(p);
		}
	}

	/**
	 * Add a perm
	 *
	 * @param group The group
	 */
	public void addInheritance(String group){
		if(!inheritances.contains(group)
				&& GroupManager.hasGroup(group)){
			permissions.add(group);
		}
	}

	/**
	 * Removes a permission
	 *
	 * @param group The group
	 */
	public void removeInheritance(String group){
		if(inheritances.contains(group)){
			inheritances.remove(group);
		}
	}

	/**
	 * Set the name
	 *
	 * @param name The name
	 */
	public void setName(String name){
		if(name.isEmpty() || name.length() > 20){
			return;
		}
		this.name = name;
		this.updateData(PermissionRegistry.ID_RANK, getRank());
	}

	/**
	 * Set the rank for this group
	 *
	 * @param rank The rank
	 */
	public void setRank(int rank){
		this.rank = rank;
		this.updateData(PermissionRegistry.ID_NAME, getName());
	}

	/**
	 * Get all permissions as permission wraps
	 * A permission wrap is a wrapper class with the
	 * permission type (Bukkit, Bungee) and the permission itself.
	 *
	 * @return The list of permissions
	 */
	public List<PermissionWrap> getPermissions(){
		List<PermissionWrap> wrappers = new ArrayList<>();

		for(String s : this.permissions){
			wrappers.add(PermissionWrap.fromSignedPermission(s));
		}
		return wrappers;
	}

	/**
	 * Get permissions with given permission type
	 * Example: If you choose the permission type bukkit
	 * than every permission with the "bukkitPermId:" prefix
	 * will be added to the string (not as signed perm)
	 *
	 * @param type The type
	 * @return The list of permissions (without type prefix)
	 */
	public List<PermissionWrap> getPermissions(PermissionType type){
		List<PermissionWrap> l = new ArrayList<>();

		for(PermissionWrap w : getPermissions()){
			if(w.getType() == type){
				l.add(w);
			}
		}
		return l;
	}

	/**
	 * Checks if the permission is set ... nothing else
	 * If you want to check, if the player HAS the permission
	 * then do hasPermission(String s)
	 *
	 * @param permission The permission as string
	 * @param type       Type of the permission (e.g. bukkit)
	 * @return The result
	 */
	public boolean isPermissionSet(String permission, PermissionType type){
		for(PermissionWrap wrap : getPermissions(type)){
			if(wrap.getPermission().equalsIgnoreCase(permission)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the player has the given permission
	 *
	 * @param type       The type of permission (bukkit, bungee)
	 * @param permission The permission
	 * @return The result
	 */
	public boolean hasPermission(String permission, PermissionType type){
		if(!isPermissionSet(permission, type)) return false;
		for(PermissionWrap w : getPermissions(type)){
			if(w.getPermission().equalsIgnoreCase(permission)){
				return !w.isNegatePermission();
			}
		}
		return false;
	}

	/**
	 * Writes the data to the database
	 */
	private void updateData(String index, Object value){
		NetworkAPI.Registry.PERMISSION.update(index, value,
				getName(), isDefault(), isArtificially(), getRank(), getChatPrefix(), getChatSuffix(),
				getTabPrefix(), getTabSuffix(), getColor(), this.permissions, getInheritances());
	}

	public void updateData(){
		this.updateData(PermissionRegistry.ID_NAME, getName());
	}

	// == Intern methods

}
