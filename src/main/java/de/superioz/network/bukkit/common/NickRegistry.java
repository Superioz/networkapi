package de.superioz.network.bukkit.common;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.superioz.network.bungee.util.Debugger;
import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.ViewManager;
import de.superioz.sx.bukkit.common.protocol.*;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created on 26.04.2016.
 */
public class NickRegistry {

	private static Map<UUID, Collection<Property>> oldSkins = new HashMap<>();
	private static Map<UUID, String> oldNames = new HashMap<>();

	/**
	 * Get old profile from given uuid
	 *
	 * @param uuid The uuid
	 * @return The profile
	 */
	public static Collection<Property> getOldSkin(UUID uuid){
		Collection<Property> skin = oldSkins.get(uuid);

		if(skin == null) return null;
		return skin;
	}

	/**
	 * Get old name from given uuid
	 *
	 * @param uuid The uuid
	 * @return The name
	 */
	public static String getOldName(UUID uuid){
		return oldNames.get(uuid);
	}

	/**
	 * Puts into both maps
	 *
	 * @param player The player
	 */
	public static void put(Player player, boolean force){
		UUID uuid = player.getUniqueId();
		if(contains(uuid) && !force){
			return;
		}

		// GET PROFILE
		WrappedGameProfile profile = WrappedGameProfile.from(player);
		String name = player.getName();
		oldSkins.put(uuid, new ArrayList<>(profile.getProperties().get("textures")));
		oldNames.put(uuid, name);
	}

	public static void put(Player player){
		put(player, false);
	}

	/**
	 * Removes given player from maps
	 *
	 * @param player The player
	 */
	public static void remove(Player player){
		UUID uuid = player.getUniqueId();
		if(!contains(uuid)){
			return;
		}

		oldSkins.remove(uuid);
		oldNames.remove(uuid);
	}

	/**
	 * Checks if given uuid is present
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public static boolean contains(UUID uuid){
		return oldSkins.containsKey(uuid) && oldNames.containsKey(uuid);
	}

	// ==

	/**
	 * NickNames the player
	 *
	 * @param player  The player
	 * @param profile New profile
	 * @param type    The type
	 * @param viewers The viewers
	 */
	private static void nickPlayer(Player player, WrappedGameProfile profile,
	                               NickPacket.Type type, boolean r, Player... viewers){
		NickPacket packet = new NickPacket(player, profile, type);

		// Backup skin
		put(player, !r);

		// Send the packet
		packet.send(r, viewers);
	}

	public static void nickPlayer(Player player, WrappedGameProfile profile, boolean r, Player... viewers){
		nickPlayer(player, profile, NickPacket.Type.ADD, r, viewers);
	}

	public static void unickPlayer(Player player, boolean r, Player... viewers){
		UUID uuid = player.getUniqueId();
		Collection<Property> oldSkin = getOldSkin(uuid);

		if(oldSkin != null){
			GameProfile profile = new GameProfile(uuid, getOldName(uuid));
			profile.getProperties().removeAll("textures");
			profile.getProperties().putAll("textures", oldSkin);

			nickPlayer(player, new WrappedGameProfile(profile), NickPacket.Type.RESET, r, viewers);
			remove(player);
		}
	}


	/**
	 * Created on 26.04.2016.
	 */
	@Getter
	public static class NickPacket {

		private Player player;
		private WrappedGameProfile currentProfile;
		private Type type;

		private WrappedGameProfile profile;

		private WrappedPacket removeTab;
		private WrappedPacket addTab;

		public NickPacket(Player player, WrappedGameProfile profile, Type type){
			this.player = player;
			this.profile = profile;
			this.currentProfile = WrappedGameProfile.from(player);
			this.type = type;
		}

		/**
		 * Send nick packet to given players
		 *
		 * @param players The players
		 */
		public void send(final boolean refresh, final Player... players){
			// Remove from tablist
			if(refresh){
				constructRemovePacket();
				getRemoveTab().send(players);
			}

			// Set properties
			currentProfile.getProperties().removeAll("textures");
			currentProfile.getProperties().putAll("textures", profile.getProperties().get("textures"));
			currentProfile.setName(profile.getName());

			// Get new game profile
			currentProfile.deepInsert(player);

			// Update view
			if(refresh){
				new BukkitRunnable() {
					@Override
					public void run(){
						ViewManager.updateSelf(player);
						ViewManager.refreshPlayer(player, players);
					}
				}.runTask(BukkitLibrary.plugin());
			}

			// Add to tablist
			if(refresh){
				constructAddPacket();
				getAddTab().send(players);
			}
		}

		/**
		 * Construct the remove packet
		 */
		private void constructRemovePacket(){
			// Remove tablist packet
			removeTab = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);
			removeTab.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
			removeTab.getPlayerInfoDataLists().write(0, Collections.singletonList(new PlayerInfoData(
					EnumWrappers.NativeGameMode.NOT_SET, getCurrentProfile(), 0,
					new WrappedChatComponent(getCurrentProfile().getName()))));
		}

		/**
		 * Construct the add packet
		 */
		private void constructAddPacket(){
			// Add tablist packet
			addTab = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);
			addTab.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
			addTab.getPlayerInfoDataLists().write(0, Collections.singletonList(new PlayerInfoData(
					EnumWrappers.NativeGameMode.NOT_SET, getCurrentProfile(), 0,
					new WrappedChatComponent(getCurrentProfile().getName()))));
		}

		/**
		 * Type of nicking (reset / add nick)
		 */
		public enum Type {

			RESET,
			ADD

		}

	}
}
