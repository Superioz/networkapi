package de.superioz.network.bukkit.common;

import de.superioz.network.api.SerializableParty;
import de.superioz.network.api.netty.NettyHandler;
import de.superioz.network.api.netty.NioPacket;
import de.superioz.network.api.netty.NioPacketType;
import de.superioz.network.bukkit.NetworkBukkit;
import de.superioz.sx.java.util.Consumer;

import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created on 05.05.2016.
 */
public class PartyAPI {

	/**
	 * Get the party from given player
	 *
	 * @param player The player
	 * @return The party
	 */
	public static SerializableParty getParty(Player player){
		final UUID uuid = player.getUniqueId();

		final SerializableParty[] party = new SerializableParty[1];
		NetworkBukkit.s.write(new NioPacket(NioPacketType.REQUEST,
				NettyHandler.GET_PARTY_COMMAND, uuid.toString(), NettyHandler.PASSWORD.toCharArray()), new Consumer<NioPacket>() {
			@Override
			public void accept(NioPacket packet){
				if(packet.getContent().equals("0")){
					party[0] = null;
				}
				else{
					party[0] = SerializableParty.fromJson(packet.getContent());
				}
			}
		});
		NettyHandler.waitFor();
		return party[0];
	}

}
