package de.superioz.network.bukkit.common;

import de.superioz.network.api.NickManager;
import de.superioz.network.bukkit.event.NickUpdateEvent;
import de.superioz.sx.bukkit.BukkitLibrary;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created on 02.05.2016.
 */
public class NickNames {

	/**
	 * Call this method when a player wants him to change his nick name
	 *
	 * @param player   The player
	 * @param nickName The nickname
	 */
	public static void setNick(Player player, String nickName, boolean onJoin){
		UUID uuid = player.getUniqueId();
		NickManager.setNick(uuid, nickName);
		BukkitLibrary.callEvent(new NickUpdateEvent(player, onJoin));
	}

	/**
	 * Checks if the name is valid
	 *
	 * @param name The name
	 * @return The result
	 */
	public static boolean checkName(String name){
		String regex = "^[a-zA-Z0-9_]{3,15}$";
		return name.matches(regex);
	}

	/**
	 * Removes the nick of the given player
	 *
	 * @param player The player
	 */
	public static void removeNick(Player player){
		setNick(player, "", false);
	}

	/**
	 * Get the real name of the given player
	 *
	 * @param player The player
	 * @return The name
	 */
	public static String getRealName(Player player){
		String name = NickManager.getRealName(player.getUniqueId());
		if(name == null) name = player.getName();
		return name;
	}

}
