package de.superioz.network.bukkit.common;

import de.superioz.network.api.PermissionManager;
import de.superioz.network.api.registry.PermissionType;
import de.superioz.network.api.registry.PermissionWrap;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.sx.bukkit.common.protocol.ProtocolUtil;
import de.superioz.sx.java.util.ReflectionUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.*;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created on 01.05.2016.
 */
public class PermissionInjector {

	/**
	 * Inject new permissible base into given commandSender
	 *
	 * @param sender      The sender
	 * @param permissible The permissible base
	 * @return The result
	 */
	public static boolean inject(CommandSender sender, Permissible permissible){
		Field f = getPermissibleField(sender);

		try{
			f.set(sender, permissible);
			return true;
		}
		catch(IllegalAccessException e){
			return false;
		}
	}

	/**
	 * Get permissible object from given sender
	 *
	 * @param sender The sender
	 * @return The permissible object
	 */
	public static Permissible getPermissible(CommandSender sender){
		Field f = getPermissibleField(sender);

		if(f == null) return null;
		try{
			return (Permissible) f.get(sender);
		}
		catch(IllegalAccessException e){
			return null;
		}
	}

	/**
	 * Get the permissible field
	 *
	 * @param sender The command sender (either player or console)
	 * @return The field
	 */
	public static Field getPermissibleField(CommandSender sender){
		Field f = null;

		if(sender instanceof Player){
			f = ReflectionUtils.getField(ProtocolUtil.getOBCClassExact("entity.CraftHumanEntity"), "perm");
		}
		else if(sender instanceof ConsoleCommandSender){
			f = ReflectionUtils.getField(ProtocolUtil.getOBCClassExact("command.ServerCommandSender"), "perm");
		}
		return f;
	}

	/**
	 * Custom permissibleBase class
	 */
	@Getter
	public static class CustomPermissible extends PermissibleBase {

		private PlayerProfile profile;
		private Permissible oldPermissible;
		private CommandSender sender;

		public CustomPermissible(CommandSender sender,
		                         PlayerProfile profile, Permissible oldPermissible){
			super(sender);
			this.sender = sender;
			this.profile = profile;
			this.oldPermissible = oldPermissible;
		}

		@Override
		public boolean isOp(){
			return super.isOp();
		}

		@Override
		public void setOp(boolean value){
			super.setOp(value);
		}

		@Override
		public boolean isPermissionSet(String name){
			return PermissionManager.isPermissionSet(profile, name, PermissionType.BUKKIT);
		}

		@Override
		public boolean isPermissionSet(Permission perm){
			return this.isPermissionSet(perm.getName());
		}

		@Override
		public boolean hasPermission(String inName){
			return isOp() || PermissionManager.hasPermission(profile, inName, PermissionType.BUKKIT);
		}

		@Override
		public boolean hasPermission(Permission perm){
			return this.hasPermission(perm.getName());
		}

		@Override
		public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value){
			return super.addAttachment(plugin, name, value);
		}

		@Override
		public PermissionAttachment addAttachment(Plugin plugin){
			return super.addAttachment(plugin);
		}

		@Override
		public void removeAttachment(PermissionAttachment attachment){
			super.removeAttachment(attachment);
		}

		@Override
		public void recalculatePermissions(){
			//
		}

		@Override
		public synchronized void clearPermissions(){
			//
		}

		@Override
		public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks){
			return super.addAttachment(plugin, name, value, ticks);
		}

		@Override
		public PermissionAttachment addAttachment(Plugin plugin, int ticks){
			return super.addAttachment(plugin, ticks);
		}

		@Override
		public Set<PermissionAttachmentInfo> getEffectivePermissions(){
			Set<PermissionAttachmentInfo> permissionList = new HashSet<>();

			for(PermissionWrap w : PermissionManager.getPermissions(getProfile())){
				PermissionAttachmentInfo i = new PermissionAttachmentInfo(this,
						w.getPermission(), null, !w.isNegatePermission());
				permissionList.add(i);

				Permission perm = Bukkit.getPluginManager().getPermission(i.getPermission());
				if(perm != null && !perm.getChildren().isEmpty()){
					Map<String, Boolean> ch = perm.getChildren();

					for(String s : ch.keySet()){
						PermissionAttachmentInfo i0 = new PermissionAttachmentInfo(this,
								s, null, ch.get(s));
						permissionList.add(i0);
					}
				}
			}
			return permissionList;
		}
	}

}
