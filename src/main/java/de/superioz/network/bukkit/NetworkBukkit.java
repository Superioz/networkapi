package de.superioz.network.bukkit;

import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NetworkAPI;
import de.superioz.network.api.mongo.MongoConfigData;
import de.superioz.network.api.netty.NettyHandler;
import de.superioz.network.api.netty.NioPacket;
import de.superioz.network.api.netty.NioPacketType;
import de.superioz.network.api.netty.NioSocketClient;
import de.superioz.network.bukkit.listener.JoinListener;
import de.superioz.network.bukkit.packet.NickPacketHandler;
import de.superioz.network.bukkit.listener.NickUpdateListener;
import de.superioz.network.bukkit.listener.PrefixSuffixUpdateListener;
import de.superioz.network.bukkit.util.LanguageManager;
import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.protocol.*;
import de.superioz.sx.bukkit.exception.CommandRegisterException;
import de.superioz.sx.bukkit.util.YamlFile;
import de.superioz.sx.java.util.Consumer;
import de.superioz.sx.java.util.JsonUtil;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created on 26.04.2016.
 */
public class NetworkBukkit extends JavaPlugin {

	public static final String CONFIG_FILE_NAME = "config";
	public static final String FILE_RESOURCE_DIR = "bukkit";

	public static final String MASTER_SERVER_IP = "masterIp";

	public static YamlFile c;
	public static NioSocketClient s;
	public static NetworkBukkit h;

	@Override
	public void onEnable(){
		BukkitLibrary.initFor(this);
		NetworkAPI.setLogger(getLogger());
		h = this;

		// Load config
		this.loadConfig();

		// Server
		this.loadNettyClient();

		// Listener
		this.registerListener();
	}

	@Override
	public void onDisable(){
		// Code here
	}

	/**
	 * Loads the config
	 */
	public void loadConfig(){
		c = new YamlFile(CONFIG_FILE_NAME, "", getDataFolder());
		c.load(FILE_RESOURCE_DIR, true, true);
		LanguageManager.initialise();
	}

	/**
	 * Registers the listener
	 */
	private void registerListener(){
		BukkitLibrary.registerListener(new JoinListener());
		BukkitLibrary.registerListener(new NickUpdateListener());
		BukkitLibrary.registerListener(new PrefixSuffixUpdateListener());
	}

	/**
	 * Load the netty client and the database within
	 */
	private void loadNettyClient(){
		if(s == null || !s.check())
			s = NettyHandler.createNewClient(c.c().getString(MASTER_SERVER_IP), getServer().getIp());
		if(!s.check()){
			return;
		}

		// REGISTER PACKET HANDLER (NICK / UNNICK
		NettyHandler.registerPacketHandler(new NickPacketHandler());

		// get database
		this.synchronize();
	}

	/**
	 * Synchronizes the database
	 */
	public void synchronize(){
		// Get the database
		getLogger().info("Synchronize database ..");
		s.write(new NioPacket(NioPacketType.REQUEST, NettyHandler.GET_DATABASE_COMMAND,
						"", NettyHandler.PASSWORD.toCharArray()),
				new Consumer<NioPacket>() {
					@Override
					public void accept(NioPacket packet){
						MongoConfigData data = JsonUtil.getGson().fromJson(packet.getContent(), MongoConfigData.class);
						NetworkAPI.mongoConnect(data.getDatabasePrefix(),
								data.getHostName(),
								data.getPort(),
								data.getUserName(),
								data.getPassword(),
								data.getDatabase());

						if(NetworkAPI.checkMongo()){
							GroupManager.synchronize();
						}
					}
				});
	}

}
