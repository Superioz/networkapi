package de.superioz.network.bukkit.event;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

/**
 * Created on 02.05.2016.
 */
@Getter
public class PrefixSuffixUpdateEvent extends Event implements Cancellable{

	private boolean cancelled = false;
	private static final HandlerList handlers = new HandlerList();

	private Player forPlayer;
	private List<Player> viewer;
	private String prefix;
	private String suffix;

	public PrefixSuffixUpdateEvent(Player forPlayer, List<Player> viewer, String suffix, String prefix){
		this.forPlayer = forPlayer;
		this.viewer = viewer;
		this.suffix = suffix;
		this.prefix = prefix;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}

}
