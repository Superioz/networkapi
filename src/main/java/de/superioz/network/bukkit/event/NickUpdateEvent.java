package de.superioz.network.bukkit.event;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created on 02.05.2016.
 */
@Getter
public class NickUpdateEvent extends Event implements Cancellable {

	private boolean cancelled = false;
	private static final HandlerList handlers = new HandlerList();

	private Player player;
	private boolean onJoin;

	public NickUpdateEvent(Player player, boolean onJoin){
		this.player = player;
		this.onJoin = onJoin;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}
}
