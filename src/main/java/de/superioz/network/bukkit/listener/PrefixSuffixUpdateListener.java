package de.superioz.network.bukkit.listener;

import de.superioz.network.bukkit.event.PrefixSuffixUpdateEvent;
import de.superioz.sx.bukkit.common.NametagManager;
import de.superioz.sx.bukkit.util.ChatUtil;
import net.md_5.bungee.protocol.packet.Chat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

/**
 * Created on 02.05.2016.
 */
public class PrefixSuffixUpdateListener implements Listener {

	@EventHandler
	public void onPrefixSuffix(PrefixSuffixUpdateEvent event){
		if(event.isCancelled()){
			return;
		}

		Player player = event.getForPlayer();
		List<Player> viewer = event.getViewer();
		String prefix = ChatUtil.colored(event.getPrefix());
		String suffix = ChatUtil.colored(event.getSuffix());

		// NOW SET THE PREFIX AND THE SUFFIX
		NametagManager.setNametag(player, prefix, suffix, viewer.toArray(new Player[]{}));
	}

}
