package de.superioz.network.bukkit.listener;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.superioz.network.api.GroupManager;
import de.superioz.network.api.NickManager;
import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.SkinCache;
import de.superioz.network.api.registry.PermissionGroup;
import de.superioz.network.api.registry.SkinProperty;
import de.superioz.network.bukkit.common.NickRegistry;
import de.superioz.network.bukkit.event.NickUpdateEvent;
import de.superioz.network.bukkit.event.PrefixSuffixUpdateEvent;
import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.GameProfileBuilder;
import de.superioz.sx.bukkit.common.protocol.WrappedGameProfile;
import de.superioz.sx.bukkit.util.BukkitUtilities;
import de.superioz.sx.bukkit.util.ChatUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

/**
 * Created on 02.05.2016.
 */
public class NickUpdateListener implements Listener {

	@EventHandler
	public void onNickUpdate(final NickUpdateEvent event){
		if(event.isCancelled()){
			return;
		}
		final Player player = event.getPlayer();
		final UUID uuid = player.getUniqueId();
		final boolean isOnJoin = event.isOnJoin();

		// CHECK PROFILE
		if(!ProfileManager.hasProfile(uuid)){
			event.setCancelled(true);
			return;
		}

		// NOW GET THE NICK NAME AND SET TO THE PLAYER
		final String nickName = NickManager.getNick(uuid);
		final WrappedGameProfile[] profile = new WrappedGameProfile[1];

		Thread thr = new Thread(new Runnable() {
			@Override
			public void run(){
				if(!nickName.isEmpty()){
					SkinProperty property;
					if(SkinCache.contains(nickName)
							&& (property = SkinCache.getProperty(nickName)) != null){
						GameProfile p = new GameProfile(uuid, nickName);
						p.getProperties().removeAll("textures");
						p.getProperties().putAll("textures", Collections.singletonList(
								new Property(property.getName(), property.getValue(), property.getSignature())));
						profile[0] = new WrappedGameProfile(p);
						profile[0].setName(nickName);
					}
					else{
						profile[0] = GameProfileBuilder.get(nickName, nickName);
						Property pr = profile[0].getProperties().get("textures").iterator().next();
						SkinCache.writeProperty(nickName, new SkinProperty(pr.getName(), pr.getSignature(), pr.getValue()));
					}
				}
			}
		});
		thr.start();
		try{
			thr.join();

			if(nickName.isEmpty()){
				NickRegistry.unickPlayer(player, !isOnJoin, BukkitUtilities.onlinePlayers());

				// UPDATE PREFIX & SUFFIX
				// I WON'T DO IT, 'CAUSE IDK WHAT THE NICK PLUGINS MEANT TO BE LATER
			}
			else{
				if(profile[0] == null){
					event.setCancelled(true);
					return;
				}
				NickRegistry.nickPlayer(player, profile[0], !isOnJoin, BukkitUtilities.onlinePlayers());

				// SET PLAYERLIST NAME
				// UPDATE PREFIX & SUFFIX
				// I WON'T DO IT, 'CAUSE IDK WHAT THE NICK PLUGINS MEANT TO BE LATER
			}
		}
		catch(InterruptedException e){
			//
		}
	}

}
