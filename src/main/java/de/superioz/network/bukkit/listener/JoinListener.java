package de.superioz.network.bukkit.listener;

import de.superioz.network.api.ProfileManager;
import de.superioz.network.api.registry.PlayerProfile;
import de.superioz.network.bukkit.common.PermissionInjector;
import de.superioz.network.bukkit.event.NickUpdateEvent;
import de.superioz.sx.bukkit.BukkitLibrary;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.permissions.Permissible;

/**
 * Created on 30.04.2016.
 */
public class JoinListener implements Listener {

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event){
		Player player = event.getPlayer();

		// Get the permission group of the player
		if(!ProfileManager.hasProfile(player.getUniqueId())){
			return;
		}
		PlayerProfile profile = ProfileManager.getProfile(player.getUniqueId());

		// Set permissions
		Permissible oldPermissible = PermissionInjector.getPermissible(player);
		PermissionInjector.inject(player, new PermissionInjector.CustomPermissible(player, profile, oldPermissible));
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();

		// UPDATE PREFIX & SUFFIX
		// I WON'T DO IT, 'CAUSE IDK WHAT THE NICK PLUGINS MEANT TO BE LATER

		// SET THE NICK NAME
		BukkitLibrary.callEvent(new NickUpdateEvent(player, true));
	}

}
