package de.superioz.network.bukkit.packet;

import de.superioz.network.api.NickManager;
import de.superioz.network.api.netty.NioPacket;
import de.superioz.network.api.netty.NioPacketHandler;
import de.superioz.network.api.netty.NioPacketReceiveEvent;
import de.superioz.network.bukkit.common.NickNames;
import de.superioz.network.bukkit.event.NickUpdateEvent;
import de.superioz.sx.bukkit.BukkitLibrary;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created on 03.05.2016.
 */
public class NickPacketHandler implements NioPacketHandler {

	@Override
	public NioPacketReceiveEvent.ChannelType getType(){
		return NioPacketReceiveEvent.ChannelType.SERVANT;
	}

	@Override
	public void onReceive(NioPacketReceiveEvent event){
		NioPacket packet = event.getPacket();
		String command = packet.getCommand();

		// NICK COMMAND
		if(!(command.equals(NickManager.NICK_COMMAND)
				|| command.equals(NickManager.UNNICK_COMMAND))){
			return;
		}

		String content = packet.getContent();
		String[] spl = content.split(";");
		String playerName = spl[0];

		// CHECK NAME
		Player player = Bukkit.getPlayer(playerName);
		if(player == null || !player.isOnline()){
			return;
		}
		String nickName = spl.length > 1 ? (spl[1]) : "";
		if(!nickName.isEmpty() && !NickNames.checkName(nickName)){
			event.respond(new NioPacket(packet.getCommand(), "0"));
			return;
		}
		NickManager.setNick(player.getUniqueId(), spl.length > 1 ? (spl[1]) : "");

		// EVENT
		NickUpdateEvent e = new NickUpdateEvent(player, false);
		BukkitLibrary.callEvent(e);

		// Get result
		if(!e.isCancelled()){
			event.respond(new NioPacket(packet.getCommand(), "1"));
		}
		else{
			NickManager.setNick(player.getUniqueId(), "");
			event.respond(new NioPacket(packet.getCommand(), "0"));
		}
	}

}
