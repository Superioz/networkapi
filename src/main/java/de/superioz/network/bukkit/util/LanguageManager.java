package de.superioz.network.bukkit.util;

import de.superioz.network.bukkit.NetworkBukkit;
import de.superioz.network.bungee.NetworkBungee;
import de.superioz.sx.java.file.SuperProperties;
import de.superioz.sx.java.file.TextFile;
import lombok.Getter;

/**
 * Created on 30.04.2016.
 */
public class LanguageManager {

	private static SuperProperties<String> prefixes;
	private static SuperProperties<String> strings;

	/**
	 * Initialises all property files
	 */
	public static void initialise(){
		// Prfixes
		prefixes = new SuperProperties<>("prefixes", "language", NetworkBukkit.h.getDataFolder());
		prefixes.load(NetworkBukkit.FILE_RESOURCE_DIR, true, true);

		// Strings
		strings = new SuperProperties<>("strings", "language", NetworkBukkit.h.getDataFolder());
		strings.load(NetworkBukkit.FILE_RESOURCE_DIR, true, true);
	}

	/*
	Getter for different property files
	 */

	public static String getPrefix(String v){
		return prefixes.get(v);
	}

	public static String getString(String v){
		return strings.get(v);
	}

}
